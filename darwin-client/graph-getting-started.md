Graphs
===

## About

Darwin provides two different ways of representing a graph. They are as follows:

* **explicit graph** - the components of graph are stored in the memory explicitly
* **implicit graph** - the graph is stored as a set o functions, each of which generates nodes and edges

## Getting started with the API

### Using explicit graph

It is always necessary to initialise the client and sign in into Darwin in the beginning:

```
#!javascript

// The value of this application ID is an example and does not refer to any application
var darwinConfig = { appId: 'some_id', autoSignIn: false };
var client = null;

$(document).on('ready', function() {
    client = new window.darwin.Client(darwinConfig);
    client.start().then(function() {
        var username = 'some_username@137.201.53.134:5984';
        var password = 'some_password';

        client.signIn(username, password).then(function() {

            client.on('explicit-graph-manager-initialise-success', function() {
                // You may use an explicit graph safely and correctly in this scope
                // because the explicit graph manager is already initialised.

                // YOUR FURTHER CODE HERE
            });

        }, function(err) {
            console.error('Failed to sign in!', err.message);
        });
    }, function(err) {
        console.error('Failed to start the Darwin client!', err.message);
    });
});
```

Once the whole initialisation is done, you are able to create an instance of explicit graph.

There are two different types of an explicit graph: consolidated and distributed. By consolidated graph
we denote a graph which is stored in its creator's space in the Darwin cloud. It means that the graph of
this type are stored in one place as one fragment. Distributed graph is completely opposite in case of
fragmentation in the memory. Each node of the distributed graph can be stored in different place. The final
location of each graph component depends on the creator's friends network and other factors.

```
#!javascript

var graphAttributes = {
    'your_attribute': [12, 15, 3],
    'another_attribute': 'value'
};

// Creates an instance of consolidated explicit graph
client.createExplicitGraph(graphAttributes, darwin.Config.GRAPH.DISTRIBUTION.CONSOLIDATED).then(function(graph) {
    // You may use your newly created graph in this scope. It is given by the first parameter
    // of the callback function - variable "graph" in this example.

    // YOUR FURTHER CODE HERE
}, function(err) {
    console.error('Failed to create the graph!', err.message);
});
```

The graph has been already created. It can be used within the scope of success callback function,
however in order to get the existing graph in the future, we use single function:

```
#!javascript

// The value of this graph ID is an example and does not refer to any graph
var graphId = 'some_id';

client.getExplicitGraph(graphId).then(function(graph) {
    // The graph is available under the variable "graph" in this example.

    // YOUR FURTHER CODE HERE
}, function(err) {
    console.error('Failed to get the graph!', err.message);
});
```

Once the graph is created or retrieved, it can be used. Let's create some nodes:

```
#!javascript

var firstNodeAttributes = {
    'first_attribute': { 'a': 3, 'b': 7 },
    'second_attribute': 'X'
};

var secondNodeAttributes = {
    'third_attribute': [],
    'fourth_attribute': 'Y'
};

graph.createNode([firstNodeAttributes, secondNodeAttributes]).then(function(nodes) {
    // The nodes are available under the array "nodes" in this example.

    // YOUR FURTHER CODE HERE
}, function(err) {
    console.error('Failed to create the nodes!', err.message);
});
```

Now the nodes can be manipulated:

```
#!javascript

var firstNode = nodes[0];
var secondNode = nodes[1];

var edgeData = {
    endNodeId: secondNode.getId(),
    attributes: {
        sampleAttribute: [1, 3]
    }
};

firstNode.createEdge([edgeData]).then(function(edges) {
    // The edge is available under the first index of the array "edges" in this example.

    // YOUR FURTHER CODE HERE
}, function(err) {
    console.error('Failed to create the edges!', err.message);
});
```

The edges can be retrieved in the future as well. One of the solutions is to enumerate outbound/inbound edges
of certain node. Let's use our first node:

```
#!javascript

firstNode.enumerateOutboundEdges().then(function(enumerator) {

    var asyncLoop = function(complete, edgesAcc=[]) {
        enumerator.hasNext().then(function(result) {

            if (result) { // If the enumerator has next element
                enumerator.next().then(function(edge) {

                    edgesAcc.push(edge);
                    asyncLoop(complete, edgesAcc);

                }, function(err) {
                    console.error('Failed to retrieve enumerator state!', err.message);
                });
            } else { // If the enumerator has went thorough the whole set
                complete(edgesAcc);
            }

        }, function(err) {
            console.error('Failed to retrieve enumerator state!', err.message);
        });
    };

    asyncLoop(function(edges) {
        // The edges are available under the array "edges" in this example.

        // YOUR FURTHER CODE HERE
    });

}, function(err) {
    console.error('Failed to get the enumerator!', err.message);
});
```
