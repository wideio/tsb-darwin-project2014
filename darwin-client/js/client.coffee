#_darwin_server_url = 'http://darwin.wide.io/'
_darwin_server_url = 'http://localhost:8000/darwin-server/'

_darwin_client_url = ''

$(document).on('ready', () =>
  if typeof(Promise) is 'undefined'
    $.getScript(_darwin_server_url + 'lib/rsvp.min.js', () =>
      Promise = RSVP.Promise
    )
)

window.darwin = {}

window.darwin.Config = {
  DISPATCHER: {
    DATABASE_MANAGER: 'DatabaseManager',
    LOCAL_MANAGER: 'LocalManager',
    REMOTE_MANAGER: 'RemoteManager',
    DISTRIBUTION_MANAGER: 'DistributionManager',
    EXPLICIT_GRAPH_MANAGER: 'ExplicitGraphManager',
    IMPLICIT_GRAPH_MANAGER: 'ImplicitGraphManager'
  },

  GRAPH: {
    TYPE: {
      EXPLICIT: 'explicit',
      IMPLICIT: 'implicit'
    },

    DISTRIBUTION: {
      CONSOLIDATED: 'consolidated',
      DISTRIBUTED: 'distributed'
    },

    EDGE_TYPE: {
      INBOUND: 'INBOUND',
      OUTBOUND: 'OUTBOUND'
    },

    IMPLICIT: {
      FUNCTION: {
        NODE_EXISTS: 'nodeExists',
        ENUMERATE_NODE_IDS: 'enumerateNodeIds',
        ENUMERATE_NODE_INBOUND_EDGE_IDS: 'enumerateNodeInboundEdgeIds',
        ENUMERATE_NODE_OUTBOUND_EDGE_IDS: 'enumerateNodeOutboundEdgeIds',
        GET_NODE_ATTRIBUTES: 'getNodeAttributes',
        GET_EDGE_ATTRIBUTES: 'getEdgeAttributes'
      }
    }
  }
}

# Represent outgoing connection
class ClientConnection
  constructor: ->
    @_userAddress = null

    @_appId = null

    @_frame = null

    @_manager = null

    @_callbacks = {}

    @_managerMethods = null

    @_eventHandlers = {}

    window.addEventListener('message', @_receive)

  _log: () =>
    args = ['client-connection @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  _initialise: (appId, frame, manager) =>
    @_appId = appId
    @_frame = frame
    @_manager = manager
    @_managerMethods = manager.getPublicMethods()

  _run: (event, success, error) =>
    try
      methodName = event.methodName
      data = event.data

      if methodName in @_managerMethods
        f = @_manager[methodName]
        f(data, success, error)
      else
        error({ message: 'Cannot find the specified method.' })
    catch err
      error({ message: err.message })

  _receive: (e) =>
    try
      connObj = e.data

      type = connObj.type

      if type is 'request'
        @_receiveRequest(connObj)
      else if type is 'response'
        @_receiveResponse(connObj)
      else if type is 'event'
        @_receiveEvent(connObj)
    catch err
      @_log(err.message)

  _receiveResponse: (connObj) =>
    id = connObj.id
    data = connObj.data
    status = connObj.status
    remainingParts = connObj.remainingParts

    if status
      callback = @_callbacks[id].success
      if typeof(callback) is 'function'
        callback(data, remainingParts)
    else
      callback = @_callbacks[id].error
      if typeof(callback) is 'function'
        if 'message' in data
          callback(new Error(data.message), remainingParts)
        else
          callback(data)

    if remainingParts is 0
      delete @_callbacks[id]

  _receiveRequest: (connObj) =>
    id = connObj.id
    methodName = connObj.methodName

    success = (result) =>
      @_sendResponse(id, methodName, result, true)

    error = (result) =>
      @_sendResponse(id, methodName, result, false)

    @_run(connObj, success, error)

  _receiveEvent: (connObj) =>
    name = connObj.name
    data = connObj.data

    if name of @_eventHandlers
      @_eventHandlers[name].data.push(data)
      for handler in @_eventHandlers[name].handlers
        handler(data)
    else
      @_eventHandlers[name] = {
        data: [data],
        handlers: []
      }

  _sendResponse: (id, methodName, data, status) =>
    if @_frame isnt null
      connObj = {
        id: id,
        type: 'response',
        sourceUserAddress: @_userAddress,
        destinationUserAddress: @_userAddress,
        methodName: methodName,
        data: data,
        status: status
      }

      @_frame.postMessage(connObj, _darwin_server_url) # connObj is not stringified to prevent blobs from data loss
    else
      @_log('The connection between the client and server is not established.')

  sendRequest: (destinationUserAddress, dispatcherName, methodName, parameters) =>
    new Promise((success, error) =>
      if @_frame isnt null
        connObj = {
          id: @_manager.uuid(),
          type: 'request',
          sourceData: {
            userAddress: @_userAddress,
            appId: @_appId
          },
          destinationData: {
            userAddress: destinationUserAddress,
            dispatcherName: dispatcherName,
            methodName: methodName,
            parameters: parameters
          },
          sourceUserAddress: @_userAddress, # obsolete
          destinationUserAddress: destinationUserAddress, # obsolete
          methodName: methodName, # obsolete
          data: parameters # obsolete
        }

        @_callbacks[connObj.id] = {
          success: success,
          error: error
        }

        @_frame.postMessage(connObj, _darwin_server_url) # connObj is not stringified to prevent blobs from data loss
      else
        error(new Error('The connection between the client and server is not established.'))
    )

  on: (name, handler) =>
    if name of @_eventHandlers
      @_eventHandlers[name].handlers.push(handler)
      for data in @_eventHandlers[name].data
        handler(data)
    else
      @_eventHandlers[name] = {
        data: [],
        handlers: [handler]
      }

  off: (name) =>
    if name of @_eventHandlers
      @_eventHandlers[name] = {
        data: [],
        handlers: []
      }

  getUserAddress: =>
    @_userAddress

  setUserAddress: (userAddress) =>
    @_userAddress = userAddress



# TODO: I don't like this class - it is placed here temporarily
class UI
  constructor: (client) ->
    @_client = client

  _log: () =>
    args = ['client-ui @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  _attachGeneralEventHandlers: =>
    $(window).on('click', () =>
      $('div.darwin-popup').hide().trigger('hide')
    )

    $('div.darwin-popup').on('click', (e) =>
      e.stopPropagation()
    )

    $('div#darwin-widget').on('sign-in', (e, username) ->
      $(this).find('div#darwin-panel div.signed-in span.info span.bold').html(username)
      $(this).find('div#darwin-controller').addClass('signed-in')
    )

    $('div#darwin-widget').on('sign-out', () ->
      $(this).find('div#darwin-controller').removeClass('signed-in')
    )

  getServerController: =>
    new Promise((success, error) =>
      if $('div#darwin-widget').length is 0
        $.get(_darwin_server_url + 'base.html', (response) =>
          $(response).appendTo('body')
          @_attachGeneralEventHandlers()
          success()
        )
      else
        error({ message: 'The server controller already exists.' })
    )

  assignServerSource: =>
    new Promise((success, error) =>
      frame = $('div#darwin-widget iframe#server-frame')
      frame.attr('src', _darwin_server_url + 'server-frame.html')
      frame.load(() =>
        success(frame[0])
      )
    )

  unlockPostInitialiseEventHandlers: =>
    refreshConnectionsHandler = null

    $('div#darwin-controller button').on('click', (e) =>
      e.stopPropagation()

      panel = $('div#darwin-panel')
      if panel.css('display') is 'none'
        if $('div#darwin-controller').hasClass('signed-in')
          panel.find('div.signed-in').css('display', 'table-cell')
          panel.find('div.signed-out').hide()
        else
          panel.find('div.signed-in').hide()
          panel.find('div.signed-out').css('display', 'table-cell')
        panel.css('display', 'table')
      else
        panel.hide()
    )

    $('div.darwin-popup div.tab-controller button').on('click', (e) =>
      e.stopPropagation()
      popup = $(e.target).parent().parent()

      classArray = $(e.target).attr('class').split(' ')
      popup.find('div.tab').hide()
      popup.find('div.tab.' + classArray[0]).show()

      popup.find('div.tab-controller button').removeClass('active')
      $(e.target).addClass('active')
    )

    $('div#darwin-panel div.signed-out button.sign-in').on('click', (e) =>
      e.stopPropagation()
      popup = $('div#darwin-sign-in')
      popup.css({
        left: ($(window).width() - popup.width()) / 2,
        top: ($(window).height() - popup.height()) / 2
      }).show(0, () =>
        popup.trigger('show')
      )
    )

    $('div#darwin-panel div.signed-in button.connections').on('click', (e) =>
      e.stopPropagation()
      popup = $('div#darwin-connections')
      popup.css({
        left: ($(window).width() - popup.width()) / 2,
        top: ($(window).height() - popup.height()) / 2
      }).show(0, () =>
        popup.trigger('show')
      )
    )

    $('div#darwin-connections button.new-connection').on('click', (e) =>
      e.stopPropagation()
      popup = $('div#darwin-new-connection')
      popup.css({
        left: ($(window).width() - popup.width()) / 2,
        top: ($(window).height() - popup.height()) / 2
      }).show(0, () =>
        popup.trigger('show')
      )
    )

    $('div#darwin-sign-in button').on('click', (e) =>
      e.stopPropagation()
      address = $('div#darwin-sign-in input.address').val()
      password = $('div#darwin-sign-in input.password').val()

      @_client.signIn(address, password).then(((response) =>
        @_client._userAddress = response.username + '@' + response.address
        @_client._connection.setUserAddress(@_client._userAddress)
        $('div.darwin-popup').hide().trigger('hide')
        $('div#darwin-panel div.signed-out').hide()
        $('div#darwin-panel div.signed-in').css('display', 'table-cell')
        $('div#darwin-widget').trigger('sign-in', [ response.username ])
        $('div#darwin-screen-cover').hide()
        @_log('User ' + response.username + ' signed in.')
      ), ((err) =>
        @_log('Failed to sign in: ' + err.message)
#       $('div#darwin-sign-in input.address').css('color','red')
#       $('div#darwin-sign-in input.password').css('color','red')
      ))
    )

    $('div#darwin-panel div.signed-in button.sign-out').on('click', (e) =>
      e.stopPropagation()
      @_client.signOut().then(((response) =>
        @_client._userAddress = null
        $('div#darwin-widget').trigger('sign-out')
        $('div.darwin-popup').hide().trigger('hide')
        $('div#darwin-panel div.signed-in').hide()
        $('div#darwin-panel div.signed-out').css('display', 'table-cell')
        @_log('User ' + response.username + ' signed out.')
      ), ((err) =>
        @_log('Failed to sign out')
      ))
    )

    $('div#darwin-connections').on('show', () =>
      @refreshConnections()
      refreshConnectionsHandler = window.setInterval(@refreshConnections, 2000)
    )

    $('div#darwin-connections').on('hide', () =>
      window.clearInterval(refreshConnectionsHandler)
    )

    $('div#darwin-new-connection button.submit').on('click', (e) =>
      e.stopPropagation()
      address = $('div#darwin-new-connection input.address').val()
      permissionsStr = $('div#darwin-new-connection input.permissions').val()

      permissions = permissionsStr.trim().split(' ')

      remoteProxy = @_client.getRemoteConnection(address)
      remoteProxy.connect(permissions).then((() =>
        $('div#darwin-new-connection').hide().trigger('hide')
        @refreshConnections()
      ), ((err) =>
        @_log('Failed to connect to the remote user: ' + err.message)
        @refreshConnections()
      ))
    )

    $('div#darwin-permissions div.tab.request button.submit').on('click', (e) =>
      e.stopPropagation()
      address = $('div#darwin-permissions input.address').val()
      permissionsStr = $('div#darwin-permissions input.my-permissions').val()

      permissions = permissionsStr.trim().split(' ')

      remoteProxy = @_client.getRemoteConnection(address)
      remoteProxy.requestPermissions(permissions).then((() =>
        $('div#darwin-permissions').hide()
        @refreshConnections()
      ), ((err) =>
        @_log('Failed to gain permissions: ' + err.message)
        @refreshConnections()
      ))
    )

    $('div#darwin-permissions div.tab.grant button.submit').on('click', (e) =>
      e.stopPropagation()
      address = $('div#darwin-permissions input.address').val()
      permissionsStr = $('div#darwin-permissions input.remote-user-permissions').val()

      permissions = permissionsStr.trim().split(' ')

      remoteProxy = @_client.getRemoteConnection(address)
      remoteProxy.grantPermissions(permissions).then((() =>
        $('div#darwin-permissions').hide()
        @refreshConnections()
      ), ((err) =>
        @_log('Failed to grant permissions: ' + err.message)
        @refreshConnections()
      ))
    )

  # TODO: Refactor it!
  refreshConnections:  =>
    popup = $('#darwin-connections')

    @_client.getAllRemoteConnections().then((remoteProxies) =>
      ul = $('<ul></ul>')
      for address, remoteProxy of remoteProxies
        status = remoteProxy.getStatus()
        li = $('<li><div class="address">' + address + '</div>
                <div class="permissions">
                <a href="javascript:window._darwinUI.showPermissionsPopup(\'' +
                remoteProxy.getRemoteUserAddress() + '\')">permissions</a>
                </div>
                <div class="status">' + status + '</div></li>')
        ul.append(li)

      if Object.keys(remoteProxies).length > 0
        popup.find('ul.connection-list').html(ul.html())
    )

  showYesNoPopup: (html, accept, decline) =>
    popup = $('div#darwin-yesno')
    popup.find('p.title').text('Request for permissions')
    popup.find('div.section').html(html)

    operations = (e) =>
      e.stopPropagation()
      popup.hide()

    popup.find('button.accept').off('click').one('click', (e) =>
      @refreshConnections()
      operations(e)
      accept()
    )

    popup.find('button.decline').off('click').one('click', (e) =>
      operations(e)
      decline()
    )

    popup.css({
      left: ($(window).width() - popup.width()) / 2,
      top: ($(window).height() - popup.height()) / 2
    }).show(0, () =>
      popup.trigger('show')
    )

  showPermissionsPopup: (remoteUserAddress) =>
    popup = $('div#darwin-permissions')

    permissionsStr = ''
    if remoteUserAddress of @_client._remoteConnections
      permissions = @_client._remoteConnections[remoteUserAddress].getMyPermissions()
      if permissions isnt null
        for p in permissions
          permissionsStr += p + ' '
        permissionsStr = permissionsStr.trim()
    popup.find('input.my-permissions').val(permissionsStr)

    permissionsStr = ''
    if remoteUserAddress of @_client._remoteConnections
      permissions = @_client._remoteConnections[remoteUserAddress].getRemoteUserPermissions()
      if permissions isnt null
        for p in permissions
          permissionsStr += p + ' '
        permissionsStr = permissionsStr.trim()
    popup.find('input.remote-user-permissions').val(permissionsStr)

    popup.find('input.address').val(remoteUserAddress)
    popup.find('div.tab.grant div.section p.subtitle span.bold').text(remoteUserAddress)
    popup.css({
      left: ($(window).width() - popup.width()) / 2,
      top: ($(window).height() - popup.height()) / 2
    }).show(0, () =>
      popup.trigger('show')
    )



# Represents the manager which runs client methods requested by the server
class ClientManager
  constructor: (client) ->
    @_client = client

  _log: () =>
    args = ['client @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  getPublicMethods: =>
    forbiddenMethods = ['getPublicMethods', 'uuid']
    methods = []
    for methodName of @
      if methodName[0] isnt '_' and methodName not in forbiddenMethods
        methods.push(methodName)
    methods

  # TODO: Move to client utils or something
  uuid: =>
    d = new Date().getTime()
    uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) =>
      r = (d + Math.random() * 16) % 16 | 0
      d = Math.floor(d/16)
      return (if c is 'x' then r else (r & 0x7 | 0x8)).toString(16)
    )
    uuid

  receiveHandshake: (data, success, error) =>
    address = data[0]
    wantedPermissions = data[1]

    permissionsStr = ''
    separator = ', '
    for p in wantedPermissions
      permissionsStr += p + separator
    permissionsStr = permissionsStr.substr(0, permissionsStr.length - separator.length)

    html = '<p>User <span class="bold">' + address + '</span> wants to connect with you.</p>
            <p>Requested permissions: <span class="bold">' + permissionsStr + '</span></p>'

    @_client._UI.showYesNoPopup(html, success, error)

  requestPermissions: (data, success, error) =>
    address = data[0]
    wantedPermissions = data[1]

    permissionsStr = ''
    separator = ', '
    for p in wantedPermissions
      permissionsStr += p + separator
    permissionsStr = permissionsStr.substr(0, permissionsStr.length - separator.length)

    html = '<p>User <span class="bold">' + address + '</span> requests
            for permissions: <span class="bold">' + permissionsStr + '</span></p>'
    @_client._UI.showYesNoPopup(html, success, error)



#TODO: move into utils?
extend = (object, properties) ->
  for key, val of properties
    object[key] = val
  object

# Represent Darwin client main class
class darwin.Client
  constructor: (darwinConfig) ->
    @_userAddress = null

    @_UI = new UI(@)

    @_manager = new ClientManager(@)

    @_connection = new ClientConnection

    @_userStatus = false

    @_remoteConnections = {}

    @_options =  extend({ autoSignIn: false }, darwinConfig)

    window._darwinUI = @_UI

  _log: () =>
    args = ['client @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  _autoSignIn: (success, error) =>
    methodName = 'autoSignIn'
    data = []
    @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((response) =>
      @_userAddress = response.username + '@' + response.address
      @_userStatus = true
      @_connection.setUserAddress(@_userAddress)
      $('div#darwin-widget').trigger('sign-in', [ response.username ])
      success(response)
    ), ((err) =>
      $('div#darwin-widget').trigger('sign-out')
      error(err)
    ))

  _getAllRemoteConnectionsInfo: (success, error) =>
    methodName = 'getAllRemoteConnectionsInfo'
    data = []
    @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.REMOTE_MANAGER, methodName, data).then(((response) =>
      success(response.result)
    ), error)

  _getConnection: =>
    @_connection

  _getUserAddress: =>
    @_userAddress

  _autoSplashLogin: =>
    $('div#darwin-screen-cover').css('display', 'inline')
    popup = $('div#darwin-sign-in')
    popup.css({
      left: ($(window).width() - popup.width()) / 2,
      top: ($(window).height() - popup.height()) / 2
    }).show(0, () =>
      popup.trigger('show')
    )

  ###
  Starts the connection between the client and the local server

  @return Returns a promise object containing success and error callbacks
  ###
  start: =>
    new Promise((success, error) =>
      @_UI.getServerController().then((() =>
        @_UI.assignServerSource().then(((frame) =>
          @_connection._initialise(@_options.appId, frame.contentWindow, @_manager)

          methodName = 'hello' #TODO: use literals
          data = [@_options.appId]
          @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then((() =>
            @_log('Connected with the server.')

            if @_options.autoSignIn
              @_autoSignIn(((response) =>
                @_log('User ' + response.username + ' signed in automatically.')
                @_UI.unlockPostInitialiseEventHandlers()
                success()
              ), ((err) =>
                @_log('Cannot sign in automatically: ' + err.message)
                @_UI.unlockPostInitialiseEventHandlers()
                success()
              ))
            else
              @_UI.unlockPostInitialiseEventHandlers()
              success()
          ), (() =>
            error(new Error('Failed to finalise "hello" with the server.'))
          ))
        ), (() =>
          error(new Error('Failed to establish connection with the server.'))
        ))
      ), (() =>
        error(new Error('Failed to get the server objects.'))
      ))
    )

  signIn: (userAddress, password) =>
    new Promise((success, error) =>
      if not @_userStatus
        methodName = 'signIn'
        data = [userAddress, password]
        @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((response) =>
          @_userAddress = response.username + '@' + response.address
          @_userStatus = true
          @_connection.setUserAddress(@_userAddress)
          $('div#darwin-widget').trigger('sign-in', [ response.username ])
          success(response)
        ), error)
      else
        error(new Error('The user is already signed in.'))
    )

  signOut: =>
    new Promise((success, error) =>
      if @_userStatus
        methodName = 'signOut'
        data = []
        @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((response) =>
          @_userStatus = false
          $('div#darwin-widget').trigger('sign-out')
          success(response)
        ), error)
      else
        error(new Error('The user is already signed out.'))
    )

  ###
  Simply gets user status (signed in / signed out)

  @return Returns the user status as a boolean
  ###
  getUserStatus: =>
    @_userStatus

  ###
  Registers event handler which listens to events triggered by the server

  @param name The name of the event
  @param handler The handler as a function
  ###
  on: (name, handler) =>
    @_connection.on(name, handler)

  # DEPRECATED
  registerEventHandler: (name, handler) =>
    @_connection.on(name, handler)

  ###
  Removes event handler

  @param name The name of the event
  ###
  off: (name) =>
    @_connection.off(name)

  # DEPRECATED
  removeEventHandler: (name) =>
    @_connection.off(name)

  ###
  Gets proxies for currently opened connections

  @return Returns a promise object containing success and error callbacks
  ###
  getAllRemoteConnections: =>
    new Promise((success, error) =>
      if @_userStatus
        @_getAllRemoteConnectionsInfo(((remoteConnectionsInfo) =>
          remoteConnections = {}
          for address, info of remoteConnectionsInfo
            remoteConnections[address] = new RemoteConnection(address, @, info.status)
          @_remoteConnections = remoteConnections
          success(remoteConnections)
        ), error)
      else
        error(new Error('The user is not signed in.'))
    )

  ###
  Gets the proxy of the remote connection (the connection which is between the local server and
  remote user's server)

  @param address The address which identifies the remote user (e.g. tester@mbk.iriscouch.com)

  @return Returns RemoteConnection object
  ###
  getRemoteConnection: (address) =>
    if @_userStatus
      new RemoteConnection(address, @)
    else
      new Error('The user is not signed in.')

  ###
  Gets the proxy of the local application

  @return Returns Application object
  ###
  getApplication: =>
    if @_userStatus
      new Application(@)
    else
      new Error('The user is not signed in.')

  ###
  Gets the explicit graph by given ID

  @param id ID of the graph

  @return Returns a promise object containing an ExplicitGraph object
  ###
  getExplicitGraph: (id) =>
    new Promise((success, error) =>
      methodName = 'getGraph'
      parameters = [id]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, parameters).then(((response) =>
        result = response.result
        success(new ExplicitGraph(result.id, @))
      ), error)
    )

  ###
  Creates a new explicit graph

  @return Returns a promise object containing an ExplicitGraph object
  ###
  createExplicitGraph: (attributes={}, distribution=darwin.Config.GRAPH.DISTRIBUTION.CONSOLIDATED) =>
    new Promise((success, error) =>
      methodName = 'createGraph'
      parameters = [attributes, distribution]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, parameters).then(((response) =>
        result = response.result
        success(new ExplicitGraph(result.id, @))
      ), error)
    )

  ###
  Gets the implicit graph by given ID

  @param id ID of the graph

  @return Returns a promise object containing an ImplicitGraph object
  ###
  getImplicitGraph: (id) =>
    new Promise((success, error) =>
      methodName = 'getGraph'
      parameters = [id]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER, methodName, parameters).then(((response) =>
        result = response.result
        success(new ImplicitGraph(result.id, @))
      ), error)
    )

  ###
  Creates a new implicit graph by given ID

  @param functions A dictionary of implicit graph functions
  @param dependencies An FunctionCollection object containing the code needed by implicit graph functions

  @return Returns a promise object containing an ImplicitGraph object
  ###
  createImplicitGraph: (functions, dependencies=new darwin.FunctionCollection, attributes={}) =>
    new Promise((success, error) =>
      methodName = 'createGraph'
      parameters = [functions, dependencies.toCode(), attributes]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER, methodName, parameters).then(((response) =>
        result = response.result
        success(new ImplicitGraph(result.id, @))
      ), error)
    )



# Exemplary callbacks
@sampleSuccess = (response) =>
  console.log('Success:', response)

@sampleError = (err) =>
  console.log('Error:', err.message)



# Represent proxy for interacting with specified database
class Database
  constructor: (dbName, userAddress, client) ->
    @_dbName = dbName

    @_userAddress = userAddress

    @_client = client

    @_connection = client._getConnection()

  ###
  Gets read batch for this database

  @return Returns the read batch object
  ###
  getReadBatch: =>
    new ReadBatch(@_connection, @_userAddress, @_dbName)

  ###
  Gets write batch for this database

  @return Returns the write batch object
  ###
  getWriteBatch: =>
    new WriteBatch(@_connection, @_userAddress, @_dbName)

  ###
  Synchronises the local database with the remote mirror

  @return Returns a promise object containing success and error callbacks
  ###
  sync: =>
    new Promise((success, error) =>
      methodName = 'sync'
      data = [@_dbName]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.DATABASE_MANAGER, methodName, data).then(success, error)
    )

  ###
  Fetches all the documents from the local database

  @param options Options object

  @return Returns a promise object containing success and error callbacks
  ###
  all: (options={}) =>
    new Promise((success, error) =>
      methodName = 'all'
      data = [@_dbName, options]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.DATABASE_MANAGER, methodName, data).then(success, error)
    )

  ###
  Gets the document with the specified key

  @param key The key as a string

  @return Returns a promise object containing success and error callbacks
  ###
  get: (key) =>
    new Promise((success, error) =>
      methodName = 'get'
      data = [@_dbName, key]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.DATABASE_MANAGER, methodName, data).then(success, error)
    )

  ###
  Creates or updates the document with the specified key and value

  @param key The key as a string
  @param value The value as an object (dictionary)
  @param options Options object
    @option includeDetailedResult bool

  @return Returns a promise object containing success and error callbacks
  ###
  set: (key, value, options={}) =>
    new Promise((success, error) =>
      methodName = 'set'
      data = [@_dbName, key, value, options]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.DATABASE_MANAGER, methodName, data).then(success, error)
    )

  ###
  Deletes the document with the specified key

  @param key The key as a string

  @return Returns a promise object containing success and error callbacks
  ###
  delete: (key) =>
    new Promise((success, error) =>
      methodName = 'delete'
      data = [@_dbName, key]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.DATABASE_MANAGER, methodName, data).then(success, error)
    )



# Represents batch for read requests to be performed by Darwin
class ReadBatch
  constructor: (connection, userAddress, dbName) ->
    @_connection = connection
    @_userAddress = userAddress
    @_dbName = dbName
    @_keys = []

  addGet: (key) =>
    @_keys.push(key)

  submit: =>
    new Promise((success, error) =>
      methodName = 'executeReadBatch'
      data = [@_dbName, @_keys]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.DATABASE_MANAGER, methodName, data).then(success, error)
      @_keys = []
    )



# Represents batch for write requests to be performed by Darwin
class WriteBatch
  constructor: (connection, userAddress, dbName) ->
    @_connection = connection
    @_userAddress = userAddress
    @_dbName = dbName
    @_docs = []

  addSet: (key, value) =>
    doc = value
    doc._id = key
    @_docs.push(doc)

  addDelete: (key) =>
    doc = {}
    doc._id = key
    doc._deleted = true
    @_docs.push(doc)

  submit: =>
    new Promise((success, error) =>
      methodName = 'executeWriteBatch'
      data = [@_dbName, @_docs]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.DATABASE_MANAGER, methodName, data).then(success, error)
      @_docs = []
    )



# Represents proxy for interacting with the application
class Application
  constructor: (client) ->
    @_client = client

    @_connection = client._getConnection()

    @_userAddress = client._getUserAddress()

  ###
  Gets the proxy of the database

  @param dbName The name of the database (e.g. graph)

  @return Returns Database object
  ###
  getDatabase: (dbName, create=false) =>
    new Database(dbName, @_userAddress, @_client)

  ###
  Gets the proxy of the drive

  @param driveName The name of the drive (e.g. images)
  @param create The boolean value determining if the drive should be created

  @return Returns a promise object containing success and error callbacks
  ###
  getDrive: (driveName, create=false) =>
    new Promise((success, error) =>
      methodName = 'getDrive'
      data = [driveName, create]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then((() =>
        success(new Drive(driveName, @_client))
      ), error)
    )

  ###
  Gets all the drive names

  @return Returns a promise object containing success and error callbacks
  ###
  getAllDriveNames: =>
    new Promise((success, error) =>
      methodName = 'getAllDriveNames'
      data = []
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(success, error)
    )



class File
  constructor: (path, drive) ->
    @path = path

    @_driveName = drive._getDriveName()

    @_connection = drive._getConnection()

    @_userAddress = drive._getUserAddress()

    @_position = 0

  read: (length=null) =>
    new Promise((success, error) =>
      methodName = 'getFile'
      data = [@_driveName, @path, false, @_position, length]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((result) =>
        @_position = result.position
        success(result.data)
      ), error)
    )

  seek: (position) =>
    @_position = position

  rewind: =>
    @_position = 0

  write: (content) =>
    new Promise((success, error) =>
      sendRequest = (buffer) =>
        methodName = 'updateFile'
        data = [@_driveName, @path, buffer, @_position, false, 'text']
        @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((response) =>
          @_position = response.position
          success()
        ), error)

      if content instanceof Uint8Array
        sendRequest(content)
      else if content instanceof Blob
        reader = new FileReader()

        reader.onloadend = (e) =>
          sendRequest(e.target.result)

        reader.readAsArrayBuffer(content)
      else
        error(new Error('The content should be of type Blob or Uint8Array.'))
    )

  truncate: =>
    new Promise((success, error) =>
      methodName = 'truncateFile'
      data = [@_driveName, @path, @_position]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((result) =>
        @_position = result.position
        success()
      ), error)
    )



class Image
  constructor: (path, drive) ->
    @path = path

    @_driveName = drive._getDriveName()

    @_connection = drive._getConnection()

    @_userAddress = drive._getUserAddress()

  read: =>
    new Promise((success, error) =>
      methodName = 'getFile'
      data = [@_driveName, @path, false, 0, null, 'image']
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((result) =>
        @_position = result.position
        str = String.fromCharCode.apply(null, result.data)
        success(str)
      ), error)
    )

  writeImg: (img, type='image/png') =>
    new Promise((success, error) =>
      sendRequest = (buffer) =>
        methodName = 'updateFile'
        data = [@_driveName, @path, buffer, 0, true, 'image']
        @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((response) =>
          @_position = response.position
          success()
        ), error)

      # Uses canvas in order to get base64 string from the image
      canvas = document.createElement('canvas')
      canvas.width = img.width
      canvas.height = img.height

      context = canvas.getContext('2d')
      context.drawImage(img, 0, 0)

      content = new Blob([canvas.toDataURL(type)])

      reader = new FileReader()

      reader.onloadend = (e) =>
        sendRequest(e.target.result)

      reader.readAsArrayBuffer(content)
    )

  writeBase64: (base64Str) =>
    new Promise((success, error) =>
      sendRequest = (buffer) =>
        methodName = 'updateFile'
        data = [@_driveName, @path, buffer, 0, true]
        @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(((response) =>
          @_position = response.position
          success()
        ), error)

      content = new Blob([base64Str])

      reader = new FileReader()

      reader.onloadend = (e) =>
        sendRequest(e.target.result)

      reader.readAsArrayBuffer(content)
    )



class Directory
  constructor: (path, drive) ->
    @path = if path[path.length - 1] is '/' then path else path + '/'

    @_drive = drive

    @_driveName = drive._getDriveName()

    @_connection = drive._getConnection()

    @_userAddress = drive._getUserAddress()

    # TODO: Move somewhere else
    @_resolvePath()

  _getAbsoluteFilePath: (path) =>
    result = null
    if path[0] isnt '/'
      if path[path.length - 1] is '/'
        path = path.substr(0, path.length - 1)
      result = @path + path
    else
      result = path

  _getAbsoluteDirectoryPath: (path) =>
    result = null
    if path[0] isnt '/'
      if path[path.length - 1] isnt '/'
        path += '/'
      result = @path + path
    else
      result = path

  _resolvePath: =>
    new Promise((success, error) =>
      methodName = 'resolveDirectoryPath'
      data = [@_driveName, @path, true]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(success, error)
    )

  listAllFileNames: =>
    new Promise((success, error) =>
      methodName = 'listFileNames'
      data = [@_driveName, @path]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(success, error)
    )

  listAllDirectoryNames: =>
    new Promise((success, error) =>
      methodName = 'listDirectoryNames'
      data = [@_driveName, @path]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(success, error)
    )

  getAllFiles: =>
    new Promise((success, error) =>
      @listAllFileNames().then(((names) =>
        result = []

        for name in names
          path = @_getAbsoluteFilePath(name)
          result.push(new File(path, @_drive))

        success(result)
      ), ((err) =>
        error(new Error('Failed to get all files.\n' + err.message))
      ))
    )

  getAllDirectories: =>
    new Promise((success, error) =>
      @listAllDirectoryNames().then(((names) =>
        result = []

        for name in names
          path = @_getAbsoluteDirectoryPath(name)
          result.push(new Directory(path, @_drive))

        success(result)
      ), ((err) =>
        error(new Error('Failed to get all directories.\n' + err.message))
      ))
    )

  openFile: (path, create=false) =>
    new Promise((success, error) =>
      path = @_getAbsoluteFilePath(path)
      methodName = 'getFile'
      data = [@_driveName, path, create, 0, null]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then((() =>
        success(new File(path, @_drive))
      ), ((err) =>
        error(new Error(err.message))
      ))
    )

  openImage: (path, create=false) =>
    new Promise((success, error) =>
      path = @_getAbsoluteFilePath(path)
      methodName = 'getFile'
      data = [@_driveName, path, create, 0, null]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then((() =>
        success(new Image(path, @_drive))
      ), ((err) =>
        error(new Error(err.message))
      ))
    )

  deleteFile: (path) =>
    new Promise((success, error) =>
      methodName = 'deleteFile'
      data = [@_driveName, @_getAbsoluteFilePath(path)]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(success, error)
    )

  getDirectory: (path) =>
    path = @_getAbsoluteDirectoryPath(path)
    new Directory(path, @_drive)

  deleteDirectory: (path) =>
    new Promise((success, error) =>
      methodName = 'deleteDirectory'
      data = [@_driveName, @_getAbsoluteDirectoryPath(path)]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(success, error)
    )

  deleteDirectoryRecursively: (path) =>
    new Promise((success, error) =>
      methodName = 'deleteDirectoryRecursively'
      data = [@_driveName, @_getAbsoluteDirectoryPath(path)]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.LOCAL_MANAGER, methodName, data).then(success, error)
    )

# Represents proxy for accessing local filesystem
class Drive
  constructor: (driveName, client) ->
    @_driveName = driveName

    @_client = client

  _getDriveName: =>
    @_driveName

  _getConnection: =>
    @_client._getConnection()

  _getUserAddress: =>
    @_client._getUserAddress()

  getRoot: =>
    new Directory('/', @)



# Represents collection of functions for distributed computing purposes
class window.darwin.FunctionCollection
  constructor: ->
    @_functions = {}

  addFunction: (name, f) =>
    if typeof(f) is 'function'
      @_functions[name] = f

  toCode: =>
    code = ''
    for name, f of @_functions
      code += 'var ' + name + ' = ' + f.toString() + '\n'
    code



# Represents proxy for interacting with specified remote user
class RemoteConnection
  constructor: (remoteUserAddress, client, status='DISCONNECTED') ->
    @_remoteUserAddress = remoteUserAddress

    @_client = client

    @_connection = client._getConnection()

    @_sourceUserAddress = client._getUserAddress()

    @_status = status.toLowerCase()

    @_myPermissions = null

    @_remoteUserPermissions = null

    @_getPermissions()

  _log: () =>
    args = ['connection-proxy @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  _getPermissions: =>
    methodName = 'getRemoteUserPermissions'
    data = [@_sourceUserAddress]
    @_connection.sendRequest(@_remoteUserAddress, darwin.Config.DISPATCHER.REMOTE_MANAGER, methodName, data).then(((response) =>
      permissions = response.result
      @_myPermissions = permissions
    ))

    data = [@_remoteUserAddress]
    @_connection.sendRequest(@_sourceUserAddress, darwin.Config.DISPATCHER.REMOTE_MANAGER, methodName, data).then(((response) =>
      permissions = response.result
      @_remoteUserPermissions = permissions
    ))

  connect: (wantedPermissions) =>
    new Promise((success, error) =>
      methodName = 'performHandshake'
      data = [@_remoteUserAddress, wantedPermissions]
      @_connection.sendRequest(@_sourceUserAddress, darwin.Config.DISPATCHER.REMOTE_MANAGER, methodName, data).then(((response) =>
        success(response.result)
      ), error)

      @_getPermissions()
    )

  disconnect: =>
    new Promise((success, error) =>
    )

  getRemoteUserAddress: =>
    @_remoteUserAddress

  getStatus: =>
    @_status

  getMyPermissions: =>
    @_myPermissions

  getRemoteUserPermissions: =>
    @_remoteUserPermissions

  getDatabase: (dbName) =>
    new Database(dbName, @_remoteUserAddress, @_client)

  requestPermissions: (permissions) =>
    new Promise((success, error) =>
      methodName = 'requestPermissions'
      data = [permissions]
      @_connection.sendRequest(@_remoteUserAddress, darwin.Config.DISPATCHER.REMOTE_MANAGER, methodName, data).then((() =>
        success(true)
      ), error)
    )

  grantPermissions: (permissions) =>
    new Promise((success, error) =>
      methodName = 'grantPermissions'
      data = [@_remoteUserAddress, permissions]
      @_connection.sendRequest(@_sourceUserAddress, darwin.Config.DISPATCHER.REMOTE_MANAGER, methodName, data).then((() =>
        success(true)
      ), error)
    )

  runCode: (code, functions) =>
    new Promise((success, error) =>
      dependencies = functions.toCode()

      methodName = 'sendCode'
      data = [code, dependencies]
      @_connection.sendRequest(@_remoteUserAddress, darwin.Config.DISPATCHER.DISTRIBUTION_MANAGER, methodName, data).then(success, error)
    )



# Represents graph object
class Graph
  constructor: (id, client) ->
    @_id = id

    @_client = client

    @_connection = client._getConnection()

    @_userAddress = client._getUserAddress()

  getId: =>
    @_id

  # Obsolete
  getUuid: =>
    @_id

  isDistributed: =>
    null

  isModifiable: =>
    false



class ExplicitGraph extends Graph
  constructor: (id, client) ->
    super

  getAttributes: =>
    new Promise((success, error) =>
      methodName = 'getGraphAttributes'
      data = [@_id]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, data).then(success, error)
    )

  updateAttributes: (changes) =>
    new Promise((success, error) =>
      methodName = 'updateGraphAttributes'
      data = [@_id, changes]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, data).then(success, error)
    )

  deleteAttributes: (keys) =>
    new Promise((success, error) =>
      methodName = 'deleteGraphAttributes'
      data = [@_id, keys]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, data).then(success, error)
    )

  enumerateNodes: =>
    new Promise((success, error) =>
      methodName = 'createNodeIdsEnumerator'
      data = [@_id]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, data).then(((response) =>
        enumeratorId = response.result
        success(new ExplicitNodeEnumerator(@_id, enumeratorId, @_client))
      ), error)
    )

  ###
  Gets an array of nodes with given IDs

  @param ids Single node ID as a string or node IDs as an array.

  @return Returns a promise object containing an array of ExplicitNode objects
  ###
  getNode: (ids) =>
    new Promise((success, error) =>
      if Object.prototype.toString.call(ids) isnt '[object Array]'
        ids = [ids]

      methodName = 'checkNodesExistence'
      data = [@_id, ids]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, data).then(((response) =>
        result = response.result
        if result.length is ids.length
          nodes = []
          for i in [0..result.length - 1]
            if result[i] isnt null
              nodes.push(new ExplicitNode(@_id, result[i], @_client))
            else
              nodes.push(null)
          success(nodes)
        else
          error(new Error('Failed to get nodes, unknown error.'))
      ), error)
    )

  ###
  Creates node or nodes with given attributes

  @param attributes Single node attributes as a dictionary or multiple node attributes as an array of dictionaries.

  @return Returns a promise object containing an array of ExplicitNode objects
  ###
  createNode: (attributes={}) =>
    new Promise((success, error) =>
      if Object.prototype.toString.call(attributes) isnt '[object Array]'
        attributes = [attributes]

      methodName = 'createNodes'
      data = [@_id, attributes]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, data).then(((response) =>
        result = response.result
        if result.length is attributes.length
          nodes = []
          for i in [0..result.length - 1]
            if result[i] isnt null
              nodes.push(new ExplicitNode(@_id, result[i], @_client))
            else
              nodes.push(null)
          success(nodes)
        else
          error(new Error('Failed to create nodes, unknown error.'))
      ), error)
    )

  deleteNode: (ids) =>
    new Promise((success, error) =>
      if Object.prototype.toString.call(ids) isnt '[object Array]'
        ids = [ids]

      methodName = 'deleteNodes'
      data = [@_id, ids]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER, methodName, data).then(success, error)
    )



class ImplicitGraph extends Graph
  constructor: (id, client) ->
    super

  getAttributes: =>
    new Promise((success, error) =>
      methodName = 'getGraphAttributes'
      data = [@_id]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER, methodName, data).then(success, error)
    )

  updateAttributes: (changes) =>
    new Promise((success, error) =>
      methodName = 'updateGraphAttributes'
      data = [@_id, changes]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER, methodName, data).then(success, error)
    )

  deleteAttributes: (keys) =>
    new Promise((success, error) =>
      methodName = 'deleteGraphAttributes'
      data = [@_id, keys]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER, methodName, data).then(success, error)
    )

  enumerateNodes: =>
    new Promise((success, error) =>
      methodName = 'createNodeIdsEnumerator'
      data = [@_id]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER, methodName, data).then(((response) =>
        enumeratorId = response.result
        success(new ImplicitNodeEnumerator(@_id, enumeratorId, @_client))
      ), error)
    )

  getNode: (obj) =>
    new Promise((success, error) =>
      if typeof(obj) is 'string'
        obj = [obj]

      methodName = 'checkNodesExistence'
      data = [@_id, obj]
      @_connection.sendRequest(@_userAddress, darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER, methodName, data).then(((response) =>
        result = response.result
        if result.length is obj.length
          nodes = []
          for i in [0..result.length - 1]
            if result[i]
              nodes.push(new ImplicitNode(@_id, obj[i], @_client))
            else
              nodes.push(null)
          success(nodes)
        else
          error(new Error('Failed to get nodes, unknown error.'))
      ), error)
    )



class Enumerator
  constructor: (graphId, id, client) ->
    @_graphId = graphId

    @_id = id

    @_client = client

    @_connection = client._getConnection()

    @_userAddress = client._getUserAddress()

  hasNext: =>
    null

  next: =>
    null



class ExplicitNodeEnumerator extends Enumerator
  constructor: (graphId, id, client) ->
    super

    @_dispatcher = darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER

  hasNext: =>
    new Promise((success, error) =>
      methodName = 'hasNextNodeId'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )

  next: =>
    new Promise((success, error) =>
      methodName = 'getNextNodeId'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        nodeId = response.result
        success(new ExplicitNode(@_graphId, nodeId, @_client))
      ), error)
    )



class ExplicitEdgeEnumerator extends Enumerator
  constructor: (graphId, id, client) ->
    super

    @_dispatcher = darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER

  hasNext: =>
    new Promise((success, error) =>
      methodName = 'hasNextEdgeId'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )

  next: =>
    new Promise((success, error) =>
      methodName = 'getNextEdgeId'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        result = response.result
        startNodeId = result.startNodeId
        edgeId = result.edgeId

        success(new ExplicitEdge(@_graphId, startNodeId, edgeId, @_client))
      ), error)
    )



class ImplicitNodeEnumerator extends Enumerator
  constructor: (graphId, id, client) ->
    super

    @_dispatcher = darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER

  hasNext: =>
    new Promise((success, error) =>
      methodName = 'hasNextNodeId'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )

  next: =>
    new Promise((success, error) =>
      methodName = 'getNextNodeId'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        nodeId = response.result[0]
        success(new ImplicitNode(@_graphId, nodeId, @_client))
      ), error)
    )



class ImplicitEdgeEnumerator extends Enumerator
  constructor: (graphId, id, client) ->
    super

    @_dispatcher = darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER

  hasNext: =>
    new Promise((success, error) =>
      methodName = 'hasNextEdgeId'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )

  next: =>
    new Promise((success, error) =>
      methodName = 'getNextEdgeId'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        edgeId = response.result[1]
        success(new ImplicitEdge(@_graphId, null, edgeId, @_client))
      ), error)
    )



class Node
  constructor: (graphId, id, client) ->
    @_graphId = graphId

    @_id = id

    @_client = client

    @_connection = client._getConnection()

    @_userAddress = client._getUserAddress()

  getId: =>
    @_id

  getAttributes: =>
    new Promise((success, error) =>
      methodName = 'getNodeAttributes'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )



class ExplicitNode extends Node
  constructor: (graphId, id, client) ->
    super

    @_dispatcher = darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER

  updateAttributes: (changes) =>
    new Promise((success, error) =>
      methodName = 'updateNodeAttributes'
      data = [@_graphId, @_id, changes]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )

  deleteAttributes: (keys) =>
    new Promise((success, error) =>
      methodName = 'deleteNodeAttributes'
      data = [@_graphId, @_id, keys]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )

  enumerateInboundEdges: =>
    new Promise((success, error) =>
      methodName = 'createInboundEdgeIdsEnumerator'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        enumeratorId = response.result
        success(new ExplicitEdgeEnumerator(@_graphId, enumeratorId, @_client))
      ), error)
    )

  enumerateOutboundEdges: =>
    new Promise((success, error) =>
      methodName = 'createOutboundEdgeIdsEnumerator'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        console.warn(response);
        enumeratorId = response.result
        success(new ExplicitEdgeEnumerator(@_graphId, enumeratorId, @_client))
      ), error)
    )

  getInboundEdge: (ids) =>
    new Promise((success, error) =>
      if Object.prototype.toString.call(ids) isnt '[object Array]'
        ids = [ids]

      methodName = 'getEdges'
      data = [@_graphId, @_id, ids, 'INBOUND']
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        result = response.result
        edges = []
        for edgeId in result
          if edgeId isnt null
            edges.push(new ExplicitEdge(@_graphId, @_id, edgeId, @_client))
          else
            edges.push(null)
        success(edges)
      ), error)
    )

  getOutboundEdge: (ids) =>
    new Promise((success, error) =>
      if Object.prototype.toString.call(ids) isnt '[object Array]'
        ids = [ids]

      methodName = 'getEdges'
      data = [@_graphId, @_id, ids, 'OUTBOUND']
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        result = response.result
        edges = []
        for edgeId in result
          if edgeId isnt null
            edges.push(new ExplicitEdge(@_graphId, @_id, edgeId, @_client))
          else
            edges.push(null)
        success(edges)
      ), error)
    )

  getEdge: (ids) =>
    new Promise((success, error) =>
      error(new Error('This method is no longer supported.'))
    )

  createEdge: (data) =>
    new Promise((success, error) =>
      if Object.prototype.toString.call(data) isnt '[object Array]'
        data = [data]

      edgesData = {}
      for i in [0..data.length - 1]
        if data[i].endNodeId of edgesData
          edgesData[data[i].endNodeId].push({
            tempId: i,
            attributes: data[i].attributes
          })
        else
          edgesData[data[i].endNodeId] = [{
            tempId: i,
            attributes: data[i].attributes
          }]

      methodName = 'createEdges'
      parameters = [@_graphId, @_id, edgesData]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, parameters).then(((response) =>
        result = response.result
        if result.length is data.length
          edges = []
          for i in [0..result.length - 1]
            if result[i] isnt null
              edges.push(new ExplicitEdge(@_graphId, @_id, result[i], @_client))
            else
              edges.push(null)
          success(edges)
        else
          error(new Error('Failed to create edges, unknown error.'))
      ), error)
    )

  deleteEdge: (obj) =>
    null



class ImplicitNode extends Node
  constructor: (graphId, id, client) ->
    super

    @_dispatcher = darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER

  enumerateInboundEdges: =>
    new Promise((success, error) =>
      methodName = 'createInboundEdgeIdsEnumerator'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        enumeratorId = response.result
        success(new ImplicitEdgeEnumerator(@_graphId, enumeratorId, @_client))
      ), error)
    )

  enumerateOutboundEdges: =>
    new Promise((success, error) =>
      methodName = 'createOutboundEdgeIdsEnumerator'
      data = [@_graphId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        enumeratorId = response.result
        success(new ImplicitEdgeEnumerator(@_graphId, enumeratorId, @_client))
      ), error)
    )



class Edge
  constructor: (graphId, startNodeId, id, client) ->
    @_graphId = graphId

    @_startNodeId = startNodeId

    @_id = id

    @_client = client

    @_connection = client._getConnection()

    @_userAddress = client._getUserAddress()

  getId: =>
    @_id

  # Obsolete
  getUuid: =>
    @_id

  getAttributes: =>
    new Promise((success, error) =>
      methodName = 'getEdgeAttributes'
      data = [@_graphId, @_startNodeId, @_id]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )



class ExplicitEdge extends Edge
  constructor: (graphId, startNodeId, id, client) ->
    super

    @_dispatcher = darwin.Config.DISPATCHER.EXPLICIT_GRAPH_MANAGER

  updateAttributes: (changes) =>
    new Promise((success, error) =>
      methodName = 'updateEdgeAttributes'
      data = [@_graphId, @_startNodeId, @_id, changes]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )

  deleteAttributes: (keys) =>
    new Promise((success, error) =>
      methodName = 'deleteEdgeAttributes'
      data = [@_graphId, @_startNodeId, @_id, keys]
      @_connection.sendRequest(@_userAddress, @_dispatcher, methodName, data).then(((response) =>
        success(response.result)
      ), error)
    )



class ImplicitEdge extends Edge
  constructor: (graphId, startNodeId, id, client) ->
    super

    @_dispatcher = darwin.Config.DISPATCHER.IMPLICIT_GRAPH_MANAGER
