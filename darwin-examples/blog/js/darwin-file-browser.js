/*
FEATURES TO IMPLEMENT
---------------------

- right click menu items; 
		copy - seems to be working?
		paste - get correct name showing and append number to end if the name is taken
- arrow up down keys
- dis-activate preview on non-previewable files

*/
var appId = 'd147c9e5d875'; //app id
window.clipboard = {
    id: "",
    name: "",
    path: ""
};

//TESTING GIT.......

//Search through the DOM and find any elements with the "darwin-img-src" attribute.
var darwinImagePreview = function(path, remoteFile) {

    //get the root of the remote file storage
    var root = darwinApp.getFileStorage(path).getRoot();


    //use the value as input to the darwin file storage
    var fileToPreview = remoteFile;

    root.openImage(fileToPreview).then(function(remoteLoc) {
        //read image
        remoteLoc.read().then(function(remoteImg) {
            //get the data  and fill the src tag
            var srcData = remoteImg;

            $('.darwin-file-preview').attr('src', srcData);
        });
    }, function() {
        console.log('error opening image');
    });

}

var darwinAddImage = function(path, imagefilename, imagedata) {
    console.log("=== darwin add image started ---");
    console.log(path);
    console.log(imagefilename);
    console.log(imagedata);
    var root = darwinApp.getFileStorage(path).getRoot(); //get root

    var namestring = imagefilename;
    var splitstring = namestring.split(".");
    var newnamestring = splitstring[0] + "_2." + splitstring[1];
    root.openImage(newnamestring).then((function(img) { //open a new image file




        img.writeBase64(imagedata).then((function() { //write the base 64
            console.log('image written - OK!'); // confirm it is ok
            addPostComplete()
        }), (function() { //error handling
            console.log('Error while writing an image');
        }));
    }), (function() {
        console.log('error!');
    }));



    root.listAllFileNames().then(function(response) {
        alert(response)
    });
}

var darwinDelete = function(file) {

    //decide wither it is a directory or a file 

    //delete accordingly

    var root = darwinApp.getFileStorage('images').getRoot(); //the proxy. posts is a dictionary-like object.

    //delete wether it is a file or a directory
    root.deleteDirectory(file.name).then(function(response) {
        console.log("OK!")
    });
    root.deleteFile(file.name).then(function(response) {
        console.log("OK!")
    });

    //		delete from the UI
    $('#darwin-file-explorer').treegrid("remove", file.id);
    console.log("remove ran");




    /*	return new Promise(function (success, error) { //return a new promise - success or error
			
			var promise = root.all('posts');//get all posts
    		promise.then((function(result) {//then on complete
    			
    			console.log("RESULT IS GOING TO BE...");
    			console.log(result);
    			console.log(root.delete());

    		}), (function(err) {//error handling for the first promise
    			error(err);
    		}))
		}); //eo first promise      
	*/
}

var darwinGetImageData = function(currentPath, path, remoteFile, callback) {
    console.log("=== get image data started ===");
    console.log(currentPath);
    console.log(path);
    console.log(remoteFile);
    //get the root of the remote file storage
    var root = darwinApp.getFileStorage(path).getRoot();


    //use the value as input to the darwin file storage
    var fileToPreview = remoteFile;

    root.openImage(fileToPreview).then(function(remoteLoc) {
        //read image
        remoteLoc.read().then(function(remoteImg) {
            //get the data  and fill the src tag
            var srcData = remoteImg;

            console.log("srcData");
            console.log(srcData);
            console.log(currentPath);
            darwinAddImage(currentPath, clipboard.name, srcData);

        });
    }, function() {
        console.log('error opening image');
    });

}


$(document).on('ready', function() {


    darwinClient = new window.darwin.Client(appId); //constructor with app ID
    darwinClient.start().then(function() { //init connection with the client and the server
        window.darwinApp = darwinClient.getApplication(); //returns the proxy for interacting with client's data

        //get the root of the remote file storage


        //test area
     /*   setTimeout(function() {
                        console.log("running experiment...");

                        var root = darwinApp.getFileStorage('images').getRoot(); 
                        console.log(root);
                        
                        //show current directories
                        root.getAllDirectories().then(function(response){
                        console.log("directories: ");
                        console.log(response);
                        
                        //navigate to test directory and add test file
                        var dest_dir = root.getDirectory('test_dir');
                        dest_dir.getDirectory('sub_dir');
                        
                        dest_dir.getAllDirectories().then(function(response){
                        console.log(response);
                        })
                        });
                        
                        
            			//var test_dir = root.getDirectory('test_dir'); 
            			
            			//open directory
               			//var dirRoot = root.openFile('test_dir').then(function(){
               			//console.log("!!!!!!!!!!");
               			//});
            			
            			//console.log("dirRoot");
            			//console.log(dirRoot);
            			//make a sub directory
            			//var sub_dir = dirRoot.getDirectory('susus_dir'); 
            			
            			//display
            			//dirRoot.listAllDirectoryNames().then(function(response_list){
            			
            			//	console.log("susus" + response_list);
            			
            			//}, function(){console.log('error')});
            			
                        console.log("...ending experiment")
            
        }, 5000); */


        //Get Directory content function
        var getDirectoryContent = function(path, callback) {
        	console.log(path + "PATH");
			var dynamicRoot = darwinApp.getFileStorage("images").getRoot();
			var dest_dir = darwinApp.getFileStorage("images").getRoot(); //initially set destination directory as top-level file storage
			var patharray = path.split("/");
			
			console.log("length: " + patharray.length);
			
			if (patharray.length != 1){ //if there are some directory-level to move down...
				for (var i=0; i<patharray.length; i++){ //for each directory-level
				//dig down into the directories to retrieve the files
					dest_dir = dynamicRoot.getDirectory(patharray[i]); //set the destination directory
					console.log("dest_dir input = " + patharray[i] );
				}
			}
			
						
						//print out all the directories in the destination
                        dest_dir.getAllDirectories().then(function(response){
                        	console.log("the result of getting all directories:");
                        	console.log(response);
                        })
                        dest_dir.listAllDirectoryNames().then(function(response){
                        	console.log("the result of listing all directories:");
                        	console.log(response);
                        })
                     		
			
            
            console.log('ROOT::');
            console.log(dynamicRoot);
            console.log("--- getting all files and directories from the remote storage ---");
            console.log(path);
            var contents = {}; //array-like object to hold the file objects
                dest_dir.listAllFileNames().then(function(response) {

                    //var array = string(response).split(',');
                    console.log("file number: " + response.length);
                    contents['files'] = response;
                    if (contents['directories'] !== undefined) {
                        callback(contents, path);
                    }else{alert("no directories")}
                }, function() {
                    console.log("Error listing files!");
                });



                dest_dir.listAllDirectoryNames().then(function(response) {
                    console.log("directory number: " + response.length);
                    contents['directories'] = response;
                    if (contents['files'] !== undefined) {
                        callback(contents, path);
                    }else{alert("no files")}

                }, function() {
                    console.log("Error listing directories!");
                });
  
        };


        $('#darwin-file-explorer').treegrid({


            onContextMenu: function(e, row) {
                e.preventDefault();
                $(this).treegrid('select', row.id);
                $('#browser-context-menu').menu('show', {
                    left: e.pageX,
                    top: e.pageY,
                    onClick: function(item) {

                        var active_item = row.name;
                        var clicked_menu_item = item.text;



                        if (clicked_menu_item == "Upload File") {
                            console.log(clicked_menu_item);

                            //open local computer file browser

                            //get file path 

                            //upload
                        };

                        if (clicked_menu_item == "Copy") {
                            console.log("Copying");
                            console.log(row.id);
                            console.log(row.name);
                            console.log(row.fullpath);

                            //copy the id of the item
                            clipboard.id = row.id;
                            clipboard.name = row.name;
                            clipboard.path = row.fullpath;
                        };

                        if (clicked_menu_item == "Paste") {
                            console.log(clicked_menu_item);
                            console.log(clipboard.path);
                            console.log(clipboard.name);
                            console.log(clipboard.id);
                            var imagedata = "";
                            darwinGetImageData(row.fullpath, clipboard.path, clipboard.name);

                            //	darwinAddImage(row.path, clipboard.name, imagedata)
                            //add new version in the current directory

                        };

                        if (clicked_menu_item == "Delete") {
                            //console.log(clicked_menu_item);							
                            darwinDelete(row);
                        };
                    }

                })

            },

            //on click of a row...
            onClickRow: function(row, event) {

                //if it is a image...
                if (row.name.indexOf(".png") > -1 || row.name.indexOf(".jpg") > -1) {

                    //...display image preview

                    darwinImagePreview(JSON.stringify(row.fullpath).replace(/\"/g, ""), JSON.stringify(row.name).replace(/\"/g, ""));
                }
            },

            onLoadSuccess: function() {
                $('#darwin-file-explorer').treegrid('setTitle', 'test');
                console.log("on loads success");
            },

            //on load...
            loader: function(param, success, error) {
 			
 			var path = "images";
 
                //if there is not a parameter, it is the first, top-level-directory load.
                console.log(param);
                if (param['id'] == undefined) {
                    var data = $('#darwin-file-explorer').treegrid('getData');
                    console.log('getting data now...');
                    console.log(data);
                  

            setTimeout(function() {

                    getDirectoryContent(path, function(data, path) {

                        console.log('--- once collected, generating the JSON for use in Easy UI---');
                        ndata = [];
                        for (var k in data['files']) {
                            ndata.push({
                                id: "f" + k,
                                fullpath: path,
                                name: data['files'][k]
                            });
                        }
                        for (var k in data['directories']) {
                            ndata.push({
                                id: "d" + k,
                                fullpath: path,
                                name: data['directories'][k],
                                state: 'closed'
                            });
                        }

                        console.log(ndata);
                        success(ndata);


                    });
                    
            }, 7000);

                } else {
                console.log('else ran');
                    alert(param['id']);
                      var data = $('#darwin-file-explorer').treegrid('getData');
                    console.log('getting data now...');
                    console.log(data);
                    
                      //get the path of the corresponding data object
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].id == param.id) {
                            path = data[i].fullpath + "/" + data[i].name;
                            alert(path);
                            break;
                        }
                    }
                    
                     getDirectoryContent(path, function callback(data,path){
                     		console.log("finished!");
                     		console.log(data);
                        console.log('--- once collected, generating the JSON for use in Easy UI---');
                        ndata = [];
                        for (var k in data['files']) {
                            ndata.push({
                                id: "f" + k,
                                fullpath: path,
                                name: data['files'][k]
                            });
                        }
                        for (var k in data['directories']) {
                            ndata.push({
                                id: "d" + k,
                                fullpath: path,
                                name: data['directories'][k],
                                state: 'closed'
                            });
                        }

                        console.log(ndata);
                        success(ndata);

                     });
                     
                    //var folderid = param['id'];
                    // append some nodes to the selected row
                    //var node = $('#darwin-file-explorer').treegrid('getSelected');
                   /* $('#darwin-file-explorer').treegrid('append', {
                        parent: folderid,
                        data: [{
                            id: "d0",
                            name: 'test_append'
                        }]
                    });*/


                    return false;
                }

                console.log('path ' + path);


                //success(data);
            },

            idField: 'id',

            treeField: 'name',
            columns: [
                [{
                    title: 'Name',
                    field: 'name',
                    width: 700
                }, ]
            ]
        });



    });




});