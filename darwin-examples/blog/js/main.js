var appId = 'd147c9e5d875'; //app id
var darwinClient = null; //client object
var darwinApp = null; //app object
var blog = null; //blog object
var remoteBlogs = null; //remote blogs
var blogTitle = "Darwin P2P Blog";
var readingTitle = "";
var selectedConnection = "brunokam@mbk.iriscouch.com"

var attachEventHandlers = function() { //event handlers
    $('div#new-post button').on('click', function(e) { //new post button click
    
        var imagefilename="img-"+(new Date()).getTime().toString(16)+".png";
        
        var root = darwinApp.getFileStorage('images').getRoot(); //get root
   	     root.openImage(imagefilename).then((function(img) { //open a new image file
    	  img.writeBase64($('#image-input').val()).then((function() {//write the base 64
    	  	  console.log('image written - OK!'); // confirm it is ok
    		  addPostComplete()
    	   }), (function() { //error handling
    		  console.log('Error while writing an image');
    	   }));
         }), (function() {
    	   console.log('error!');
        }));
        
        
        var post = { //create new post
            title: $('div#new-post input[type="text"]').val(), //with the title
            image: imagefilename,
            content: $('div#new-post textarea').val() //with the content
        };
       
       function addPostComplete() {
          blog.addPost(post).then(function(result) { //run the add post with callback
           location.reload(); //refresh page to see the result
          }, function(err) { //error message
           console.log(err.message);
          });
        }
    
    

    });
    
    
    $('#tab-me').on('click', function(e){
    	meView();		
    });
    
    $('#tab-reading').on('click', function(e){
    	readingView();
    });
    
    $('#tab-compose').on('click', function(e){
    	composeView();
    });
};

var startRemoteLoadingIcon = function(){
	$("#loading").show();
};

var endRemoteLoadingIcon = function(){
	$("#loading").hide();
};
/*
var fetchAllRemotePostsInterval = function(){
		setInterval(function() {
		console.log("running this thing");
     		var promise = blog.fetchAllRemotePosts();
     	},5000); // end of set interval
     	return promise;
}
*/     	
     	
$(document).on('ready', function () { //WHEN DOCUMENT IS READY..
   if (window.location.href.indexOf('?') == -1){ //'ME'
        	//load 'me'
	meView();
	}else{
	youView();
	}
	
	
Dropzone.options.postImageDropzone = {
	// The configuration we've talked about above
  		autoProcessQueue: false,
  		maxFiles: 1,
  		dictDefaultMessage: "<div class='dropzone-text'><i class='ion-image'></i><br/> drop image here</div>",
  		addRemoveLinks: true,
  		thumbnailWidth: null,
  		thumbnailHeight: 150,

  	init: function() {
    
    //on upload of thumbnail image we can get the data url
    this.on("thumbnail", function(file, dataUrl) { 
    console.log(dataUrl);//test//
    
    //hide default message
    $(".dz-message").hide();
    
    //update the hidden input field in the new post form
    $("#image-input").val(dataUrl);
    
	//NOTE: Moved image upload code to after submit button is pressed
	
    });
    

  	}
	};

	
    darwinClient = new window.darwin.Client(appId); //constructor with app ID
    darwinClient.start().then(function () { //init connection with the client and the server
        darwinApp = darwinClient.getApplication(); //returns the proxy for interacting with client's data
        darwinGetImages();
        blog = new BlogModel(); //new BlogModel Object
    
        //LOAD 'MY BLOG'       
        if (window.location.href.indexOf('?') == -1){ //'ME'
        	//load 'me'
        	blog.fetchAllPosts(selectedConnection).then(function (posts) { //fetch all posts from the BlogModel
              	console.log(posts);
              	var postCollection = new PostCollectionView({ //create a new post collection view
                el: $('div#posts div.collection'), //the HTML will look like this
                collection: blog
            	});
            postCollection.render(); //render the view
            
            readingTitle = selectedConnection;
            $(".page-header").html(readingTitle);

            });
//LOAD 'READER BLOG'
        }else{ 
        	startRemoteLoadingIcon();
        	//get the location of the blog to be loaded
        	var split = window.location.href.split('?');
        	var selectedConnection = split[1];
        	
        	blogTitle = selectedConnection;
        	$(".page-header").html(blogTitle);

        	       	
        	//setTimeout(function() {
        	darwinClient.registerEventHandler("remote-connections-initialised", function(){
        			blog.fetchAllRemotePosts(selectedConnection).then(function (posts) { //fetch all posts from the BlogModel
            	 		var postCollection = new PostCollectionView({ //create a new post collection view
                			el: $('div#posts div.collection'), //the HTML will look like this
                			collection: blog
            			});
            		postCollection.render(); //render the view  
        			});
        			endRemoteLoadingIcon();  
        	});
        	}

 	  remoteBlogs2 = new RemoteBlogsModel(); //create a new remoteBlogsModel
 
        remoteBlogs = new RemoteBlogsModel(); //create a new remoteBlogsModel
     //},5000); // end of set interval
 		//blog.deleteAllPosts(); //testing the function


        var connectionCollection = new ConnectionCollectionView({
            el: $('div#remote-blogs div.collection'), //the HTML will look like this
            collection: remoteBlogs
        });
        
        var connectionCollection2 = new ConnectionCollectionView({
            el: $('div.collection2'), //the HTML will look like this
            collection: remoteBlogs
        });
        
        remoteBlogs.refreshConnections(function (connections) { //refresh the remote connections
            connectionCollection.collection = remoteBlogs; //
            connectionCollection.render(); //render
            darwinGetImages();
        });
        
        remoteBlogs2.refreshConnections(function (connections) { //refresh the remote connections
            connectionCollection2.collection = remoteBlogs; //
            connectionCollection2.render(); //render
        });

        attachEventHandlers(); //attach event handlers
    });
});