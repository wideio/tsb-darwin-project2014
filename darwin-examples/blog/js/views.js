/* ~~~~~~~~~~~~ TAB VIEWS ~~~~~~~~~~ */

var meView = function(){

    	$(".nav-tab").removeClass("nav-selection");
		$("#tab-me").addClass("nav-selection");
		
		//set blog title
		$(".page-header").html(blogTitle);
		
		//orchestrate components of the UI
		//hide all divs that are not part of this view
		$("#new-post").hide();
		$('.collection2').hide();
		//show the ones that are
		$('#title-row').show();
		$("#widgets-sidebar").show();
		$("#blog-entries").show();
		
}

var youView = function(){
		$(".nav-tab").removeClass("nav-selection");
    	$("#tab-reading").addClass("nav-selection");
    	

		//set blog title
		$(".page-header").html(blogTitle);
		
		//orchestrate components of the UI
		//hide all divs that are not part of this view
		$("#new-post").hide();
		$('.collection2').hide();
		//show the ones that are
		$('#title-row').show();
		$("#widgets-sidebar").show();
		$("#blog-entries").show();
}

var readingView = function(readingTitle){
    	$(".nav-tab").removeClass("nav-selection");
    	$("#tab-reading").addClass("nav-selection");
    	
		//set blog title
		$(".page-header").html("<i class='ion-ios7-bookmarks'></i>&nbsp; Reading");
		
		//orchestrate components of the UI
		//hide all divs that are not part of this view
		$("#widgets-sidebar").hide();
		$("#blog-entries").hide();
		$("#new-post").hide();
				
		//show the ones that are	
		$('#title-row').show();
		$('.collection2').show();
}
	
var composeView = function(){

		$(".nav-tab").removeClass("nav-selection");
    	$("#tab-compose").addClass("nav-selection");
    	
    	
    	$(".nav-tab").removeClass("nav-selection");
    	$(this).addClass("nav-selection");
		//change the page header
		$(".page-header").html("<i class='ion-compose'></i>&nbsp; Compose A New Post");
		
		//orchestrate components of the UI
		//hide all divs that are not part of this view
		$("#widgets-sidebar").hide();
		$("#blog-entries").hide();
		$('.collection2').hide();		
		//show the ones that are
		$("#new-post").show();
}
	
/* ~~~~~~~~ SUB VIEWS ~~~~~~~~~~ */

var PostView = Backbone.View.extend({
    initialize: function () { //initialise...
        this.$el = $('<div class="post"></div>'); //create a post div
    },

    render: function () {
    console.log('IMG F N: ' + this.model.get('image'));
        var variables = { //render...
            title: this.model.get('title'), //set title
            image: this.model.get('image'),//this.model.get('image'), 
            content: this.model.get('content'), //set content
            created_at: this.model.get('created_at'), //set content 
        };

        var template = _.template($('div#posts script#post-template').html(), variables);// template
        this.$el.html(template);

        return this.$el;
    }
});

var PostCollectionView = Backbone.View.extend({ //collection of post views
    initialize: function () { //initialise
        var that = this;

        this._postViews = [];
        this.collection.each(function (post) {
            that._postViews.push(new PostView({
                model: post
            }));
        });
    },

    render: function () { //render
        var that = this;

        this.$el.empty();
        this._postViews.every(function (post) {
            that.$el.append(post.render());
            return true;
        });
    }
});

var ConnectionView = Backbone.View.extend({ //connection view
    initialize: function () { //initialise
        this.$el = $('<a href="index.html?brunokam@mbk.iriscouch.com"><div class="connection"></div></a>');
    },

    render: function () {//render
        var variables = {
            address: this.model.get('address')
        };

        var template = _.template($('div#remote-blogs script#connection-template').html(), variables);
        this.$el.html(template);

        return this.$el;
    }
});

var ConnectionCollectionView = Backbone.View.extend({//a collection of connection views
    initialize: function () {}, //initialise

    render: function () { //render
        var that = this;

        this.$el.empty();
        this.collection.each(function (connection) { //for each connection
            connectionView = new ConnectionView({ 
                model: connection
            });
            that.$el.append(connectionView.render());
            return true;
        });
    }
});



