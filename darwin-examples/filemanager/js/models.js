var PostModel = Backbone.Model.extend({
    initialize: function () {} //empty(? perhaps to fill with an array of post objects)
});

var BlogModel = Backbone.Collection.extend({ //a collection is an ordered set of models
    model: PostModel,

	deleteAllPosts: function () { //DELETES ALL THE POSTS
		var that = this;
		
		
		return new Promise(function (success, error) { //return a new promise - success or error
			var proxy = darwinApp.getDatabase('posts'); //the proxy. posts is a dictionary-like object.
			
			var promise = proxy.all('posts');//get all posts
    		promise.then((function(result) {//then on complete
    			var batch = proxy.getWriteBatch();//batches are multiple api calls. Return the write batch.
    			for (var i = 0; i < result.length; ++i) {//for the length of the posts returned by the promise
    				var id = result[i]._id;//get the id of each result in turn
    				batch.addDelete(id);//add the delete action to the batch
    			}

    			var promise2 = batch.submit();//promise a batch submit
    			promise2.then((function() {//success
    				success();
    			}), (function(err) {//error handling
    				error(err);
    			}))
    			
    		}), (function(err) {//error handling for the first promise
    			error(err);
    		}))
		}); //eo first promise
	},//eo delete all posts
	
    fetchAllPosts: function () {//FETCHES ALL THE POSTS
        var that = this; //get 'this' in scope to use later

        return new Promise(function (success, error) {//a new promise
            var proxy = darwinApp.getDatabase('posts'); //get posts
            
            
        	///////////////////////////////////    
 
   	  /*   
   	     
   	     root.openImage(imagefilename).then((function(img) { //open a new image file
    	  img.writeBase64($('#image-input').val()).then((function() {//write the base 64
    	  	  console.log('OK!'); // confirm it is ok
    		  addPostComplete()
    	   }), (function() { //error handling
    		  console.log('Error while writing an image');
    	   }));
         }), (function() {
    	   console.log('error!');
        }));*/
        //////////////////////////////////
        
        
        
            proxy.all().then((function (result) { 
                var response = []; //response is going to be an array things
                for (var i = 0; i < result.length; ++i) { //for the number in the length  
                    var doc = result[i]; //gets the individual post in order in the blogs format
                    
                    
                    //for each doc image, change for the link to the file
                    //console.log(doc.image);
                    //open the image
                   
                   	
            
        
                    var post = { //a post object
                        title: doc.title, //is made up on a title (from the keys)
                        image: doc.image,

                        content: doc.content, // and content
                        created_at : doc.created_at,
                        update_at: doc.update_at
                    };
                    that.add(new PostModel(post)); //add a new post model with the post data
                    response.push(post);//add the post object to the response collection
                    
                }

                if (typeof(success) === 'function') { //if a success
                    success(response);
                }
            }), (function (err) {
                if (typeof(error) === 'function') { //if an error
                    error(err);
                }
            }));
        });
    },
 
    
    fetchAllRemotePosts: function (selectedConnection) {//FETCHES ALL THE POSTS
    
    	console.log("fetching all remote posts...");
        var that = this; //get 'this' in scope to use later

        return new Promise(function (success, error) {//a new promise
        //setInterval(function() {
        	//get remote connection
        	var conProxy = darwinClient.getRemoteConnection(selectedConnection);//nieve key implementation
        	//conProxy.connect();
        	console.log('printing db');
        	var dbProxy = conProxy.getDatabase('posts');
        	//console.log (dbProxy);
        
        
            //var proxy = darwinApp.getDatabase('posts'); //get posts
            
            dbProxy.all().then((function (result) {
            	console.log(result);
                var response = []; //response is going to be an array things
                for (var i = 0; i < result.length; ++i) { //for the number in the length  
                    var doc = result[i]; //gets the individual post in order in the blogs format
                    var post = { //a post object
                        title: doc.title, //is made up on a title (from the keys)
                        content: doc.content, // and content
                        image: doc.image
                    };

                    that.add(new PostModel(post)); //add a new post model with the post data
                    response.push(post);//add the post object to the response collection
                    
                }

                if (typeof(success) === 'function') { //if a success
                    success(response);
                }
            }), (function (err) {
                if (typeof(error) === 'function') { //if an error
                    error(err);
                }
                console.log('error');
            }));
  //          },5000); // end of set interval
        });
        
     
        
    },
    
    addPost: function (post) { //ADDS A POST
        var that = this;
        post["created_at"]=new Date();//.getTime().toString(16);
        post["updated_at"]=new Date();//.getTime().toString(16);
        return new Promise(function (success, error) {
            var proxy = darwinApp.getDatabase('posts'); //get proxy to work with
            proxy.set(new Date().getTime().toString(16), post).then(function (result) {//set of unique values of any type
                that.models.push(new PostModel(post)); //add a new post model to the main object (BlogModel)
                success(result);
            }, error);
        });
    }
});



var ConnectionModel = Backbone.Model.extend({//CONNECTION MODEL
    initialize: function () {} //empty to start
});

var RemoteBlogsModel = Backbone.Collection.extend({//REMOTE BLOGS MODEL
    model: ConnectionModel,

    refreshConnections: function (callback) { //function refreshes connections inorder to update
        var that = this;

        setInterval(function () { //at a specified interval
            darwinClient.getAllRemoteConnections().then(function (result) { // get all remote connections
                for (var address in result) {
                    var remoteConn = { //blog remote connections model //create a remote connection for each one
                        address: address
                    };
        
                    that.models = []; 
                    that.models.push(new ConnectionModel(remoteConn)); //add each new connection model to that
                }
                callback(result);
            });
        }, 5000);
    }
});
