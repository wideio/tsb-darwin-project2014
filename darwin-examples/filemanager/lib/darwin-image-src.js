/*		  
								    
			::::		---	DARWIN IMAGE TAG WIDGET ---			::::

hook into this functionality by including:
data-darwin-src="" 
on your element & place the file name


*/


//Search through the DOM and find any elements with the "darwin-img-src" attribute.
var darwinGetImages = function() {

    //get the root of the remote file storage
    var root = darwinApp.getFileStorage('images').getRoot();

    //for each element with the tag; replace their
    $('img[data-darwin-src]').each(function() {

        var $this = $(this);
        var $that = $this;

        //get remote file name
        var remoteFile = $this.data('darwin-src');

        //use the value as input to the darwin file storage
        //open image
        root.openImage(remoteFile).then(function(remoteLoc) {
            //read image
            remoteLoc.read().then(function(remoteImg) {

                //get the data  and fill the src tag
                var srcData = remoteImg;
                $that.attr('src', srcData);
            });
        }, function() {
            console.log('error opening image');
        });

    });
}




    




