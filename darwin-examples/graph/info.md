
USAGE
=====

client = window.darwin.client('a147f962c01a')
promise = client.start()
graph = app.getDatabase('graph')
doc = graph.get(graph(1|2|3|4)) # <-- return promise object
doc.then(success_fct, error_fct)

connection to client
--------------------

addr = adrien@mbk.iriscouch.com
remoteConn = client.getRemoteConnection(addr)
permissions = ["FETCH", "MODIFY", "MEDIATE"]
promise = remoteConn.connect(permissions)
          remoteConn.getDatabase("graph")

sqrt = (n) ->
    return Math.sqrt(n)

code = "
n = 10;
return sqrt(n);
"

function_col = new FunctionCollection
function_col.addFunction("sqrt", sqrt)

promise = remoteConn.run(code, functions_col)


HELP
----

have to check if promise successfull before continue...
wait(promise)
