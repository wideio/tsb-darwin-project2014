clientID = 'a147f962c01a'
db_name = 'graph'
permissions = ['FETCH', 'MODIFY', 'MEDIATE']
sleep = 50

gen_uuid = () ->
    'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) ->
        r = Math.random() * 16|0
        if c == 'x'
            v = r
        else
            v = (r&0x3|0x8)
        return v.toString(16)
    )

class window.Conn
    constructor: () ->
        @client = new window.darwin.Client(clientID)
        @remotes_conn = {}
        promise = @client.start()
        promise.then(() =>
            @conn = @client.getApplication()
            @proxy = @get_proxy(@conn, db_name)
        )

    get_proxy: (conn, name) ->
        return conn.getDatabase(name)

    get_documents: (proxy, filters) ->
        res = {}
        if filters is undefined
            proxy.all().then( (result) ->
                for r in result
                    res[r._id] = r
            )
        else
            for filter in filters
                # FIXME: get -> filter
                proxy.filter(name).then((result) ->
                    # FIXME new graph if key == **_graph
                    for r in result
                        res[r._id] = r
                )
        return res

    get_document: (proxy, key) ->
        # FIXME: promise wait before return
        res = undefined
        proxy.get(key).then( (result) ->
            # FIXME new graph if key == **_graph
            res = result
        )
        return res
        

    get_remote: (addr, permissions) ->
        if permissions is undefined
            permissions = ['FETCH', 'MODIFY', 'MEDIATE']
        if addr in @remotes_conn
            return @remotes_conn[addr]
        remote_conn = @client.getRemoteConnection(addr)
        remote_conn.connect(permissions).then(() ->
            @remotes_conn[addr] = remote_conn
        , () ->
            remote_conn = undefined
        )
        return remote_conn

    set: (proxy, set_list) ->
        keys = Object.keys(set_list)
        i = 0
        result = []
        lambda = (elem) ->
            setTimeout(( () ->
                k = elem
                v = set_list[elem]
                console.log(k, v)
                result.push proxy.set(k, v).then(sampleSuccess, sampleError)
                ++i
                if i < keys.length
                    lambda(keys[i])
            ), sleep)
        lambda(keys[i])
        return result

    update: (proxy) ->
        return proxy.sync()

class window.Graph
    constructor: (@name, @description) ->
        @uuid = gen_uuid()
        if @name is undefined
            @name = @uuid
        @nodes = []
        @edges = []

    _get_node: (uuid, db, conn) ->
        if uuid in db
            n = db[uuid]
        else
            n = conn.get_key(conn.proxy, uuid)
        node = @new_node(conn.addr, n['data'], undefined, undefined, uuid)
        return node

    _get_edge: (uuid, db, conn) ->
        get_node = (uuid) ->
            for n in @nodes
                if n.uuid == uuid
                    return n
        if uuid in db
            e = db[uuid]
        else
            e = conn.get_key(conn.proxy, uuid)
        e_in = {
            node: get_node(e['in']['node'])
            addr: e['in']['addr']
            graph: e['in']['graph']
        }
        e_out = {
            node: get_node(e['out']['node'])
            addr: e['out']['addr']
            graph: e['out']['graph']
        }
        edge = @new_egde(e_in, e_out, e['uuid'], e['data'])
        edge.out.node.edge_in.push edge
        edge.in.node.edge_out.push edge
        return edge

    from_documents: (conn, docs, key) ->
        @uuid = key
        @nodes = (@_get_node(n, docs, conn) for n in docs[key]['nodes'])
        @edges = (@_get_edge(e, docs, conn) for e in docs[key]['edges'])

    set: () ->
        set_list = {}
        key = @name + "__graph"
        graph = {
            'uuid': @uuid
            'name': @name
            'nodes': (n.uuid for n in @nodes)
            'edges': (e.uuid for e in @edges)
            'description': @description
        }
        set_list[key] = graph
        for n in @nodes
            data = n.data
            if data
                set_list[data.uuid + "__data"] = data
                data = data.uuid
            node = {
                'uuid': n.uuid
                'edges_in': (e.uuid for e in n.edges_in)
                'edges_out': (e.uuid for e in n.edges_out)
                'data': data
                'addr': n.addr
            }
            set_list[n.uuid + "__node"] = node
        for e in @edges
            data = e.data
            if data
                set_list[data.uuid + "__data"] = data
                data = data.uuid
            edge = {
                'uuid': e.uuid
                'in': {
                    'addr': e.in.addr,
                    'graph': e.in.graph,
                    'uuid': e.in.uuid
                }
                'out': {
                    'addr': e.out.addr,
                    'uuid': e.out.uuid
                    'graph': e.out.graph,
                }
                'data': data
            }
            set_list[e.uuid + "__edge"] = edge
        return set_list

    get_local_nodes: (uuids) ->
        res = {}
        for node in @nodes
            for uuid in uuids
                if uuid == node.uuid
                    res[uuid] = node
                if uuids.length == res.length
                    return res
        return res

    # link fixed
    link: (n_in, n_out, data) ->
        ///
            n_in: {
                node: node  <-- if node we do not looking for it
                uuid: uuid  <-- if node uuid is do not used
                addr: toto@domain.com <-- error if empty
                graph: my_graph <-- take the current graph if empty
            } same for n_out
            data <-- data's edge 
        ///
        # FIXME: here we should take an addr and check if the node 
        # addr is not the same, then we do not have to get it from
        # get_local_nodes
        if n_in.node is undefined
            n_in.node = @get_local_nodes([n_in.uuid])[n_in.uuid]
        if n_out.node is undefined
            n_out.node = @get_local_nodes([n_out.uuid])[n_out.uuid]
        edge = @new_edge(n_in, n_out, data)
        if edge is undefined
            return false
        if n_in.node
            n_in.node.edges_out.push edge
        if n_out.node
            n_out.node.edges_in.push edge
        @edges.push edge
        return true

    links: (nodes_in, nodes_out, datas) ->
        i = 0
        len_in = nodes_in.length
        len_out = nodes_out.length
        if len_in != len_out
            return -1
        while i < len_in
            data = undefined
            if datas
                data = datas[i]
            if not link(nodes_in[i], nodes_out[i], data)
                return i
            i += 1
        return i

    new_data: (data, uuid) ->
        if uuid is undefined
            uuid = gen_uuid()
        return {
            uuid: uuid
            data: data
        }

    new_edge: (d_in, d_out, uuid, data) ->
        /// 
        d_in and d_out ==
        {
            node: node
            addr: 'name@domain.com'
            graph: graph.name
            uuid: uuid
        }
        data should be an uuid or something
        ///
        for node in [d_in, d_out]
            check = [node.uuid, node.addr, node.graph]
            for c in check
                if c is undefined
                    console.log('parameter required undefined')
                    return undefined
        if uuid is undefined
            uuid = gen_uuid()
        return {
            uuid: uuid
            in: d_in
            out: d_out
            data: data
        }

    new_node: (addr, data, edges_in, edges_out, uuid) ->
        ///
            data should be a dict with an uuid like
            {
                'uuid': uuid
                ....
            }
        ///
        check = [addr]
        for c in check
            if c is undefined
                return undefined
        if uuid is undefined
            uuid = gen_uuid()
        if edges_in is undefined
            edges_in = []
        if edges_out is undefined
            edges_out = []
        n = {
            uuid: uuid
            edges_in: edges_in
            edges_out: edges_out
            data: data
            addr: addr
        }
        @nodes.push n
        return n

    new_nodes: (datas, addrs) ->
        list = []
        for i in [0..datas.length]
            list.push @new_node(addrs[i], datas[i])
        return list

    delete_node: (uuid) ->
        i = nodes.length()
        while i > 0 and uuid != node.uuid
            i -= 1
        if i > -1
            nodes.splice(i, 1)
            return true
        return false

    delete_nodes: (uuids, error) ->
        for uuid in uuids
            res = @delete_node(uuid)
            if res is false
                if error and typeof(error) is 'function'
                    error(this, uuid)
                else
                    return uuid
        return true

    _pagerank: (graph, conn, iter, fct) ->
        def = 1.00
        dampFac = 0.825
        d = 1 - dampFac
        nodes = graph.nodes
        pr = (def for n in nodes)
        for x in [0..iter]
            for i in [0..graph.nodes.length]
                if node[i]['PR'] is undefined
                    node[i]['PR'] = def
                res = 0
                for e in node.edges_in
                    res += fct(e, x)
                pr[i] = dampFac + d * res
            for i in [0..graph.nodes.length]
                node[i]['PR'] = pr[i]

    local_pagerank: (graph, conn, iter) ->
        get_PR = (edge, x) ->
            if edge.in.addr != conn.addr
                return 1.00
            node = edge.in.node
            if node['PR'] is undefined
                node['PR'] = 1.00
            return node['PR'] / node.edge_out.length
        @_pagerank(graph, conn, iter, get_PR)

    full_pagerank: (graph, conn, iter, limit) ->
        def = 1.00
        if limit is undefined
            limit = 2

        list_addr = {}
        fct_run_cmd = (node_uuid, addr, graph, x_iter) ->
            if addr not in list_addr
                list_addr[addr] = x_iter
            else
                if list_addr[addr] >= x_iter
                    # FIXME: return key  from addr
                    remote = conn.get_remote(addr)
                    proxy = get_proxy(remote)
                    db = conn.get_db(proxy, node_uuid + "__uuid")
                    return db[node_uuid + "__node"]['PR']
                else
                    list_addr[addr] = x_iter
            remote = conn.get_remote(addr)
            if remote is undefined
                return def
            fcts = new FunctionCollection()
            fcts.addFunction('full_pagerank', full_pagerank)
            fcts.addFunction('Graph', Graph)
            fcts.addFunction('Conn', Conn)
            code = "
            conn = new Conn();
            name = \"" + graph + "__graph\";
            db = window.Conn.get_db(conn.proxy, name);
            g = window.Graph.from_db(conn, db, name);
            full_pagerank(graph, conn, 1, " + (limit - 1).toString + ");
            for (var i = 0; i < g.nodes.length; ++i) {
                if (g.nodes[i].uuid == " + node_uuid + "){
                    return g.nodes[i]['PR'];
                }
            }
            return 1.00;
            "
            promise = remote.run(code, fcts)
            res = null
            promise.then((result) ->
                res = result
            )
            return res

        fct = (edge, x_iter) =>
            if edge.in.addr != conn.addr
                return fct_run_cmd(edge.in.node, edge.in.addr, edge.in.graph, x_iter)
            node = edge.in.node
            if node['PR'] is undefined
                node['PR'] = def
            return node['PR'] / node.edge_out.length

        @_pagerank(graph, conn, iter, fct)

    graph_clone: (fct, conn) ->
        return 0

    graph_modify: (fct, conn) ->
        return 0

    graph_result: (fct, conn) ->
        res = {}
        fcts = new FunctionCollection()
        fcts.addFunction('fct', fct)
        fcts.addFunction('Graph', Graph)

        funct = (addr, graph) ->
            addr_visited[addr] = true
            remote = conn.get_remote(addr)
            return db[node_uuid + "__node"]['PR']
            # there we have to think:
            code = "
            conn = new Conn();
            name = \"" + @name + "__graph\";
            db = window.Conn.get_db(conn.proxy, name);
            g = window.Graph.from_db(conn, db, name);

            g.
            fct()
            full_pagerank(graph, conn, 1, " + (limit - 1).toString + ");
            for (var i = 0; i < g.nodes.length; ++i) {
                if (g.nodes[i].uuid == " + node_uuid + "){
                    return g.nodes[i]['PR'];
                }
            }
            return 1.00;
            "
            promise = remote.run(code, fcts)

        addr_visited = {}
        addr_visited[conn.addr] = true
        for node in @nodes
            res.push fct(node)
            for e in node.edges_in
                if e.in.addr not in addr_visited
                    funct(e.in.addr, e.in.graph)
            for e in node.edges_out
                if e.out.addr not in addr_visited
                    funct(e.out.addr, e.out.graph)
        return res

    propagate_modify: () ->
        ///
        update each nodes from the current node with the fct to apply
        ///
        return 0

    propagate_clone: () ->
        /// 
        make a new graph with nodes modified by fct
        ///
        return 0

    propagate_result: (fct, conn, node)  ->
        ///
        return a dict of result as [node_uuid] : result
        ///
        # for now the propagate work on edges -> out
        # we can changed it after
        return 0

example = () ->
    nodes = [1, 2, 3, 4]
    addrs = ['adrien@','adrien@','adrien@','adrien@']
    links = [[1, 2], [1,3], [2,3], [3,4], [4,1]]
    g = new window.Graph()
    for i in [0..(nodes.length - 1)]
        n = g.new_node(addrs[i], g.new_data(nodes[i]))
        nodes[i] = n.uuid
        for x in [0..(links.length - 1)]
            for y in [0..(links[x].length - 1)]
                if i + 1 == links[x][y]
                    links[x][y] = n.uuid
    for link in links
        n_in = {
            'uuid': link[0]
            'addr': 'adrien@'
            'graph': g.name
        }
        n_out = {
            'uuid': link[1]
            'addr': 'adrien@'
            'graph': g.name
        }
        g.link(n_in, n_out)
    return g

conn = new window.Conn()
