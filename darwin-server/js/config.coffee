darwin.Config = {
  CRYPTOGRAPHY: {
    ENABLED: false # temporary
  },

  CONNECTION: {
    STATUS: {
      NEW: 'NEW',
      DISCONNECTED: 'DISCONNECTED',
      CONNECTED: 'CONNECTED'
#      UNAUTHORISED: 'UNAUTHORISED'
    },

    TIMEOUT: 5000
  },

  PERMISSION: {
    FETCH: 'FETCH',
    MODIFY: 'MODIFY',
    MEDIATE: 'MEDIATE'
  },

  FILE_STORAGE: {
    MAX_SLICE_SIZE: 1024 #65536
  }

  GRAPH: {
    TYPE: {
      EXPLICIT: 'explicit',
      IMPLICIT: 'implicit'
    },

    DISTRIBUTION: {
      CONSOLIDATED: 'consolidated',
      DISTRIBUTED: 'distributed'
    },

    EDGE_TYPE: {
      INBOUND: 'INBOUND',
      OUTBOUND: 'OUTBOUND'
    },

    IMPLICIT: {
      FUNCTION: {
        NODE_EXISTS: 'nodeExists',
        ENUMERATE_NODE_IDS: 'enumerateNodeIds',
        ENUMERATE_NODE_INBOUND_EDGE_IDS: 'enumerateNodeInboundEdgeIds',
        ENUMERATE_NODE_OUTBOUND_EDGE_IDS: 'enumerateNodeOutboundEdgeIds',
        GET_NODE_ATTRIBUTES: 'getNodeAttributes',
        GET_EDGE_ATTRIBUTES: 'getEdgeAttributes'
      }
    }
  }

  REPLICATION: {
    DIRECTION: {
      FROM: 'FROM',
      TO: 'TO'
    }
  },

  DISPATCHER: {
    DATABASE_MANAGER: 'DatabaseManager',
    LOCAL_MANAGER: 'LocalManager',
    REMOTE_MANAGER: 'RemoteManager',
    DISTRIBUTION_MANAGER: 'DistributionManager',
    EXPLICIT_GRAPH_MANAGER: 'ExplicitGraphManager',
    IMPLICIT_GRAPH_MANAGER: 'ImplicitGraphManager'
  }
}
