class Core extends darwin.EventProvider
  constructor: ->
    super

    # Address of the user which uses this application
    @_userAddress = null

    # Password of the user which uses this application
    @_password = null

    # ID of the application
    @_appId = null

    # Peer ID of the application
    @_peerId = null

    # Determines whether the user is signed in or not
    @_userStatus = false

    # Application's drive directories
    @_drives = new darwin.DriveCollection

#    @_offlineQueue = []
#
#    setInterval(@_handleOfflineQueue, 5000)

  # Logs messages
  _log: () =>
    args = ['core @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  # Retrieves application's database names from core databases and saves it here
  _initialiseAppSchema: =>
    new Promise((success, error) =>
      databasesDb = @_coreDbs.getController('applications-databases')

      # Synchronises applications-databases - it is called if the first attempt of initialising fails
      customError = () =>
        databasesDb.sync().then((() =>
          operations(error)
        ), ((err) =>
          error(err)
        ))

      operations = (err) =>
        databasesDb.get(@_appId).then(((doc) =>
          @_appDbNames = doc['db-names']
          success()
        ), err)

      operations(customError)
    )

  # Retrieves drive database names from core databases and saves it here
  _initialiseDriveSchema: =>
    new Promise((success, error) =>
      drivesDb = @_coreDbs.getController('drives')

      drivesDb.all().then(((docs) =>
        @_driveDbNames = []
        for doc in docs
          @_driveDbNames.push(doc._id)
        success()
      ), error)
    )

#  _handleOfflineQueue: =>
#    setTimeout((() =>
#      if navigator.onLine and @_offlineQueue.length > 0
#        @executeQueue()
#    ), 5000)

  # Simply gets user address
  getUserAddress: =>
    @_userAddress

  # Simply gets user password
  # TODO: Check if it is safe
  getPassword: =>
    @_password

  # Simply gets application ID
  getAppId: =>
    @_appId

  # Simply gets application's peer ID
  getPeerId: =>
    @_peerId

  # Simply gets user's status
  getUserStatus: =>
    @_userStatus

#  # Simply gets core database controllers
#  getCoreDbs: =>
#    @_coreDbs
#
#  # Simply gets drive database controllers
#  getDriveDbs: =>
#    @_driveDbs

  # Simply gets drive controllers
  getDrives: =>
    @_drives

#  # Simply gets application's database controllers
#  getAppDbs: =>
#    @_appDbs
#
#  # Simply gets application's hidden database controllers
#  getAppHiddenDbs: =>
#    @_appHiddenDbs

  # Simply sets user address
  setUserAddress: (userAddress) =>
    @_userAddress = userAddress
    @_triggerEvent('initialise-user-address-success')

  # Simply sets user password
  setPassword: (password) =>
    @_password = password

  # Simply sets application ID
  setAppId: (appId) =>
    if @_appId is null
      @_appId = appId

  # Simply sets application's peer ID
  setPeerId: (peerId) =>
    @_peerId = peerId
    @_triggerEvent('initialise-peer-id-success')

  # Simply sets user's status to true
  setupUserStatus: =>
    @_userStatus = true

  # Simply sets user's status to false
  resetUserStatus: =>
    @_userStatus = false

  # Simply sets drive database names
  setDriveDbNames: (dbNames) =>
    @_driveDbNames = dbNames
    @_driveDbs.setDbNames(@_driveDbNames)

#  queueMethod: (scope, f, args) =>
#    @_offlineQueue.push({
#      scope: scope,
#      f: f,
#      args: args
#    })
#
#  executeQueue: =>
#    for item in @_offlineQueue
#      scope = item.scope
#      f = item.f
#      args = item.args
#
#      f.apply(scope, args)
#    @_offlineQueue = []

  # Authenticates given credentials in Darwin
  # TODO: Remember to authenticate again when Internet connection is reset
  authenticate: (userAddress, password, success, error) =>
    new Promise((success, error) =>
      if navigator.onLine
        coordinates = darwin.Utils.extractRemoteAddress(userAddress)
        $.ajax({
          url: 'http://' + coordinates.address + '/_session',
          type: 'POST',
          dataType: 'json',
          data: {
            'name': coordinates.username,
            'password': password
          },
          success: ((response) =>
            darwin.LocalStorageManager.setCredentials(userAddress, password)

            success({ status: true })
          ),
          error: ((err) =>
            error(new darwin.Error('Address or password are incorrect.'))
          )
        })
      else
        credentials = darwin.LocalStorageManager.getCredentials()
        if credentials['user-address'] is userAddress and credentials['password'] is password
          success({ status: true })
        else
          error(new darwin.Error('Address or password are incorrect.'))
    )

  # Initialises Darwin core when the user is authenticated
  initialise: =>
    new Promise((success, error) =>
      darwin.DatabaseManager.on('initialise-app-dbs-schema-success', () =>
        darwin.DatabaseManager.on('upload-peer-success', () =>
          darwin.CryptographyManager.initialiseKeys(((result) =>
            @_log('Initialised the key pair.')
          ), ((err) =>
            @_log(err.message)
          ))

          darwin.RemoteManager.initialise().then((() =>
            darwin.ExplicitGraphManager.initialise()
            darwin.ImplicitGraphManager.initialise()

            @_log('Initialised remote manager.')
            darwin.LocalConnection.triggerEvent('remote-connections-initialised')
          ), ((err) =>
            @_log('Failed to initialise remote manager:', err.message)
          ))
        )

        darwin.DatabaseManager.on('initialise-drive-dbs-schema-success', () =>
          @_drives.initialise(darwin.DatabaseManager.getDriveDbsNames(), darwin.DatabaseManager.getDriveDbs()).then((() =>
            @_log('Initialised drive directories.')
            darwin.LocalConnection.triggerEvent('drives-initialised')
          ), ((err) =>
            @_log('Failed to initialise drive directories:', err.message)
          ))
        )

        darwin.DistributionManager.initialise()
        @_log('Initialised distribution manager.')
        success()
      )
    )

darwin.Core = new Core
