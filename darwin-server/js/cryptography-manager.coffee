$.getScript('http://polycrypt.net/front/polycrypt.js')


class CryptographyManager
  constructor: ->
    @_crypto = null

    if window.crypto.subtle
      @_crypto = window.crypto.subtle
    else
      @_crypto = window.polycrypt

  # Logs messages
  _log: () =>
    args = ['key-manager @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  # Saves key pair in the browser
  _saveUserKeys: (publicKey, privateKey, success, error) =>
    @_exportKey(privateKey).then(((exportedPrivateKey) =>
      @_exportKey(publicKey).then(((exportedPublicKey) =>
        darwin.LocalStorageManager.setKeys(exportedPublicKey, exportedPrivateKey)

        # Saves public key in the local core database
        data = {
          publicKey: publicKey
        }

        success(data)
      ), ((err) =>
        error(new darwin.Error('Failed to save the keys.', err.message))
      ))
    ), ((err) =>
      error(new darwin.Error('Failed to save the keys.', err.message))
    ))

  _digest: (data, algo='SHA-256') =>
    new Promise((success, error) =>
      if window.crypto.subtle
        algorithm = {
          name: algo
        }

        window.crypto.subtle.digest(algorithm, data).then(success, (err) =>
          error(new darwin.Error('Failed to generate a hash.', err.message))
        )
      else
        algorithm = algo

        op = window.polycrypt.digest(algorithm, data)

        op.oncomplete = (e) =>
          success(e.target.result)

        op.onerror = (err) =>
          error(new darwin.Error('Failed to generate a hash.', err.message))
    )

  _generateKey: =>
    new Promise((success, error) =>
      if window.crypto.subtle
        algorithm = {
          name: 'RSASSA-PKCS1-v1_5',
          modulusLength: 512,
          publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
          hash: {
            name: 'SHA-256'
          }
        }

        window.crypto.subtle.generateKey(algorithm, true, ['sign', 'verify']).then(success, (err) =>
          error(new darwin.Error('Failed to generate a key pair.', err.message))
        )
      else
        algorithm = {
          name: 'RSASSA-PKCS1-v1_5',
          params: {
            modulusLength: 512,
            publicExponent: new Uint8Array([0x01, 0x00, 0x01])
          }
        }

        op = window.polycrypt.generateKey(algorithm, true, ['sign', 'verify'])

        op.oncomplete = (e) =>
          success(e.target.result)

        op.onerror = (err) =>
          error(new darwin.Error('Failed to generate a key pair.', err.message))
    )

  _exportKey: (key) =>
    new Promise((success, error) =>
      if window.crypto.subtle
        window.crypto.subtle.exportKey('jwk', key).then(success, (err) =>
          error(new darwin.Error('Failed to export the key.', err.message))
        )
      else
        op = window.polycrypt.exportKey('jwk', key)

        op.oncomplete = (e) =>
          success(e.target.result)

        op.onerror = (err) =>
          error(new darwin.Error('Failed to export the key.', err.message))
    )

  _importKey: (keyJWK, usages) =>
    new Promise((success, error) =>
      if window.crypto.subtle
        algorithm = {
          name: 'RSASSA-PKCS1-v1_5',
          hash: {
            name: 'SHA-256'
          }
        }
        extractable = true

        window.crypto.subtle.importKey('jwk', keyJWK, algorithm, extractable, usages).then(success, (err) =>
          error(new darwin.Error('Failed to import the key.', err.message))
        )
      else
        algorithm = 'RSASSA-PKCS1-v1_5'
        extractable = true

        op = window.polycrypt.importKey('jwk', keyJWK, algorithm, extractable, usages)

        op.oncomplete = (e) =>
          success(e.target.result)

        op.onerror = (err) =>
          error(new darwin.Error('Failed to import the key.', err.message))
    )

  _sign: (privateKey, serialisedData) =>
    new Promise((success, error) =>
      data = darwin.Utils.stringToBuffer(serialisedData)
      console.log('Private key ', privateKey)

      if window.crypto.subtle
        algorithm = {
          name: 'RSASSA-PKCS1-v1_5'
        }

        window.crypto.subtle.sign(algorithm, privateKey, data).then(((signature) =>
          success(darwin.Utils.bufferToInts(signature))
        ), ((err) =>
          @_log(err)
          error(new darwin.Error('Failed to sign the data.', err.message))
        ))
      else
        algorithm = {
          name: 'RSASSA-PKCS1-v1_5',
          params: {
            hash: 'SHA-256'
          }
        }

        op = window.polycrypt.sign(algorithm, privateKey, data)

        op.oncomplete = (e) =>
          if e.target.result
            success(darwin.Utils.bufferToInts(e.target.result))
          else
            error(new darwin.Error('Failed to sign the data.'))

        op.onerror = (err) =>
          error(new darwin.Error('Failed to sign the data.', err.message))
    )

  _verify: (publicKey, signature, serialisedData) =>
    new Promise((success, error) =>
      signature = darwin.Utils.intsToBuffer(signature)
      data = darwin.Utils.stringToBuffer(serialisedData)

      if window.crypto.subtle
        algorithm = {
          name: 'RSASSA-PKCS1-v1_5'
        }

        window.crypto.subtle.verify(algorithm, publicKey, signature, data).then((() =>
          success()
        ), ((err) =>
          error(new darwin.Error('Failed to verify the data.', err.message))
        ))
      else
        algorithm = {
          name: 'RSASSA-PKCS1-v1_5',
          params: {
            hash: 'SHA-256'
          }
        }
        op = window.polycrypt.verify(algorithm, publicKey, signature, data)

        op.oncomplete = (e) =>
          if e.target.result
            success()
          else
            error(new darwin.Error('Failed to verify the data.'))

        op.onerror = (err) =>
          error(new darwin.Error('Failed to verify the data.', err.message))
    )

  # Retrieves application's public key
  getPublicKey: =>
    new Promise((success, error) =>
      darwinKeys = darwin.LocalStorageManager.getKeys()
      keyData = null
      if darwinKeys isnt null and 'public-key' of darwinKeys
        keyData = darwinKeys['public-key']

      if keyData isnt null
        @_importKey(keyData, ['verify']).then(((publicKey) =>
          success(publicKey)
        ), ((err) =>
          error(new darwin.Error('Failed to import the public key.', err.message))
        ))
      else
        error(new darwin.Error('Failed to get the public key from the local storage.'))
    )

  # Retrieves application's private key
  getPrivateKey: =>
    new Promise((success, error) =>
      userAddress = darwin.Core.getUserAddress()
      darwinKeys = darwin.LocalStorageManager.getKeys()
      keyData = null
      if darwinKeys isnt null and userAddress of darwinKeys and 'private-key' of darwinKeys[userAddress]
        keyData = darwinKeys[userAddress]['private-key']

      if keyData isnt null
        @_importKey(keyData, ['sign']).then(((privateKey) =>
          success(privateKey)
        ), ((err) =>
          error(new darwin.Error('Failed to get the private key from the local storage.', err.message))
        ))
      else
        error(new darwin.Error('Failed to get the private key from the local storage.'))
    )

  # Gets the public key of the remote user
  getRemoteUserKey: (remoteUserAddress) =>
    new Promise((success, error) =>
      usersDb = darwin.DatabaseManager.getCoreDbs().getController('applications-users')
      usersDb.get(remoteUserAddress).then(((doc) =>
        publicKey = doc['public-key']
        success(publicKey)
      ), ((err) =>
        if typeof(error) isnt 'undefined'
          error(err)
      ))
    )

  # Saves the address and public key of the remote user in the application's database 'users'
  setRemoteUserKey: (remoteUserAddress, remotePublicKey) =>
    new Promise((success, error) =>
      doc = {
        'public-key': remotePublicKey,
        'timestamp': new Date().getTime().toString()
      }
      usersDb = darwin.DatabaseManager.getCoreDbs().getController('applications-users')
      usersDb.set(remoteUserAddress, doc).then(success, error)
      usersDb.sync()
    )

  # Deletes the public key of the remote user
  deleteRemoteUserKey: (remoteUserAddress) =>
    new Promise((success, error) =>
      usersDb = darwin.DatabaseManager.getCoreDbs().getController('applications-users')
      usersDb.delete(remoteUserAddress).then(success, error)
      usersDb.sync()
    )

  # Generates a hash from uint8 array data
  digest: (data, algorithm='SHA-256') =>
    new Promise((success, error) =>
      @_digest(data, algorithm).then(success, error)
    )

  # Signs data with local private key
  sign: (serialisedData, success, error) =>
    @getPrivateKey().then(((privateKey) =>
      @_sign(privateKey, serialisedData).then(success, error)
    ), ((err) =>
      @_log(err.message)
      error(new darwin.Error('Failed to get the private key.', err.message))
    ))

  # Verifies data with given signature
  verify: (serialisedData, signature, remoteUserAddress, success, error) =>
    @getRemoteUserKey(remoteUserAddress).then(((remotePublicKey) =>
      @_log(remotePublicKey)
      @_verify(remotePublicKey, signature, serialisedData).then(success, error)
    ), (() =>
      error(new darwin.Error('Failed to get public key of remote user when verifying the data.'))
    ))

  # Generates key pair if it does not exist
  initialiseKeys: (success, error) =>
    userAddress = darwin.Core.getUserAddress()
    darwinKeys = darwin.LocalStorageManager.getKeys()

    if darwinKeys is null or (darwinKeys isnt null and userAddress not of darwinKeys)
      @_generateKey().then(((result) =>
        @_saveUserKeys(result.publicKey, result.privateKey, success)
      ), ((err) =>
        if typeof(error) is 'function'
          error(new darwin.Error(err.message))
      ))
    else
      @getPublicKey().then(((publicKey) =>
        data = {
          publicKey: publicKey
        }
        success(data)
      ), ((err) =>
        error(new darwin.Error('Failed to initialise the keys.', err.message))
      ))

darwin.CryptographyManager = new CryptographyManager
