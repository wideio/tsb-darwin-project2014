class DatabaseManager extends darwin.EventProvider
  constructor: ->
    super

    # Names of the application databases
    @_appDbNames = []

    # Names of application's hidden databases
    @_appHiddenDbNames = [
      '__permissions'
    ]

    # Names of drive databases
    @_driveDbNames = []

    # Names of the core databases
    @_coreDbNames = [
      'applications',
#      'applications-bridges',
      'applications-databases',
      'applications-users',
      'drives',
      'graphs',
      'peers',
#      'sync-locks'
    ]

    # Application's database controllers (both local and remote)
    @_appDbs = new darwin.DatabaseCollection

    # Application's hidden database controllers (both local and remote)
    @_appHiddenDbs = new darwin.DatabaseCollection

    # Drive database controllers (both local and remote)
    @_driveDbs = new darwin.DatabaseCollection

    # Core database controllers (both local and remote)
    @_coreDbs = new darwin.DatabaseCollection

    @_graphsDb = null

    @_localNodesDb = null

    @_edgesDb = null

  # Logs messages
  _log: () =>
    args = ['database-manager @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  # Retrieves application's database names from core databases and saves it here
  _initialiseAppDbsSchema: =>
    new Promise((success, error) =>
      databasesDb = @_coreDbs.getController('applications-databases')

      # Synchronises applications-databases - it is called if the first attempt of initialising fails
      customError = () =>
        databasesDb.sync().then((() =>
          operations((err) =>
            @_triggerEvent('initialise-app-dbs-schema-error')
            error(err)
          )
        ), ((err) =>
          @_triggerEvent('initialise-app-dbs-schema-error')
          error(err)
        ))

      operations = (err) =>
        databasesDb.get(darwin.Core.getAppId()).then(((doc) =>
          @_appDbNames = doc['db-names']

          @_triggerEvent('initialise-app-dbs-schema-success')
          success()
        ), err)

      operations(customError)
    )

  # Retrieves drive database names from core databases and saves it here
  _initialiseDriveDbsSchema: =>
    new Promise((success, error) =>
      drivesDb = @_coreDbs.getController('drives')

      drivesDb.all().then(((docs) =>
        @_driveDbNames = []
        for doc in docs
          @_driveDbNames.push(doc._id)

        @_triggerEvent('initialise-drive-dbs-schema-success')
        success()
      ), ((err) =>
        @_triggerEvent('initialise-drive-dbs-schema-error')
        error(err)
      ))
    )

  # Updates peer information in the core database
  _uploadPeer: =>
    new Promise((success, error) =>
      if navigator.onLine
        peersDb = @_coreDbs.getController('peers')
        peersDb.set(darwin.Core.getAppId(), {
          'recent-peer-id': darwin.Core.getPeerId()
        }).then(((info) =>
          peersDb.sync().then((() =>
            @_triggerEvent('upload-peer-success')
            success(info)
          ), ((err) =>
            @_triggerEvent('upload-peer-error')
            error(err)
          ))
        ), ((err) =>
          @_triggerEvent('upload-peer-error')
          error(err)
        ))
      else
        @_triggerEvent('upload-peer-error')
        error(new darwin.Error('Cannot upload peer without an access to the Internet.'))
    )

  initialise: (userAddress, password) =>
    new Promise((success, error) =>
      # Initialises core databases
      @_coreDbs.initialise(userAddress, password, '__darwin', @_coreDbNames)
      @_triggerEvent('initialise-core-dbs-success')
      @_log('Initialised core databases.')

      appId = darwin.Core.getAppId()
      coordinates = darwin.Utils.extractRemoteAddress(userAddress)

      # Synchronises core databases
      @_coreDbs.sync().then((info) =>
        if info.failed > 0
          @_log('Failed to synchronise', info.failed, 'core databases.')
        @_log('Synchronised core databases.')

        @_initialiseAppDbsSchema().then((() =>
          @_log('Initialised application database schema.')

          # Initialises application databases
          @_appDbs.initialise(userAddress, password, appId, @_appDbNames)
          @_appHiddenDbs.initialise(userAddress, password, appId, @_appHiddenDbNames)
          @_triggerEvent('initialise-application-dbs-success')
          @_log('Initialised application databases.')

          success(coordinates)

          # Synchronises application's hidden databases
          @_appHiddenDbs.sync().then((info) =>
            @_log('Synchronised application hidden databases.')
          )
        ), ((err) =>
          error(new darwin.Error('Failed to initialise application databases schema.', err.message))
        ))

        @_initialiseDriveDbsSchema().then((() =>
          @_driveDbs.initialise(userAddress, password, '__drive', @_driveDbNames)
          @_log('Initialised drive databases.')
        ), ((err) =>
          @_log('Failed to initialise drive database schema:', err.message)
        ))

        # Once the peer ID is initialised, it can be uploaded
        darwin.Core.on('initialise-peer-id-success', () =>
          @_uploadPeer().then((() =>
            @_log('Uploaded peer ID successfully.')
          ), ((err) =>
            @_log('Failed to upload peer ID:', err.message)
          ))
        )
      )
    )

  # Gets the methods which are accessible by Darwin client and remote users
  getPublicMethods: =>
    forbiddenMethods = ['initialise', 'getPublicMethods', 'getCoreDbs', 'getDriveDbs', 'getAppDbs', 'getAppHiddenDbs',
                        'getDriveDbsNames', 'setDriveDbNames']
    methods = []
    for methodName of @
      if methodName[0] isnt '_' and methodName not in forbiddenMethods
        methods.push(methodName)
    methods

  # Simply gets core database controllers
  getCoreDbs: =>
    @_coreDbs

  # Simply gets drive database controllers
  getDriveDbs: =>
    @_driveDbs

  # Simply gets application's database controllers
  getAppDbs: =>
    @_appDbs

  # Simply gets application's hidden database controllers
  getAppHiddenDbs: =>
    @_appHiddenDbs

  # Simply gets graphs database controller
  getGraphsDb: =>
    @_graphsDb

  # Simply gets local nodes database controller
  getLocalNodesDb: =>
    @_localNodesDb

  # Simply gets edges database controller
  getEdgesDb: =>
    @_edgesDb

  # Simply gets drive database names
  getDriveDbsNames: =>
    @_driveDbNames

  # Simply sets drive database names
  setDriveDbNames: (dbNames) =>
    @_driveDbNames = dbNames
    @_driveDbs.setDbNames(@_driveDbNames)

  # Fetches all the local data
  all: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      dbName = parameters[0]
      options = parameters[1]

      if typeof(options) isnt 'object' or options is null
        options = {}

      options.include_docs = true

      if dbName[0] isnt '_'
        @_appDbs.getController(dbName).all(options).then(success, error)
      else
        error(new darwin.Error('Accessing this database is forbidden.'))
    else
      darwin.RemoteManager.callRemoteMethod(sourceData, destinationData, success, error)

  # Gets the document with given key
  get: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      dbName = parameters[0]
      key = parameters[1]

      if dbName[0] isnt '_'
        @_appDbs.getController(dbName).get(key).then(success, error)
      else
        error(new darwin.Error('Accessing this database is forbidden.'))
    else
      darwin.RemoteManager.callRemoteMethod(sourceData, destinationData, success, error)

  # Creates or updates the document with given key and value
  set: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      dbName = parameters[0]
      key = parameters[1]
      value = parameters[2]
      options = parameters[3]

      if dbName[0] isnt '_'
        @_appDbs.getController(dbName).set(key, value, options).then(success, error)
      else
        error(new darwin.Error('Accessing this database is forbidden.'))
    else
      darwin.RemoteManager.callRemoteMethod(sourceData, destinationData, success, error)

  # Deletes the document with given key
  delete: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      dbName = parameters[0]
      key = parameters[1]

      if dbName[0] isnt '_'
        @_appDbs.getController(dbName).delete(key).then(success, error)
      else
        error(new darwin.Error('Accessing this database is forbidden.'))
    else
      darwin.RemoteManager.callRemoteMethod(sourceData, destinationData, success, error)

  # Synchronises user's local and remote databases
  sync: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      dbName = parameters[0]

      if dbName[0] isnt '_'
        @_appDbs.getController(dbName).sync().then(success, error)
      else
        error(new darwin.Error('Accessing this database is forbidden.'))
    else
      error(new darwin.Error('Synchronising remote user\'s database is forbidden.'))

  executeReadBatch: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      dbName = parameters[0]
      keys = parameters[1]

      if dbName[0] isnt '_'
        @_appDbs.getController(dbName).executeReadBatch(keys).then(success, error)
      else
        error(new darwin.Error('Accessing this database is forbidden.'))
    else
      darwin.RemoteManager.callRemoteMethod(sourceData, destinationData, success, error)

  executeWriteBatch: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      dbName = parameters[0]
      docs = parameters[1]

      if dbName[0] isnt '_'
        @_appDbs.getController(dbName).executeWriteBatch(docs).then(success, error)
      else
        error(new darwin.Error('Accessing this database is forbidden.'))
    else
      darwin.RemoteManager.callRemoteMethod(sourceData, destinationData, success, error)

darwin.DatabaseManager = new DatabaseManager
