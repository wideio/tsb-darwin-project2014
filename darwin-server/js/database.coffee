class darwin.Database
  constructor: (userAddress, password, appId, dbName) ->
    @_userAddress = userAddress

    @_appId = appId

    @_dbName = dbName

    @_isOnlyRemote = null

    @_address = {}

    @_db = null

    coordinates = darwin.Utils.extractRemoteAddress(@_userAddress)
    username = coordinates.username
    address = coordinates.address

    if typeof(password) isnt 'undefined' and password isnt null
      remoteDbPrefix = 'http://' + username + ':' + password + '@' + address + '/'
      @_address.local = username + '-' + @_appId + '-' + @_dbName
      @_address.remote = remoteDbPrefix + username + '-' + @_appId + '-' + @_dbName
      @_db = new PouchDB(@_address.local)
      @_isOnlyRemote = false
    else
      remoteDbPrefix = 'http://' + address + '/'
      @_address.remote = remoteDbPrefix + username + '-' + @_appId + '-' + @_dbName
      @_db = new PouchDB(@_address.remote)
      @_isOnlyRemote = true

  # Logs messages
  _log: () =>
    args = ['database @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  getName: =>
    @_dbName

  getAddress: =>
    @_address

  getOwnerAddress: =>
    @_userAddress

  # Fetches all the local data
  all: (options={}) =>
    new Promise((success, error) =>
      if typeof(options) isnt 'object' or options is null
        options = {}

      if 'include_docs' not of options
        options.include_docs = true

      @_db.allDocs(options, (err, result) =>
        if !err
          finalResult = []

          for doc in result.rows
            finalResult.push(doc.doc)

          success(finalResult)
        else
          error(new darwin.Error('Failed to get all documents from the database.'))
      )
    )

  # Gets the document with given key
  get: (key) =>
    new Promise((success, error) =>
      @_db.get(key, (err, doc) =>
        if not err
          success(doc)
        else
          error(new darwin.Error('Failed to get the document from the database.'))
      )
    )

  # Creates or updates the document with given key and value
  set: (key, value, options={}) =>
    new Promise((success, error) =>
      if typeof(options) isnt 'object' or options is null
        options = {}

      finalCallback = (err, doc) =>
        if not err
          if 'includeDetailedResult' of options and options.includeDetailedResult is true
            success(doc)
          else
            success()
        else
          error(new darwin.Error('Failed to set data to the document in the database.'))

      @_db.get(key, (err, doc) =>
        finalValue = {}
        for k, v of doc
          finalValue[k] = v if k[0] isnt '_'

        for k, v of value
          finalValue[k] = v if k[0] isnt '_'

        if not err
          @_db.put(finalValue, key, doc._rev, finalCallback)
        else
          @_db.put(finalValue, key, finalCallback)
      )
    )

  # Deletes the document with given key
  delete: (key) =>
    new Promise((success, error) =>
      @_db.get(key, (err, doc) =>
        if not err
          @_db.remove(doc, (err, response) =>
            if not err
              success(response)
            else
              error(new darwin.Error('Failed to delete the document from the database.'))
          )
        else
          error(err)
      )
    )

  replicate: (remoteAddress, direction, options) =>
    new Promise((success, error) =>
      if navigator.onLine
        if typeof(options) isnt 'object' or options is null
          options = {}

        if 'live' not of options
          options.live = false

        if direction is darwin.Config.REPLICATION.DIRECTION.FROM
          @_db.replicate.from(remoteAddress, options)
          .on('complete', success)
          .on('error', (err) =>
            error(new darwin.Error('Failed to replicate data from the remote mirror.'))
          )
        else if direction is darwin.Config.REPLICATION.DIRECTION.TO
          @_db.replicate.to(remoteAddress, options)
          .on('complete', success)
          .on('error', (err) =>
            error(new darwin.Error('Failed to replicate data to the remote mirror.'))
          )
        else
          error(new darwin.Error('Unknown direction of the replication.'))
      else
        error(new darwin.Error('Cannot synchronise database without an access to the Internet.'))
    )

  # Synchronises user's local and remote databases
  sync: =>
    new Promise((success, error) =>
      if navigator.onLine
        if not @_isOnlyRemote
          options = {
            live: false
          }

          PouchDB.sync(@_address.local, @_address.remote, options)
          .on('complete', success)
          .on('error', (err) =>
            error(new darwin.Error('Failed to synchronise the database with the remote mirror.'))
          )
        else
          error(new darwin.Error('The database is remote only and cannot be synchronised.'))
      else
        error(new darwin.Error('Cannot synchronise database without an access to the Internet.'))
    )

  # Executes read batch
  executeReadBatch: (keys) =>
    new Promise((success, error) =>
      options = {
        keys: keys,
        include_docs: true
      }

      @_db.allDocs(options, (err, result) =>
        if not err
          finalResult = []
          for doc in result.rows
            finalResult.push(doc.doc)
          success(finalResult)
        else
          error(new darwin.Error('Failed to execute read batch.'))
      )
    )

  # Executes write batch
  executeWriteBatch: (docs) =>
    new Promise((success, error) =>
      options = {
        include_docs: true,
        keys: []
      }

      for item in docs
        options.keys.push(item._id)

      @_db.allDocs(options, (err, result) =>
        if result.rows.length > 0
          for i in [0..result.rows.length - 1] by 1
            if 'doc' of result.rows[i]
              docs[i]._rev = result.rows[i].doc._rev
              for key of result.rows[i].doc
                if key[0] isnt '_'
                  docs[i][key] = result.rows[i].doc

          @_db.bulkDocs(docs, (err, response) =>
            if not err
              success(response)
            else
              error(new darwin.Error('Failed to execute write batch.'))
          )
        else
          error(new darwin.Error('Failed to execute write batch.'))
      )
    )


class darwin.DatabaseCollection
  constructor: ->
    # Names of the databases which form the collection
    @_dbNames = null

    # Address of the user who owns the database collection
    @_userAddress = null

    # ID of the application which the database collection belongs to
    @_appId = null

    # Password to the remote mirror which contains the database collection
    @_password = null

  # Logs messages
  _log: () =>
    args = ['database-controller-collection @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  initialise: (userAddress, password, appId, dbNames) =>
    @_userAddress = userAddress
    @_appId = appId
    @_password = password
    @_dbNames = dbNames

  setDbNames: (dbNames) =>
    @_dbNames = dbNames

  getController: (dbName) =>
    new darwin.Database(@_userAddress, @_password, @_appId, dbName)

  sync: =>
    new Promise((success, error) =>
      info = {
        synchronised: 0,
        failed: 0
      }

      if navigator.onLine
        for dbName in @_dbNames
          @getController(dbName).sync().then((() =>
            ++info.synchronised
            if info.synchronised + info.failed is @_dbNames.length
              success(info)
          ), (() =>
            ++info.failed
            if info.synchronised + info.failed is @_dbNames.length
              success(info)
          ))
      else
        info.failed = @_dbNames.length
        success(info)
    )
