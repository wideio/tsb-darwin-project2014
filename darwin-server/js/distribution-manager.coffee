# Represents manager for distributed computing purposes
class DistributionManager
  constructor: ->
    @_forbiddenCalls = ['document', 'console', 'window', 'eval']

    @_forbiddenDots = []

  # Logs messages
  _log: () =>
    args = ['distribution-manager @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  _send: (remoteUserAddress, dispatcherName, methodName, parameters, success, error) =>
    connection = darwin.RemoteManager.getConnection(remoteUserAddress)

    if connection
      connection.sendRequest(remoteUserAddress, dispatcherName, methodName, parameters, success, error)
    else
      error(new darwin.Error('Failed to find the remote connection.'))

  _verifyCode: (code) =>
    result = true
    recentCall = null

    walker = new UglifyJS.TreeWalker((node, descend) =>
      if not result
        return true

      if node instanceof UglifyJS.AST_Call
        if node.start.value in @_forbiddenCalls
          result = false

        recentCall = node
        descend()
        recentCall = null
        return true

      if node instanceof UglifyJS.AST_Dot
        if node.property in @_forbiddenDots
          result = false
    )

    mainScope = UglifyJS.parse(code)
    mainScope.walk(walker)
    result

  initialise: =>
    @_appDbs = darwin.DatabaseManager.getAppDbs()
    @_publicAPI = new darwin.PublicAPI(@_appDbs)

  getPublicMethods: =>
    forbiddenMethods = ['initialise', 'getPublicMethods']
    methods = []
    for methodName of @
      if methodName[0] isnt '_' and methodName not in forbiddenMethods
        methods.push(methodName)
    methods

  # Sends the code to remote user
  sendCode: (sourceData, destinationData, success, error) =>
    send = () =>
      dispatcherName = darwin.Config.DISPATCHER.DISTRIBUTION_MANAGER
      methodName = 'runCode'
      @_send(destinationData.userAddress, dispatcherName, methodName, destinationData.parameters, success, error)

    if sourceData.userAddress is darwin.Core.getUserAddress()
      send()
    else if destinationData.userAddress is darwin.Core.getUserAddress()
      selfData = {
        userAddress: darwin.Core.getUserAddress()
      }
      remoteUserData = {
        userAddress: destinationData.userAddress,
        parameters: [darwin.Core.getUserAddress()]
      }
      darwin.RemoteManager.getRemoteUserPermissions(selfData, remoteUserData, ((permissions) =>
        if darwin.Config.PERMISSION.MEDIATE in permissions
          send()
        else
          error(new darwin.Error('User is not allowed to mediate.'))
      ))
    else
      error(new darwin.Error('It seems that the code got lost in the network.'))

  # Runs the code received from remote user
  runCode: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress isnt darwin.Core.getUserAddress() and destinationData.userAddress is darwin.Core.getUserAddress()
      try
        parameters = destinationData.parameters

        code = parameters[0]
        dependencies = parameters[1]

        finalCode = dependencies.toString() + '\n' + code.toString()
        verificationResult = @_verifyCode(finalCode)
        if verificationResult
          selfData = {
            userAddress: darwin.Core.getUserAddress()
          }
          remoteUserData = {
            userAddress: darwin.Core.getUserAddress(),
            parameters: [sourceData.userAddress]
          }
          darwin.RemoteManager.getRemoteUserPermissions(selfData, remoteUserData, ((permissions) =>
            run = new Function('application', 'success', 'error', finalCode)
            publicAPI = new darwin.PublicAPI(permissions)
            run(publicAPI, ((result) =>
              success(result)
            ), ((err) =>
              # TODO: error description
              error(err)
            ))
          ), (() =>
            error(new darwin.Error('Failed to validate permissions.'))
          ))
        else
          error(new darwin.Error('The code contains forbidden calls.'))
      catch err
        error(new darwin.Error('Failed to run the code.'))
    else
      error(new darwin.Error('Executing this operation is forbidden.'))

darwin.DistributionManager = new DistributionManager
