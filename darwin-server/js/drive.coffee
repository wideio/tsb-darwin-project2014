class Drive
  constructor: (name, root, db) ->
    @_name = name

    @_root = root

    @_db = db

  # Logs messages
  _log: () =>
    args = ['drive @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  # Checks if the directory tree exists and creates it if requested
  _dive: (path, ending='directory', create=false) =>
    new Promise((success, error) =>
      dive = (root, items) =>
        while items.length > 0 and (items[0] is '.' or items[0].length is 0)
          items.shift()

        if items.length > 0
          if items.length is 1 and ending is 'file'
            root.getFile(items[0], { create: create }, ((fileEntry) =>
              if items.length > 0
                dive(fileEntry, items.slice(1))
              else
                success(fileEntry)
            ), (() =>
              error(new darwin.Error('Failed to dive into the directory tree.'))
            ))
          else
            root.getDirectory(items[0], { create: create }, ((dirEntry) =>
              if items.length > 0
                dive(dirEntry, items.slice(1))
              else
                success(dirEntry)
            ), (() =>
              error(new darwin.Error('Failed to dive into the directory tree.'))
            ))
        else
          success(root)

      dirs = path.split('/')
      while dirs.length > 0 and dirs[dirs.length - 1].length is 0
        dirs.pop()

      dive(@_root, dirs)
    )

  # Generates a hash of the specified slice of the blob
  _computeHash: (slice, id=null) =>
    new Promise((success, error) =>
      reader = new FileReader()

      reader.onloadend = (e) =>
        hashStr = darwin.CryptographyManager.digest(new Uint8Array(e.target.result)).then(((hashBuff) =>
          hashStr = darwin.Utils.intsToHexString(new Uint8Array(hashBuff))
          success({
            id: id,
            hash: hashStr
          })
        ), ((err) =>
          error(new darwin.Error('Failed to generate a hash of the slice.', err.message))
        ))

      reader.readAsArrayBuffer(slice)
    )

  # Generates an array of hashes of the blob
  _generateRevision: (blob) =>
    new Promise((success, error) =>
#    startBlock = Math.floor(position / darwin.Config.FILE_STORAGE.MAX_SLICE_SIZE)
#    endBlock = Math.floor((position + length - 1) / darwin.Config.FILE_STORAGE.MAX_SLICE_SIZE))

      maxSliceSize = darwin.Config.FILE_STORAGE.MAX_SLICE_SIZE

      slicesCount = Math.ceil(blob.size / maxSliceSize)

      if slicesCount > 0
        hashArray = []
        for i in [0..slicesCount - 1]
          hashArray.push(null)

        info = {
          completed: 0,
          failed: 0
        }

        for i in [0..slicesCount - 1]
          start = i * maxSliceSize
          end = if start + maxSliceSize < blob.size then start + maxSliceSize else blob.size
          @_computeHash(blob.slice(start, end), i).then(((result) =>
            hashArray[result.id] = result.hash
            ++info.completed
            if info.completed is slicesCount
              success(hashArray)
            else if info.completed + info.failed is slicesCount
              error(new darwin.Error('Failed to generate a hash of some of the slices.'))
          ), (() =>
            ++info.failed
            if info.completed + info.failed is slicesCount
              error(new darwin.Error('Failed to generate a hash of some of the slices.'))
          ))
      else
        success([])
    )

  # Updates a revision of the file in the database
  _updateFileRevision: (internalPath, blob, type) =>
    new Promise((success, error) =>
      @_generateRevision(blob).then(((hashArray) =>
        @_db.set(internalPath, {
          revisions: hashArray,
          type: type
        }).then((() =>
          success()
          @_db.sync()
#          .then((() =>
#            success()
#          ), ((err) =>
#            error(new darwin.Error('Failed to synchronise the revision of the blob:', err.message))
#          ))
        ), ((err) =>
          error(new darwin.Error('Failed to set a revision of the file.', err.message))
        ))
      ), ((err) =>
        error(new darwin.Error('Failed to generate a revision of the blob:', err.message))
      ))
    )

  # Gets the revision of the file
  _getFileRevision: (internalPath) =>
    new Promise((success, error) =>
      db = @_driveDbs.getController(@_name)

      db.get(internalPath).then(((result) =>
        success(result.revisions)
      ), error)
    )

  # Gets a list of the directory names of specified path
  listDirectoryNames: (internalPath) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_dive(path).then(((entry) =>
        dirReader = entry.createReader()
        names = []

        readEntries = () =>
          dirReader.readEntries(((entries) =>
            if entries.length is 0
              success(names)
            else
              for item in entries
                if item.isDirectory
                  names.push(item.name)
              readEntries()
          ), (() =>
            error(new darwin.Error('Failed to fetch some items of directory "' + entry.fullPath + '".'))
          ))

        readEntries()
      ))
    )

  # Gets a list of the file names of specified path
  listFileNames: (internalPath) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_dive(path).then(((entry) =>
        dirReader = entry.createReader()
        names = []

        readEntries = () =>
          dirReader.readEntries(((entries) =>
            if entries.length is 0
              success(names)
            else
              for item in entries
                if item.isFile
                  names.push(item.name)
              readEntries()
          ), (() =>
            error(new darwin.Error('Failed to fetch some items of directory "' + entry.fullPath + '".'))
          ))

        readEntries()
      ))
    )

  # Checks if the path exists; if create == true, creates the path
  resolveDirectoryPath: (internalPath, create=false) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_dive(path, 'directory', create).then(success, error)
    )

  deleteDirectory: (internalPath) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_root.getDirectory(path, { create: false, exclusive: true }, ((dirEntry) =>

        dirEntry.remove((() =>
          success({ status: true })
        ), ((err) =>
          error(new darwin.Error('Failed to delete directory "' + path + '".', err.message))
        ))

      ), ((err) =>
        error(new darwin.Error('Failed to get directory "' + path + '".', err.message))
      ))
    )

  deleteDirectoryRecursively: (internalPath) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_root.getDirectory(path, { create: false, exclusive: true }, ((dirEntry) =>

        dirEntry.removeRecursively((() =>
          success({ status: true })
        ), ((err) =>
          error(new darwin.Error('Failed to delete directory recursively "' + path + '".', err.message))
        ))

      ), ((err) =>
        error(new darwin.Error('Failed to get directory "' + path + '".', err.message))
      ))
    )

  # Checks if the path exists; if create == true, creates the path
  resolveFilePath: (internalPath, create=false) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_dive(path, 'file', create).then(success, error)
    )

  getFileSize: (internalPath) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_root.getFile(path, { create: false, exclusive: true }, ((fileEntry) =>
        fileEntry.file(((blob) =>
          success(blob.size)
        ), ((err) =>
          error(new darwin.Error('Failed to get ' + internalPath + ' from ' + @_name + '.', err.message))
        ))
      ), ((err) =>
        error(new darwin.Error('Failed to set file "' + path + '".', err.message))
      ))
    )

  # Gets the file of specified path
  getFile: (internalPath, create, position, length) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_root.getFile(path, { create: create, exclusive: not create }, ((fileEntry) =>

        fileEntry.file(((blob) =>
          reader = new FileReader()

          # Starting position
          start = if position < blob.size then position else blob.size

          # Ending position
          length = if length >= 0 then length else 0
          end = if length isnt null and position + length < blob.size then position + length else blob.size

          reader.onloadend = (e) =>
            success({
              data: new Uint8Array(e.target.result),
              position: end
            })

          slice = blob.slice(start, end)
          reader.readAsArrayBuffer(slice)
        ), ((err) =>
          error(new darwin.Error('Failed to get ' + internalPath + ' from ' + @_name + '.', err.message))
        ))

      ), ((err) =>
        error(new darwin.Error('Failed to get ' + internalPath + ' from ' + @_name + '.', err.message))
      ))
    )

  # Updates the file of specified path
  updateFile: (internalPath, content, position, truncate, type) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_root.getFile(path, { create: false, exclusive: true }, ((fileEntry) =>

        fileEntry.createWriter((writer) =>
          position = if position <= writer.length then position else writer.length

          writeEnd = () =>
            fileEntry.file(((blob) =>
              @_updateFileRevision(internalPath, blob, type).then((() =>
                success({
                  position: position + content.length
                })
              ), ((err) =>
                error(new darwin.Error('Failed to update the revision of the file.', err.message))
              ))
            ), ((err) =>
              error(new darwin.Error('Failed to get file "' + path + '".', err.message))
            ))

          truncateEnd = () =>
            writer.onwriteend = writeEnd
            writer.write(new Blob([content]))

          writer.onerror = (err) =>
            error(new darwin.Error('Failed to write content to file "' + path + '".', err.message))

          if not truncate
            writer.seek(position)
            writer.onwriteend = writeEnd
            writer.write(new Blob([content]))
          else
            writer.truncate(position)
            writer.onwriteend = truncateEnd
        )

      ), ((err) =>
        error(new darwin.Error('Failed to set file "' + path + '".', err.message))
      ))
    )

  # Truncates the file of specified path
  truncateFile: (internalPath, position) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_root.getFile(path, { create: false, exclusive: true }, ((fileEntry) =>

        fileEntry.createWriter((writer) =>
          writer.onwriteend = () =>
            fileEntry.file(((blob) =>
              @_updateFileRevision(internalPath, blob).then((() =>
                success({
                  position: writer.length
                })
              ), ((err) =>
                error(new darwin.Error('Failed to update the revision of the file.', err.message))
              ))
            ), ((err) =>
              error(new darwin.Error('Failed to get file "' + path + '".', err.message))
            ))

          writer.truncate(position)
        )

      ), ((err) =>
        error(new darwin.Error('Failed to get ' + internalPath + ' from ' + @_name + '.', err.message))
      ))
    )

  # Deletes the file of specified path
  deleteFile: (internalPath) =>
    new Promise((success, error) =>
      path = '/' + @_name + internalPath

      @_root.getFile(path, { create: false, exclusive: true }, ((fileEntry) =>

        fileEntry.remove((() =>
          success()
        ), ((err) =>
          error(new darwin.Error('Failed to delete file "' + path + '".', err.message))
        ))

      ), ((err) =>
        error(new darwin.Error('Failed to delete file "' + path + '".', err.message))
      ))
    )

  #  sync: (sourceInfo, storageName, internalPath, revision) =>
#    new Promise((success, error) =>
#      path = '/' + storageName + internalPath
#
#      @_getFileRevision(internalPath, storageName).then(((currentRevision) =>
#        changes = []
#
#        for i in [0..currentRevision.length - 1] by 1
#          if
#        @_root.getFile(path, { create: false, exclusive: true }, ((fileEntry) =>
#
#          recursiveRead = () =>
#
#
#          fileEntry.file(((blob) =>
#            reader = new FileReader()
#
#            reader.onloadend = (e) =>
#              success({
#                data: new Uint8Array(e.target.result),
#                position: end
#              })
#
#            slice = blob.slice(start, end)
#            reader.readAsArrayBuffer(slice)
#          ), ((err) =>
#            error(new darwin.Error('Failed to get ' + internalPath + ' from ' + storageName + '.', err.message))
#          ))
#
#        ), ((err) =>
#          error(new darwin.Error('Failed to delete file "' + path + '".', err.message))
#        ))
#      ))
#    )



class darwin.DriveCollection
  constructor: ->
    @_driveNames = null

    @_root = null

    @_driveDbs = null

  _addDriveToCollection: (driveName) =>
    @_driveNames.push(driveName)
    darwin.DatabaseManager.setDriveDbNames(@_driveNames)

    drivesDb = darwin.DatabaseManager.getCoreDbs().getController('drives')
    drivesDb.set(driveName, {
      'permissions': [{
        'user-address': darwin.Core.getUserAddress(),
        'app-id': darwin.Core.getAppId()
      }]
    }).then(() =>
      drivesDb.sync()
    )

  initialise: (driveNames, driveDbs) =>
    new Promise((success, error) =>
      @_driveNames = driveNames
      @_driveDbs = driveDbs

      navigator.webkitTemporaryStorage.requestQuota(1024*1024, ((grantedBytes) =>
        window.requestFileSystem(window.TEMPORARY, grantedBytes, ((fileSystem) =>
          @_root = fileSystem.root
          success()
        ), ((err) =>
          error(new darwin.Error(err.message))
        ))
      ), (err) =>
        error(new darwin.Error(err.message))
      )
    )

  getDrive: (driveName, create=false) =>
    if driveName not in @_driveNames and not create
      null
    else
      if driveName not in @_driveNames and create
        @_addDriveToCollection(driveName)

      db = @_driveDbs.getController(driveName)
      new Drive(driveName, @_root, db)

  getAllDriveNames: =>
    @_driveNames

window.requestFileSystem  = window.requestFileSystem || window.webkitRequestFileSystem
