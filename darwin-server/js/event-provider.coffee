class darwin.EventProvider
  constructor: ->
    @_eventHandlers = {}

  _triggerEvent: (name, parameter=null) =>
    if name of @_eventHandlers and 'handlers' of @_eventHandlers[name]
      for handler in @_eventHandlers[name].handlers
        handler(parameter)

    @_eventHandlers[name] = {
      parameter: parameter,
    }

  on: (name, handler) =>
    if name of @_eventHandlers
      if 'parameter' not of @_eventHandlers[name]
        @_eventHandlers[name].handlers.push(handler)
      else
        parameter = @_eventHandlers[name].parameter
        handler(parameter)
    else
      @_eventHandlers[name] = {
        handlers: [handler]
      }
