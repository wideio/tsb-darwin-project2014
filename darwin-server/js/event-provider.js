// Generated by CoffeeScript 1.8.0
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  darwin.EventProvider = (function() {
    function EventProvider() {
      this.on = __bind(this.on, this);
      this._triggerEvent = __bind(this._triggerEvent, this);
      this._eventHandlers = {};
    }

    EventProvider.prototype._triggerEvent = function(name, parameter) {
      var handler, _i, _len, _ref;
      if (parameter == null) {
        parameter = null;
      }
      if (name in this._eventHandlers && 'handlers' in this._eventHandlers[name]) {
        _ref = this._eventHandlers[name].handlers;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          handler = _ref[_i];
          handler(parameter);
        }
      }
      return this._eventHandlers[name] = {
        parameter: parameter
      };
    };

    EventProvider.prototype.on = function(name, handler) {
      var parameter;
      if (name in this._eventHandlers) {
        if (!('parameter' in this._eventHandlers[name])) {
          return this._eventHandlers[name].handlers.push(handler);
        } else {
          parameter = this._eventHandlers[name].parameter;
          return handler(parameter);
        }
      } else {
        return this._eventHandlers[name] = {
          handlers: [handler]
        };
      }
    };

    return EventProvider;

  })();

}).call(this);

//# sourceMappingURL=event-provider.js.map
