class darwin.Executor
  constructor: (functionCode, dependencies='') ->
    # TODO: The name is weakly secured
    @_functionCode = 'var _darwinFunc = ' + functionCode + ';\n'

    @_dependencies = dependencies

    @_forbiddenCalls = ['document', 'window', 'eval']

    @_forbiddenDots = []

  _verifyCode: (code) =>
    result = true
    recentCall = null

    walker = new UglifyJS.TreeWalker((node, descend) =>
      if not result
        return true

      if node instanceof UglifyJS.AST_Call
        if node.start.value in @_forbiddenCalls
          result = false

        recentCall = node
        descend()
        recentCall = null
        return true

      if node instanceof UglifyJS.AST_Dot
        if node.property in @_forbiddenDots
          result = false
    )

    mainScope = UglifyJS.parse(code)
    mainScope.walk(walker)
    result

  execute: =>
    args = Array.prototype.slice.call(arguments)
    new Promise((success, error) =>
      # TODO: The name is weakly secured
      argsJSON = 'var _darwinArgsJSON = "' + JSON.stringify(args) + '";\n'
      call = argsJSON + '_darwinFunc.apply(this, JSON.parse(_darwinArgsJSON));\n'

      finalCode = @_dependencies.toString() + '\n' + @_functionCode.toString() + '\n' + call
      verificationResult = @_verifyCode(finalCode)

      if verificationResult
        run = new Function('context', 'success', 'error', finalCode)
        run(null, ((result) =>
          success(result)
        ), ((err) =>
          # TODO: error description
          error(err)
        ))
      else
        error(new darwin.Error('The code contains forbidden calls.'))
    )