class ExplicitGraphEnumerationManager
  constructor: ->
    @_enumerators = {}

  getEnumerator: (id) =>
    if id of @_enumerators then @_enumerators[id] else null

  createNodeIdsEnumerator: (graph, firstNodeId) =>
    id = darwin.Utils.uuid()
    @_enumerators[id] = new darwin.ExplicitGraphNodeIdsEnumerator(graph, firstNodeId)
    id

  createInboundEdgeIdsEnumerator: (graph, nodeId) =>
    id = darwin.Utils.uuid()
    @_enumerators[id] = new darwin.ExplicitGraphInboundEdgeIdsEnumerator(graph, nodeId)
    id

  createOutboundEdgeIdsEnumerator: (graph, nodeId) =>
    id = darwin.Utils.uuid()
    @_enumerators[id] = new darwin.ExplicitGraphOutboundEdgeIdsEnumerator(graph, nodeId)
    id

  deleteEnumerator: (id) =>
    if id of @_enumerators
      delete @_enumerators[id]

darwin.ExplicitGraphEnumerationManager = new ExplicitGraphEnumerationManager
