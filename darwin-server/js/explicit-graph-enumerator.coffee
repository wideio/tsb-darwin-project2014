class darwin.ExplicitGraphNodeIdsEnumerator
  constructor: (graph, firstNodeId) ->
    @_graph = graph

    @_firstNodeId = firstNodeId

    @_state = firstNodeId

    @_queue = [firstNodeId]

    @_visited = []

  hasNext: =>
    @_queue.length > 0

  # TODO: First node is not initialised properly!
  # Using BFS
  next: =>
    new Promise((success, error) =>
      @_graph.getNodes([@_queue[0]]).then(((response) =>
        node = response.result[0]

        nodeId = @_queue.shift()
        outboundEdgeRefs = node['outbound-edge-references']

        if nodeId not in @_visited
          for edgeId, edgeRef of outboundEdgeRefs
            endNodeId = edgeRef['end-node-id']
            if endNodeId not in @_visited
              @_queue.push(endNodeId)

        @_visited.push(nodeId)
        success(nodeId)
      ), ((err) =>
        error(new darwin.Error('Failed to get next node ID.', err.message))
      ))
    )

  reset: =>
    @_state = @_firstNodeId
    @_queue = [@_firstNodeId]



class darwin.ExplicitGraphInboundEdgeIdsEnumerator
  constructor: (graph, nodeId, sliceLength=10) ->
    @_graph = graph

    @_nodeId = nodeId

    @_state = null

    @_sliceLength = sliceLength

    @_data = []

    @_currentIteration = 0

    @_finished = false

  _refill: =>
    new Promise((success, error) =>
      iteration = () =>
        if @_data.length < @_sliceLength
          @_graph.getNodes([@_nodeId]).then(((response) =>
            node = response.result[0]
            inboundEdgeRefs = node['inbound-edge-references']
            inboundEdgeIds = Object.keys(inboundEdgeRefs)

            if @_currentIteration < inboundEdgeIds.length
              edge = inboundEdgeRefs[inboundEdgeIds[@_currentIteration]]
              @_data.push({
                startNodeId: edge['start-node-id'],
                edgeId: inboundEdgeIds[@_currentIteration]
              })
              ++@_currentIteration
              iteration()
            else
              @_finished = true
              success()
          ), (() =>

          ))
        else
          success()

      iteration()
    )

  hasNext: =>
    if @_data.length is 0 and @_finished then false else true

  next: =>
    new Promise((success, error) =>
      if @_data.length <= 1
        @_refill().then((() =>
          success(@_data.shift())
        ))
      else
        success(@_data.shift())
    )

  reset: =>
    @_state = null
    @_data = []
    @_currentIteration = 0
    @_finished = false



class darwin.ExplicitGraphOutboundEdgeIdsEnumerator
  constructor: (graph, nodeId, sliceLength=10) ->
    @_graph = graph

    @_nodeId = nodeId

    @_state = null

    @_sliceLength = sliceLength

    @_data = []

    @_currentIteration = 0

    @_finished = false

  _refill: =>
    new Promise((success, error) =>
      iteration = () =>
        if @_data.length < @_sliceLength
          @_graph.getNodes([@_nodeId]).then(((response) =>
            node = response.result[0]
            outboundEdgeRefs = node['outbound-edge-references']
            outboundEdgeIds = Object.keys(outboundEdgeRefs)

            if @_currentIteration < outboundEdgeIds.length
              edge = outboundEdgeRefs[outboundEdgeIds[@_currentIteration]]
              @_data.push({
                startNodeId: @_nodeId,
                edgeId: outboundEdgeIds[@_currentIteration]
              })
              ++@_currentIteration
              iteration()
            else
              @_finished = true
              success()
          ), (() =>

          ))
        else
          success()

      iteration()
    )

  hasNext: =>
    if @_data.length is 0 and @_finished then false else true

  next: =>
    new Promise((success, error) =>
      if @_data.length <= 1
        @_refill().then((() =>
          success(@_data.shift())
        ))
      else
        success(@_data.shift())
    )

  reset: =>
    @_state = null
    @_data = []
    @_currentIteration = 0
    @_finished = false
