class ExplicitGraphManager extends darwin.EventProvider
  constructor: ->
    super

    @_graphsDb = null

    @_graphs = new darwin.ExplicitGraphCollection

#  _initialiseReplication: =>
#    new Promise((success, error) =>
#      darwin.RemoteManager.on('initialise-success', () =>
#        remoteUserAddresses = darwin.RemoteManager.getRemoteUserAddresses()
#
#        for userAddress in remoteUserAddresses
#          coordinates = darwin.Utils.extractRemoteAddress(userAddress)
#          username = coordinates.username
#          address = coordinates.address
#
#          dbAddress = 'http://' + address + '/' + username + '-__darwin-graphs'
#          @_graphsDb.replicate(dbAddress, darwin.Config.REPLICATION.DIRECTION.FROM, { live: true })
#
#        success()
#      )
#    )

  initialise: =>
    new Promise((success, error) =>
      darwin.DatabaseManager.on('initialise-core-dbs-success', () =>
        @_graphsDb = darwin.DatabaseManager.getCoreDbs().getController('graphs')

        @_triggerEvent('initialise-success')
        darwin.LocalConnection.triggerEvent('explicit-graph-manager-initialise-success')
        console.log('Initialised explicit graph manager.')
        success()
      )
    )

  # Gets the methods which are accessible by Darwin client and remote users
  getPublicMethods: =>
    forbiddenMethods = ['initialise', 'getPublicMethods', 'getTargetAddress']
    methods = []
    for methodName of @
      if methodName[0] isnt '_' and methodName not in forbiddenMethods
        methods.push(methodName)
    methods

  getGraph: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      id = parameters[0]

      @_graphs.getGraph(id).then(((graph) =>
        success(new darwin.Success({ id: graph.getId() }))
      ), ((err) =>
        error(new darwin.Error('The graph does not exist.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  createGraph: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      attributes = parameters[0]
      distribution = parameters[1]

      @_graphs.createGraph(attributes, distribution).then(((id) =>
        success(new darwin.Success({ id: id }))
      ), ((err) =>
        error(new darwin.Error('Failed to create a graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  getGraphAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.getGraphAttributes().then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  updateGraphAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      changes = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.updateGraphAttributes(changes).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  deleteGraphAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      keys = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.deleteGraphAttributes(keys).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  createNodeIdsEnumerator: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.createNodeIdsEnumerator().then(success, error)
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  hasNextNodeId: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      enumeratorId = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.hasNextNodeId(enumeratorId).then(success, error)
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  getNextNodeId: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      enumeratorId = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.getNextNodeId(enumeratorId).then(success, error)
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  checkNodesExistence: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeIds = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.checkNodesExistence(nodeIds).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  createNodes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      allAttributes = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.createNodes(allAttributes).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  deleteNodes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeIds = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.deleteNodes(nodeIds).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  getNodeAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeId = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.getNodeAttributes(nodeId).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  updateNodeAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeId = parameters[1]
      changes = parameters[2]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.updateNodeAttributes(nodeId, changes).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  deleteNodeAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeId = parameters[1]
      keys = parameters[2]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.deleteNodeAttributes(nodeId, keys).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  createInboundEdgeIdsEnumerator: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeId = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.createInboundEdgeIdsEnumerator(nodeId).then(success, error)
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  createOutboundEdgeIdsEnumerator: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeId = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.createOutboundEdgeIdsEnumerator(nodeId).then(success, error)
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  hasNextEdgeId: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      enumeratorId = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.hasNextEdgeId(enumeratorId).then(success, error)
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  getNextEdgeId: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      enumeratorId = parameters[1]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.getNextEdgeId(enumeratorId).then(success, error)
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  getEdges: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeId = parameters[1]
      edgeIds = parameters[2]
      direction = parameters[3]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.getEdges(nodeId, edgeIds, direction).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  createEdges: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      nodeId = parameters[1]
      edgeData = parameters[2]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.createEdge(nodeId, edgeData).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  getEdgeAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      startNodeId = parameters[1]
      edgeId = parameters[2]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.getEdgeAttributes(startNodeId, edgeId).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  updateEdgeAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      startNodeId = parameters[1]
      edgeId = parameters[2]
      changes = parameters[3]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.updateEdgeAttributes(startNodeId, edgeId, changes).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  deleteEdgeAttributes: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      graphId = parameters[0]
      startNodeId = parameters[1]
      edgeId = parameters[2]
      keys = parameters[3]

      @_graphs.getGraph(graphId).then(((graph) =>
        graph.deleteEdgeAttributes(startNodeId, edgeId, keys).then(success, error)
      ), ((err) =>
        error(new darwin.Error('Failed to get graph.', err.message))
      ))
    else
      error(new darwin.Error('This operation is forbidden.'))


darwin.ExplicitGraphManager = new ExplicitGraphManager
