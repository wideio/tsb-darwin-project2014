class ExplicitGraphUtils
  constructor: ->
    null

  # TODO: It is under construction
  # Returns an address of the user who owns a graph, node or edge of a given id
  getTargetAddress: (id) =>
    new Promise((success, error) =>
      friends = darwin.RemoteManager.getRemoteUserAddresses()
      friends.unshift(darwin.Core.getUserAddress())

      bufferedId = darwin.Utils.stringToBuffer(id)
      darwin.CryptographyManager.digest(bufferedId, 'SHA-1').then(((resultBuffer) =>

        # Gets an integer representation of the result
        hash = parseInt('0x' + darwin.Utils.bufferToHexString(resultBuffer))
        success(new darwin.Success(friends[hash %% friends.length]))

      ), ((err) =>
        error(new darwin.Error('Failed to get target address of the object', err.message))
      ))
    )

darwin.ExplicitGraphUtils = new ExplicitGraphUtils
