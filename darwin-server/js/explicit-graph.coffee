class darwin.ExplicitGraph
  constructor: (id, ownerAddress) ->
    @_id = id

    @_ownerAddress = ownerAddress

    @_distribution = null

    @_firstNodeId = null

    @_graphsDb = null

    @_localNodesDb = null

    @_remoteNodesDbs = {}

  initialise: =>
    new Promise((success, error) =>
      darwin.DatabaseManager.on('initialise-core-dbs-success', () =>
        @_graphsDb = darwin.DatabaseManager.getCoreDbs().getController('graphs')

        @_graphsDb.get(@_id).then(((graphData) =>

          type = if 'type' of graphData then graphData['type'] else null
          distribution = if 'distribution' of graphData then graphData['distribution'] else null
          firstNodeId = if 'first-node-id' of graphData then graphData['first-node-id'] else null

          if type is darwin.Config.GRAPH.TYPE.EXPLICIT
            @_distribution = distribution
            @_firstNodeId = firstNodeId
            @_initialiseDbs()
            success({ id: @_id })
          else
            error(new darwin.Error('Failed to get a graph because the ID points to the graph which is not explicit.'))

        ), ((err) =>
          error(new darwin.Error('Failed to get a graph with given uuid.', err.message))
        ))
      )
    )

  # Initialises local nodes database controller and synchronises databases
  _initialiseDbs: =>
    @_localNodesDb = new darwin.Database(darwin.Core.getUserAddress(), darwin.Core.getPassword(), '__graph', @_id + '-node-subset')

    @_graphsDb.sync()
    @_localNodesDb.sync()

  # Gets database controller of the proper nodes db in the distributed network in which the node of a given ID is saved
  _getNodesDb: (nodeId) =>
    new Promise((success, error) =>
      if @_distribution is darwin.Config.GRAPH.DISTRIBUTION.DISTRIBUTED
        darwin.ExplicitGraphUtils.getTargetAddress(nodeId).then(((response) =>
          userAddress = response.result

          if userAddress is darwin.Core.getUserAddress()
            success(new darwin.Success(@_localNodesDb))
          else if userAddress of @_remoteNodesDbs
            success(new darwin.Success(@_remoteNodesDbs[userAddress]))
          else
            @_remoteNodesDbs[userAddress] = new darwin.Database(userAddress, null, '__graph', @_id + '-node-subset')
            success(new darwin.Success(@_remoteNodesDbs[userAddress]))
        ), error)
      else
        success(new darwin.Success(@_localNodesDb))
    )

  # Updates distribution of the graph
  _updateDistribution: (distribution) =>
    new Promise((success, error) =>
      if distribution in [darwin.Config.GRAPH.DISTRIBUTION.CONSOLIDATED, darwin.Config.GRAPH.DISTRIBUTION.DISTRIBUTED]
        @_distribution = distribution
        @_graphsDb.set(@_id, { 'distribution': distribution }).then(success, error)
      else
        error('Invalid distribution type.')
    )

  # Updates first node ID of the graph
  _updateFirstNodeId: (nodeId) =>
    new Promise((success, error) =>
      @_firstNodeId = nodeId
      @_graphsDb.set(@_id, { 'first-node-id': nodeId }).then(success, error)
    )

  # Simply gets graph ID
  getId: =>
    @_id

  getGraphAttributes: =>
    new Promise((success, error) =>
      @_graphsDb.get(@_id).then(((result) =>
        if 'attributes' of result
          success(result['attributes'])
        else
          error(new darwin.Error('Failed to get graph attributes.'))
      ), ((err) =>
        error(new darwin.Error('Failed to get graph attributes.', err.message))
      ))
    )

  updateGraphAttributes: (changes) =>
    new Promise((success, error) =>
      @getGraphAttributes().then(((attributes) =>
        for key of changes
          attributes[key] = changes[key]

        @_graphsDb.set(@_id, { 'attributes': attributes }).then((() =>
          success({ status: true })
        ), ((err) =>
          error(new darwin.Error('Failed to update graph attributes.', err.message))
        ))
      ), error)
    )

  deleteGraphAttributes: (keys) =>
    new Promise((success, error) =>
      @getGraphAttributes().then(((attributes) =>
        for key in keys
          if key of attributes
            delete attributes[key]

        @_graphsDb.set(@_id, { 'attributes': attributes }).then((() =>
          success({ status: true })
        ), ((err) =>
          error(new darwin.Error('Failed to delete graph attributes.', err.message))
        ))
      ), error)
    )

  createNodeIdsEnumerator: =>
    new Promise((success, error) =>
      enumeratorId = darwin.ExplicitGraphEnumerationManager.createNodeIdsEnumerator(@, @_firstNodeId)
      success(new darwin.Success(enumeratorId))
    )

  hasNextNodeId: (enumeratorId) =>
    new Promise((success, error) =>
      # TODO: Enumerator uuid shall be prevented from being changed!
      enumerator = darwin.ExplicitGraphEnumerationManager.getEnumerator(enumeratorId)

      hasNext = enumerator.hasNext()

      info = { enumeratorId: enumeratorId }
      success(new darwin.Success(hasNext, info))
    )

  getNextNodeId: (enumeratorId) =>
    new Promise((success, error) =>
      # TODO: Enumerator uuid shall be prevented from being changed!
      enumerator = darwin.ExplicitGraphEnumerationManager.getEnumerator(enumeratorId)

      if enumerator.hasNext()
        enumerator.next().then((nodeId) =>
          info = { enumeratorId: enumeratorId }
          success(new darwin.Success(nodeId, info))
        )
      else
        error(new darwin.Error('The enumerator reached the end of the collection.'))
    )

  checkNodesExistence: (nodeIds) =>
    new Promise((success, error) =>
      result = []
      for i in [0..nodeIds.length - 1]
        result.push(null)

      info = {
        received: 0,
        failed: 0
      }

      # Single step of the recursive "iteration"
      iteration = (i) =>
        @_getNodesDb(nodeIds[i]).then(((response) =>
          nodesDb = response.result
          nodesDb.get(nodeIds[i]).then((() =>
            result[i] = nodeIds[i]

            ++info.received
            if info.received + info.failed is nodeIds.length
              success(new darwin.Success(result))
          ), (() =>
            ++info.failed
            if info.received + info.failed is nodeIds.length
              success(new darwin.Success(result))
          ))
        ), error)

        if i < nodeIds.length - 1
          iteration(i + 1)

      iteration(0)
    )

  getNodes: (nodeIds) =>
    new Promise((success, error) =>
      result = []
      for i in [0..nodeIds.length - 1]
        result.push(null)

      info = {
        received: 0,
        failed: 0
      }

      # Single step of the recursive "iteration"
      iteration = (i) =>
        @_getNodesDb(nodeIds[i]).then(((response) =>
          nodesDb = response.result
          nodesDb.get(nodeIds[i]).then(((node) =>
            result[i] = node

            ++info.received
            if info.received + info.failed is nodeIds.length
              success(new darwin.Success(result))
          ), (() =>
            ++info.failed
            if info.received + info.failed is nodeIds.length
              success(new darwin.Success(result))
          ))
        ), error)

        if i < nodeIds.length - 1
          iteration(i + 1)

      iteration(0)
    )

  createNodes: (allAttributes) =>
    new Promise((success, error) =>
      nodeIds = []
      docs = []
      for attributes in allAttributes
        nodeId = darwin.Utils.uuid()

        nodeIds.push(nodeId)
        docs.push({
          'attributes': attributes,
          'inbound-edge-references': {},
          'outbound-edge-references': {}
        })

      complete = () =>
        success(new darwin.Success(nodeIds))
        @_localNodesDb.sync()

      info = {
        created: 0,
        failed: 0
      }

      # TODO: It can be even faster if we would use write batch for the same db controllers
      # Single step of the recursive "iteration"
      iteration = (i) =>
        @_getNodesDb(nodeIds[i]).then(((response) =>
          nodesDb = response.result
          nodesDb.set(nodeIds[i], docs[i]).then((() =>
            # Updates first node ID if the graph is empty (== firstNodeId is null)
            if @_firstNodeId is null
              @_updateFirstNodeId(nodeIds[i])

            ++info.created
            if info.created + info.failed is nodeIds.length
              complete()
          ), (() =>
            nodeIds[i] = null

            ++info.failed
            if info.created + info.failed is nodeIds.length
              complete()
          ))
        ), error)

        if i < nodeIds.length - 1
          iteration(i + 1)

      iteration(0)
    )

  deleteNodes: (nodeIds) =>
    new Promise((success, error) =>
      nodeStatuses = []
      for i in [0..nodeIds.length - 1]
        nodeStatuses.push(false)

      complete = () =>
        success(new darwin.Success(nodeStatuses))
        @_localNodesDb.sync()

      info = {
        deleted: 0,
        failed: 0
      }

      # Single step of the recursive "iteration"
      iteration = (i) =>
        @_getNodesDb(nodeIds[i]).then(((response) =>
          nodesDb = response.result
          nodesDb.delete(nodeIds[i]).then((() =>
            # TODO: Update firstNodeId!

            ++info.deleted
            nodeStatuses[i] = true
            if info.deleted + info.failed is nodeIds.length
              complete()
          ), (() =>
            ++info.failed
            if info.deleted + info.failed is nodeIds.length
              complete()
          ))
        ), error)

        if i < nodeIds.length - 1
          iteration(i + 1)

      iteration(0)
    )

  getNodeAttributes: (nodeId) =>
    new Promise((success, error) =>
      @_getNodesDb(nodeId).then(((response) =>
        nodesDb = response.result
        nodesDb.get(nodeId).then(((result) =>
          if 'attributes' of result
            success(new darwin.Success(result['attributes'], { ownerAddress: @_userAddress }))
          else
            error(new darwin.Error('Failed to get node attributes.'))
        ), ((err) =>
          error(new darwin.Error('Failed to get the node.', err.message))
        ))
      ), ((err) =>
        error(new darwin.Error('Failed to determine database which contains the node.', err.message))
      ))
    )

  updateNodeAttributes: (nodeId, changes) =>
    new Promise((success, error) =>
      @getNodeAttributes(nodeId).then(((response) =>
        attributes = response.result

        for key of changes
          attributes[key] = changes[key]

        @_getNodesDb(nodeId).then(((response) =>
          nodesDb = response.result
          nodesDb.set(nodeId, { 'attributes': attributes }).then((() =>
            success(new darwin.Success(true))
            nodesDb.sync()
          ), ((err) =>
            error(new darwin.Error('Failed to update node attributes.', err.message))
          ))
        ), ((err) =>
          error(new darwin.Error('Failed to determine database which contains the node.', err.message))
        ))
      ), error)
    )

  deleteNodeAttributes: (nodeId, keys) =>
    new Promise((success, error) =>
      @getNodeAttributes(nodeId).then(((response) =>
        attributes = response.result

        for key in keys
          if key of attributes
            delete attributes[key]

        @_getNodesDb(nodeId).then(((response) =>
          nodesDb = response.result
          nodesDb.set(nodeId, { 'attributes': attributes }).then((() =>
            success(new darwin.Success(true))
            nodesDb.sync()
          ), ((err) =>
            error(new darwin.Error('Failed to delete node attributes.', err.message))
          ))
        ), ((err) =>
          error(new darwin.Error('Failed to determine database which contains the node.', err.message))
        ))
      ), error)
    )

  createInboundEdgeIdsEnumerator: (nodeId) =>
    new Promise((success, error) =>
      enumeratorId = darwin.ExplicitGraphEnumerationManager.createInboundEdgeIdsEnumerator(@, nodeId)
      success(new darwin.Success(enumeratorId))
    )

  createOutboundEdgeIdsEnumerator: (nodeId) =>
    new Promise((success, error) =>
      enumeratorId = darwin.ExplicitGraphEnumerationManager.createOutboundEdgeIdsEnumerator(@, nodeId)
      success(new darwin.Success(enumeratorId))
    )

  hasNextEdgeId: (enumeratorId) =>
    new Promise((success, error) =>
      # TODO: Enumerator uuid shall be prevented from being changed!
      enumerator = darwin.ExplicitGraphEnumerationManager.getEnumerator(enumeratorId)

      hasNext = enumerator.hasNext()

      info = { enumeratorId: enumeratorId }
      success(new darwin.Success(hasNext, info))
    )

  getNextEdgeId: (enumeratorId) =>
    new Promise((success, error) =>
      # TODO: Enumerator uuid shall be prevented from being changed!
      enumerator = darwin.ExplicitGraphEnumerationManager.getEnumerator(enumeratorId)

      if enumerator.hasNext()
        enumerator.next().then((nodeId) =>
          info = { enumeratorId: enumeratorId }
          success(new darwin.Success(nodeId, info))
        )
      else
        error(new darwin.Error('The enumerator reached the end of the collection.'))
    )

  getEdges: (nodeId, edgeIds, direction) =>
    new Promise((success, error) =>
      @_getNodesDb(nodeId).then(((response) =>
        nodesDb = response.result
        nodesDb.get(nodeId).then(((nodeDoc) =>
          if 'outbound-edge-references' of nodeDoc and 'inbound-edge-references' of nodeDoc
            result = []
            for i in [1..edgeIds.length]
              result.push(null)

            if direction is darwin.Config.GRAPH.EDGE_TYPE.OUTBOUND
              edgeRefs = nodeDoc['outbound-edge-references']
            else
              edgeRefs = nodeDoc['inbound-edge-references']

            for i in [0..edgeIds.length - 1]
              if edgeIds[i] of edgeRefs
                result[i] = edgeIds[i]

            success(new darwin.Success(result))
          else
            error(new darwin.Error('Failed to get edge references from the node.'))
        ), ((err) =>
          error(new darwin.Error('Failed to get the start node.', err.message))
        ))
      ), ((err) =>
        error(new darwin.Error('Failed to determine database which contains the node.', err.message))
      ))
    )

  # Creates edges from the start node to the nodes specified in edgesData
  # The most complicated function in the whole project
  createEdge: (startNodeId, edgesData) =>
    new Promise((success, error) =>
      if typeof(edgesData) isnt 'object' or edgesData is null
        edgesData = {}

      # Prepares return value
      ids = []
      for endNodeId, edges of edgesData
        for edge in edges
          ids.push(null)

      # Enriches edgeData with newly generated ids
      for endNodeId of edgesData
        for edge in edgesData[endNodeId]
          edge.id = darwin.Utils.uuid()

      # Edge IDs are the keys of the edges data
      endNodeIds = Object.keys(edgesData)

      info = {
        succeeded: 0,
        failed: 0
      }

      # The function which is called when all the edges are created
      complete = () =>
        success(new darwin.Success(ids))
        @_localNodesDb.sync()

      # The function which is called when some read/write operation succeeds or fails during edges creation
      singleSuccess = () =>
        ++info.succeeded
        if info.succeeded + info.failed is endNodeIds.length
          complete()

      # The function which is called when some read/write operation fails during edges creation
      singleFail = () =>
        ++info.failed
        if info.succeeded + info.failed is endNodeIds.length
          complete()

      # Updates outbound edge references in the start node
      updateStartNode = (nodesDb) =>
        new Promise((stepFinished) =>
          nodesDb.get(startNodeId).then(((startNodeDoc) =>
            outboundEdgeRefs = startNodeDoc['outbound-edge-references']

            for endNodeId, edges of edgesData
              for edge in edges
                outboundEdgeRefs[edge.id] = {
                  'attributes': (if 'attributes' of edge then edge.attributes else {}),
                  'end-node-id': endNodeId,
                  'end-node-owner-address': nodesDb.getOwnerAddress()
                }

            # Sets outbound references
            nodesDb.set(startNodeId, { 'outbound-edge-references': outboundEdgeRefs }).then(stepFinished, stepFinished)

          ), stepFinished)
        )

      # Updates inbound edge references in the end node
      updateEndNode = (nodesDb, endNodeId, startNodeOwnerAddress) =>
        new Promise((stepFinished) =>
          nodesDb.get(endNodeId).then(((endNodeDoc) =>
            inboundEdgeRefs = endNodeDoc['inbound-edge-references']

            for edge in edgesData[endNodeId]
              inboundEdgeRefs[edge.id] = {
                'start-node-id': startNodeId,
                'start-node-owner-address': startNodeOwnerAddress
              }

            # Sets inbound references
            nodesDb.set(endNodeId, { 'inbound-edge-references': inboundEdgeRefs }).then((() =>
              for edge in edgesData[endNodeId]
                ids[edge.tempId] = edge.id

              singleSuccess()
              stepFinished()
            ), (() =>
              singleFail()
              stepFinished()
            ))

          ), (() =>
            singleFail()
            stepFinished()
          ))
        )

      @_getNodesDb(startNodeId).then(((response1) =>
        nodesDb1 = response1.result
        startNodeOwnerAddress = nodesDb1.getOwnerAddress()

        # Main recursive "iteration"
        iteration = (i) =>
          endNodeId = endNodeIds[i]

          @_getNodesDb(endNodeId).then(((response2) =>
            nodesDb2 = response2.result

            updateEndNode(nodesDb2, endNodeId, startNodeOwnerAddress).then(() =>
              if i < endNodeIds.length - 1
                iteration(i + 1)
            )
          ), (() =>
            if i < endNodeIds.length - 1
              iteration(i + 1)
          ))

        updateStartNode(nodesDb1).then(() =>
          iteration(0)
        )

      ), ((err) =>
        error(new darwin.Error('Failed to determine database which contains the start node.', err.message))
      ))
    )

  getEdgeAttributes: (startNodeId, edgeId) =>
    new Promise((success, error) =>
      @_getNodesDb(startNodeId).then(((response) =>
        nodesDb = response.result
        nodesDb.get(startNodeId).then(((startNodeDoc) =>

          if 'outbound-edge-references' of startNodeDoc
            outboundEdgeRefs = startNodeDoc['outbound-edge-references']

            if edgeId of outboundEdgeRefs and 'attributes' of outboundEdgeRefs[edgeId] and typeof(outboundEdgeRefs[edgeId].attributes) is 'object'
              success(new darwin.Success(outboundEdgeRefs[edgeId].attributes))
            else
              error(new darwin.Error('Failed to get edge attributes.'))
          else
            error(new darwin.Error('Failed to get outbound edge references from the start node.'))

        ), ((err) =>
          error(new darwin.Error('Failed to get the start node.', err.message))
        ))
      ), ((err) =>
        error(new darwin.Error('Failed to determine database which contains the start node.', err.message))
      ))
    )

  updateEdgeAttributes: (startNodeId, edgeId, changes) =>
    new Promise((success, error) =>
      @_getNodesDb(startNodeId).then(((response) =>
        nodesDb = response.result
        nodesDb.get(startNodeId).then(((startNodeDoc) =>

          if 'outbound-edge-references' of startNodeDoc
            outboundEdgeRefs = startNodeDoc['outbound-edge-references']

            if edgeId of outboundEdgeRefs and 'attributes' of outboundEdgeRefs[edgeId] and typeof(outboundEdgeRefs[edgeId].attributes) is 'object'
              # Saves changes in the attributes
              for key of changes
                outboundEdgeRefs[edgeId].attributes[key] = changes[key]

              # Saves node document
              nodesDb.set(startNodeId, { 'outbound-edge-references': outboundEdgeRefs }).then((() =>
                success(new darwin.Success(true))
                nodesDb.sync()
              ), ((err) =>
                error(new darwin.Error('Failed to update edge attributes.', err.message))
              ))
            else
              error(new darwin.Error('Failed to get existing edge attributes.'))
          else
            error(new darwin.Error('Failed to get outbound edge references from the start node.'))

        ), ((err) =>
          error(new darwin.Error('Failed to get the start node.', err.message))
        ))
      ), ((err) =>
        error(new darwin.Error('Failed to determine database which contains the start node.', err.message))
      ))
    )

  deleteEdgeAttributes: (startNodeId, edgeId, keys) =>
    new Promise((success, error) =>
      @_getNodesDb(startNodeId).then(((response) =>
        nodesDb = response.result
        nodesDb.get(startNodeId).then(((startNodeDoc) =>

          if 'outbound-edge-references' of startNodeDoc
            outboundEdgeRefs = startNodeDoc['outbound-edge-references']

            if edgeId of outboundEdgeRefs and 'attributes' of outboundEdgeRefs[edgeId] and typeof(outboundEdgeRefs[edgeId].attributes) is 'object'
              # Deletes keys from the attributes
              for key of keys
                delete outboundEdgeRefs[edgeId].attributes[key]

              # Saves node document
              nodesDb.set(startNodeId, { 'outbound-edge-references': outboundEdgeRefs }).then((() =>
                success(new darwin.Success(true))
                nodesDb.sync()
              ), ((err) =>
                error(new darwin.Error('Failed to delete edge attributes.', err.message))
              ))
            else
              error(new darwin.Error('Failed to get existing edge attributes.'))
          else
            error(new darwin.Error('Failed to get outbound edge references from the start node.'))

        ), ((err) =>
          error(new darwin.Error('Failed to get the start node.', err.message))
        ))
      ), ((err) =>
        error(new darwin.Error('Failed to determine database which contains the start node.', err.message))
      ))
    )


class darwin.ExplicitGraphCollection
  constructor: ->
    @_graphs = {}

    @_graphsDb = null

  getGraph: (id) =>
    new Promise((success, error) =>
      if id not of @_graphs
        # TODO: Handle other user addresses
        graph = new darwin.ExplicitGraph(id, darwin.Core.getUserAddress())
        graph.initialise().then(((result) =>
          @_graphs[result.id] = graph
          success(graph)
        ), ((err) =>
          error(err)
        ))
      else
        success(@_graphs[id])
    )

  createGraph: (attributes, distribution=darwin.Config.GRAPH.DISTRIBUTION.CONSOLIDATED) =>
    new Promise((success, error) =>
      create = () =>
        data = {
          'attributes': attributes,
          'distribution': distribution,
          'first-node-id': null,
          'type': 'explicit'
        }

        id = darwin.Utils.uuid()

        @_graphsDb.set(id, data).then((() =>
          @_graphsDb.sync()
          success(id)
        ), error)

      if @_graphsDb isnt null
        create()
      else
        darwin.DatabaseManager.on('initialise-core-dbs-success', () =>
          @_graphsDb = darwin.DatabaseManager.getCoreDbs().getController('graphs')
          create()
        )
    )