class ImplicitGraphEnumerationManager
  constructor: ->
    @_enumerators = {}

  getEnumerator: (uuid) =>
    if uuid of @_enumerators then @_enumerators[uuid] else null

  createEnumerator: (f, initialState=[null], sliceLength=10) =>
    uuid = darwin.Utils.uuid()
    @_enumerators[uuid] = new darwin.ImplicitGraphEnumerator(f, initialState, sliceLength)
    uuid

  deleteEnumerator: (uuid) =>
    if uuid of @_enumerators
      delete @_enumerators[uuid]

darwin.ImplicitGraphEnumerationManager = new ImplicitGraphEnumerationManager
