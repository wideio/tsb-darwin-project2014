// Generated by CoffeeScript 1.8.0
(function() {
  var ImplicitGraphEnumerationManager,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  ImplicitGraphEnumerationManager = (function() {
    function ImplicitGraphEnumerationManager() {
      this.deleteEnumerator = __bind(this.deleteEnumerator, this);
      this.createEnumerator = __bind(this.createEnumerator, this);
      this.getEnumerator = __bind(this.getEnumerator, this);
      this._enumerators = {};
    }

    ImplicitGraphEnumerationManager.prototype.getEnumerator = function(uuid) {
      if (uuid in this._enumerators) {
        return this._enumerators[uuid];
      } else {
        return null;
      }
    };

    ImplicitGraphEnumerationManager.prototype.createEnumerator = function(f, initialState, sliceLength) {
      var uuid;
      if (initialState == null) {
        initialState = [null];
      }
      if (sliceLength == null) {
        sliceLength = 10;
      }
      uuid = darwin.Utils.uuid();
      this._enumerators[uuid] = new darwin.ImplicitGraphEnumerator(f, initialState, sliceLength);
      return uuid;
    };

    ImplicitGraphEnumerationManager.prototype.deleteEnumerator = function(uuid) {
      if (uuid in this._enumerators) {
        return delete this._enumerators[uuid];
      }
    };

    return ImplicitGraphEnumerationManager;

  })();

  darwin.ImplicitGraphEnumerationManager = new ImplicitGraphEnumerationManager;

}).call(this);

//# sourceMappingURL=implicit-graph-enumeration-manager.js.map
