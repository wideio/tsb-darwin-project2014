class darwin.ImplicitGraphEnumerator
  constructor: (f, initialState, sliceLength) ->
    @_function = f

    @_initialState = initialState

    @_state = initialState

    @_finished = false

    @_sliceLength = sliceLength

    @_data = []

  _refill: =>
    new Promise((success, error) =>
      try
        executor = new darwin.Executor(@_function)

        iteration = () =>
          if @_data.length < @_sliceLength
            executor.execute.apply(executor, @_state).then(((state) =>
              if Object.prototype.toString.call(state) isnt '[object Array]'
                state = [state]
              @_state = state
              @_data.push(state)
              iteration()
            ), (() =>
              @_finished = true
              success()
            ))
          else
            success()

        iteration()
      catch
        @_finished = true
    )

  hasNext: =>
    if @_data.length is 0 and @_finished then false else true

  next: =>
    new Promise((success, error) =>
      if @_data.length <= 1
        @_refill().then((() =>
          success(@_data.shift())
        ))
      else
        success(@_data.shift())
    )

  all: =>
    new Promise((success, error) =>
      result = []

      iteration = () =>
        if @hasNext()
          @next().then((element) =>
            result.push(element)
            iteration()
          )
        else
          success(result)

      iteration()
    )

  reset: =>
    @_state = @_initialState
    @_data = []
    @_finished = false
