class darwin.ImplicitGraph
  constructor: (id) ->
    @_id = id

    @_ownerAddress = null

    @_originUrl = {}

    @_graphsDb = null

    @_functions = null

    @_dependencies = null

  initialise: =>
    new Promise((success, error) =>
      darwin.DatabaseManager.on('initialise-core-dbs-success', () =>
        @_graphsDb = darwin.DatabaseManager.getCoreDbs().getController('graphs')

        @_graphsDb.get(@_id).then(((result) =>
          if 'type' of result and  result['type'] is darwin.Config.GRAPH.TYPE.IMPLICIT
            @_ownerAddress = result['owner-address']
            @_functions = result['functions']
            @_dependencies = result['dependencies']
            @_graphsDb.sync()
            success({ id: @_id })
          else
            error(new darwin.Error('Failed to get a graph with given uuid.'))
        ), ((err) =>
          error(new darwin.Error('Failed to get a graph with given uuid.', err.message))
        ))
      )
    )

  getId: =>
    @_id

  getGraphAttributes: =>
    new Promise((success, error) =>
      @_graphsDb.get(@_id).then(((result) =>
        if 'attributes' of result
          success(result['attributes'])
        else
          error(new darwin.Error('Failed to get graph attributes.'))
      ), ((err) =>
        error(new darwin.Error('Failed to get graph attributes.', err.message))
      ))
    )

  updateGraphAttributes: (changes) =>
    new Promise((success, error) =>
      @getGraphAttributes().then(((attributes) =>
        for key of changes
          attributes[key] = changes[key]

        @_graphsDb.set(@_id, { 'attributes': attributes }).then((() =>
          success({ status: true })
        ), ((err) =>
          error(new darwin.Error('Failed to update graph attributes.', err.message))
        ))
      ), error)
    )

  deleteGraphAttributes: (keys) =>
    new Promise((success, error) =>
      @getGraphAttributes().then(((attributes) =>
        for key in keys
          if key of attributes
            delete attributes[key]

        @_graphsDb.set(@_id, { 'attributes': attributes }).then((() =>
          success({ status: true })
        ), ((err) =>
          error(new darwin.Error('Failed to delete graph attributes.', err.message))
        ))
      ), error)
    )

  createNodeIdsEnumerator: =>
    new Promise((success, error) =>
      func = @_functions[darwin.Config.GRAPH.IMPLICIT.FUNCTION.ENUMERATE_NODE_IDS]
      enumeratorId = darwin.ImplicitGraphEnumerationManager.createEnumerator(func)
      success(new darwin.Success(enumeratorId))
    )

  hasNextNodeId: (enumeratorId) =>
    new Promise((success, error) =>
      # TODO: Enumerator uuid shall be prevented from being changed!
      enumerator = darwin.ImplicitGraphEnumerationManager.getEnumerator(enumeratorId)

      hasNext = enumerator.hasNext()

      info = { enumeratorUuid: enumeratorId }
      success(new darwin.Success(hasNext, info))
    )

  getNextNodeId: (enumeratorId) =>
    new Promise((success, error) =>
      # TODO: Enumerator uuid shall be prevented from being changed!
      enumerator = darwin.ImplicitGraphEnumerationManager.getEnumerator(enumeratorId)

      if enumerator.hasNext()
        enumerator.next().then((nodeId) =>
          info = { enumeratorUuid: enumeratorId }
          success(new darwin.Success(nodeId, info))
        )
      else
        error(new darwin.Error('The enumerator reached the end of the collection.'))
    )

  checkNodesExistence: (nodeIds) =>
    new Promise((success, error) =>
      func = @_functions[darwin.Config.GRAPH.IMPLICIT.FUNCTION.NODE_EXISTS]

      result = []
      for i in [1..nodeIds.length]
        result.push(null)

      executor = new darwin.Executor(func)
      loopFunc = (i) =>
        if i < nodeIds.length
          executor.execute(nodeIds[i]).then(((exists) =>
            result[i] = exists
            loopFunc(i + 1)
          ), (() =>
            loopFunc(i + 1)
          ))
        else
          success(new darwin.Success(result))

      loopFunc(0)
    )

  getNodeAttributes: (nodeId) =>
    new Promise((success, error) =>
      func = @_functions[darwin.Config.GRAPH.IMPLICIT.FUNCTION.GET_NODE_ATTRIBUTES]

      executor = new darwin.Executor(func)
      executor.execute(nodeId).then(((attributes) =>
        success(new darwin.Success(attributes))
      ), error)
    )

  createInboundEdgeIdsEnumerator: (nodeId) =>
    new Promise((success, error) =>
      func = @_functions[darwin.Config.GRAPH.IMPLICIT.FUNCTION.ENUMERATE_NODE_INBOUND_EDGE_IDS]
      enumeratorId = darwin.ImplicitGraphEnumerationManager.createEnumerator(func, [nodeId])
      success(new darwin.Success(enumeratorId))
    )

  createOutboundEdgeIdsEnumerator: (nodeId) =>
    new Promise((success, error) =>
      func = @_functions[darwin.Config.GRAPH.IMPLICIT.FUNCTION.ENUMERATE_NODE_OUTBOUND_EDGE_IDS]
      enumeratorId = darwin.ImplicitGraphEnumerationManager.createEnumerator(func, [nodeId])
      success(new darwin.Success(enumeratorId))
    )

  hasNextEdgeId: (enumeratorId) =>
    new Promise((success, error) =>
      # TODO: Enumerator uuid shall be prevented from being changed!
      enumerator = darwin.ImplicitGraphEnumerationManager.getEnumerator(enumeratorId)

      hasNext = enumerator.hasNext()

      info = { enumeratorUuid: enumeratorId }
      success(new darwin.Success(hasNext, info))
    )

  getNextEdgeId: (enumeratorId) =>
    new Promise((success, error) =>
      # TODO: Enumerator uuid shall be prevented from being changed!
      enumerator = darwin.ImplicitGraphEnumerationManager.getEnumerator(enumeratorId)

      if enumerator.hasNext()
        enumerator.next().then((edgeId) =>
          info = { enumeratorUuid: enumeratorId }
          success(new darwin.Success(edgeId, info))
        )
      else
        error(new darwin.Error('The enumerator reached the end of the collection.'))
    )

  getEdgeAttributes: (edgeId) =>
    new Promise((success, error) =>
      func = @_functions[darwin.Config.GRAPH.IMPLICIT.FUNCTION.GET_EDGE_ATTRIBUTES]

      executor = new darwin.Executor(func)
      executor.execute(edgeId).then(((attributes) =>
        success(new darwin.Success(attributes))
      ), error)
    )



class darwin.ImplicitGraphCollection
  constructor: ->
    @_graphs = {}

    @_graphsDb = null

  getGraph: (id) =>
    new Promise((success, error) =>
      if id not of @_graphs
        graph = new darwin.ImplicitGraph(id)
        graph.initialise().then(((result) =>
          @_graphs[result.id] = graph
          success(graph)
          syncDb()
        ), ((err) =>
          error(err)
        ))
      else
        success(@_graphs[id])
        syncDb()

      syncDb = () =>
        darwin.DatabaseManager.on('initialise-core-dbs-success', () =>
          if @_graphsDb is null
            @_graphsDb = darwin.DatabaseManager.getCoreDbs().getController('graphs')
          @_graphsDb.sync()
        )
    )

  createGraph: (functions, dependencies, attributes) =>
    new Promise((success, error) =>
      create = () =>
        data = {
          'attributes': attributes,
          'dependencies': dependencies,
          'distribution': 'consolidated',
          'functions': functions,
          'owner-address': darwin.Core.getUserAddress(),
          'type': 'implicit'
        }

        id = darwin.Utils.uuid()

        @_graphsDb.set(id, data).then((() =>
          @_ownerAddress = darwin.Core.getUserAddress()
          @_graphsDb.sync()
          success(id)
        ), error)

      if @_graphsDb isnt null
        create()
      else
        darwin.DatabaseManager.on('initialise-core-dbs-success', () =>
          @_graphsDb = darwin.DatabaseManager.getCoreDbs().getController('graphs')
          create()
        )
    )