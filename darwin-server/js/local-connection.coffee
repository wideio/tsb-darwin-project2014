# Represents connection between the client and the server
class LocalConnection
  constructor: ->
    @_source = null

    @_origin = null

    @_callbacks = {}

#    @_databaseManagerMethods = darwin.DatabaseManager.getPublicMethods()
#
#    @_localManagerMethods = darwin.LocalManager.getPublicMethods()
#
#    @_remoteManagerMethods = darwin.RemoteManager.getPublicMethods()
#
#    @_distributionManagerMethods = darwin.DistributionManager.getPublicMethods()
#
#    @_explicitGraphManagerMethods = darwin.ExplicitGraphManager.getPublicMethods()

    @_dispatchers = {
      DatabaseManager: darwin.DatabaseManager,
      LocalManager: darwin.LocalManager,
      RemoteManager: darwin.RemoteManager,
      DistributionManager: darwin.DistributionManager,
      ExplicitGraphManager: darwin.ExplicitGraphManager,
      ImplicitGraphManager: darwin.ImplicitGraphManager
    }

    # The methods which are available for the users who are not signed in
    @_openMethods = ['hello', 'signIn', 'autoSignIn']

    window.addEventListener('message', @_receive)

  _log: () =>
    args = ['local-connection @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  _receive: (e) =>
    # TODO: What's process tick?
    if e.data is 'process-tick'
      return

    if @_source is null
      @_source = e.source
      @_origin = e.origin
      @_log('Connected with the client.')

    try
      connObj = e.data

      type = connObj.type

      if type is 'request'
        @_receiveRequest(connObj)
      else if type is 'response'
        @_receiveResponse(connObj)
    catch err
      @_log(err.message)

  _run: (connObj, success, error) =>
    try
      sourceData = connObj.sourceData
      destinationData = connObj.destinationData

      sourceUserAddress = sourceData.userAddress
      destinationUserAddress = destinationData.userAddress
      dispatcherName = destinationData.dispatcherName
      methodName = destinationData.methodName

      if darwin.Core.getUserStatus() or methodName in @_openMethods
        if typeof(sourceUserAddress) isnt 'undefined' and typeof(destinationUserAddress) isnt 'undefined'

          if dispatcherName of @_dispatchers
            f = @_dispatchers[dispatcherName][methodName]
            f(sourceData, destinationData, success, error)
          else
            error(new darwin.Error('Cannot find the specified method.'))

        else
          error(new darwin.Error('Source address or destination address are not properly set.'))
      else
        error(new darwin.Error('The user is not signed in.'))
    catch err
      error(new darwin.Error('Failed to process request.', err.message))

  _receiveResponse: (connObj) =>
    id = connObj.id
    data = connObj.data
    status = connObj.status

    if status
      callback = @_callbacks[id]['success']
    else
      callback = @_callbacks[id]['error']

    if typeof(callback) is 'function'
      callback(data)
    delete @_callbacks[id]

  _receiveRequest: (connObj) =>
    id = connObj.id
    methodName = connObj.methodName

    success = (response, remainingParts=0) =>
      if response and 'getObject' of response
        response = response.getObject()
      @_sendResponse(id, methodName, response, true, remainingParts)

    error = (response, remainingParts=0) =>
      message = if response and 'message' of response then response.message else 'Unknown error.'
      @_sendResponse(id, methodName, { message: message }, false, remainingParts)

    @_run(connObj, success, error)

  _sendResponse: (id, methodName, data, status, remainingParts=0) =>
    if @_source isnt null
      connObj = {
        id: id,
        type: 'response',
        methodName: methodName,
        data: data,
        status: status,
        remainingParts: remainingParts
      }

      @_source.postMessage(connObj, @_origin) # connObj is not stringified to prevent blobs from data loss
    else
      error(new darwin.Error('The connection between the client and server is not established.'))

  sendRequest: (methodName, data, success, error) =>
    if @_source isnt null
      connObj = {
        id: darwin.Utils.uuid(),
        type: 'request',
        methodName: methodName,
        data: data
      }

      @_callbacks[connObj.id] = {
        success: success,
        error: error
      }

      @_source.postMessage(connObj, @_origin) # connObj is not stringified to prevent blobs from data loss
    else
      error(new darwin.Error('The connection between the client and server is not established.'))

  triggerEvent: (name, data=null) =>
    if @_source isnt null
      connObj = {
        id: darwin.Utils.uuid(),
        type: 'event',
        name: name,
        data: data
      }

      @_source.postMessage(connObj, @_origin) # connObj is not stringified to prevent blobs from data loss
    else
      error(new darwin.Error('The connection between the client and server is not established.'))

darwin.LocalConnection = new LocalConnection