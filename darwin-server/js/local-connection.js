// Generated by CoffeeScript 1.8.0
(function() {
  var LocalConnection,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

  LocalConnection = (function() {
    function LocalConnection() {
      this.triggerEvent = __bind(this.triggerEvent, this);
      this.sendRequest = __bind(this.sendRequest, this);
      this._sendResponse = __bind(this._sendResponse, this);
      this._receiveRequest = __bind(this._receiveRequest, this);
      this._receiveResponse = __bind(this._receiveResponse, this);
      this._run = __bind(this._run, this);
      this._receive = __bind(this._receive, this);
      this._log = __bind(this._log, this);
      this._source = null;
      this._origin = null;
      this._callbacks = {};
      this._dispatchers = {
        DatabaseManager: darwin.DatabaseManager,
        LocalManager: darwin.LocalManager,
        RemoteManager: darwin.RemoteManager,
        DistributionManager: darwin.DistributionManager,
        ExplicitGraphManager: darwin.ExplicitGraphManager,
        ImplicitGraphManager: darwin.ImplicitGraphManager
      };
      this._openMethods = ['hello', 'signIn', 'autoSignIn'];
      window.addEventListener('message', this._receive);
    }

    LocalConnection.prototype._log = function() {
      var args, item, _i, _len;
      args = ['local-connection @'];
      for (_i = 0, _len = arguments.length; _i < _len; _i++) {
        item = arguments[_i];
        args.push(item);
      }
      return console.log.apply(console, args);
    };

    LocalConnection.prototype._receive = function(e) {
      var connObj, err, type;
      if (e.data === 'process-tick') {
        return;
      }
      if (this._source === null) {
        this._source = e.source;
        this._origin = e.origin;
        this._log('Connected with the client.');
      }
      try {
        connObj = e.data;
        type = connObj.type;
        if (type === 'request') {
          return this._receiveRequest(connObj);
        } else if (type === 'response') {
          return this._receiveResponse(connObj);
        }
      } catch (_error) {
        err = _error;
        return this._log(err.message);
      }
    };

    LocalConnection.prototype._run = function(connObj, success, error) {
      var destinationData, destinationUserAddress, dispatcherName, err, f, methodName, sourceData, sourceUserAddress;
      try {
        sourceData = connObj.sourceData;
        destinationData = connObj.destinationData;
        sourceUserAddress = sourceData.userAddress;
        destinationUserAddress = destinationData.userAddress;
        dispatcherName = destinationData.dispatcherName;
        methodName = destinationData.methodName;
        if (darwin.Core.getUserStatus() || __indexOf.call(this._openMethods, methodName) >= 0) {
          if (typeof sourceUserAddress !== 'undefined' && typeof destinationUserAddress !== 'undefined') {
            if (dispatcherName in this._dispatchers) {
              f = this._dispatchers[dispatcherName][methodName];
              return f(sourceData, destinationData, success, error);
            } else {
              return error(new darwin.Error('Cannot find the specified method.'));
            }
          } else {
            return error(new darwin.Error('Source address or destination address are not properly set.'));
          }
        } else {
          return error(new darwin.Error('The user is not signed in.'));
        }
      } catch (_error) {
        err = _error;
        return error(new darwin.Error('Failed to process request.', err.message));
      }
    };

    LocalConnection.prototype._receiveResponse = function(connObj) {
      var callback, data, id, status;
      id = connObj.id;
      data = connObj.data;
      status = connObj.status;
      if (status) {
        callback = this._callbacks[id]['success'];
      } else {
        callback = this._callbacks[id]['error'];
      }
      if (typeof callback === 'function') {
        callback(data);
      }
      return delete this._callbacks[id];
    };

    LocalConnection.prototype._receiveRequest = function(connObj) {
      var error, id, methodName, success;
      id = connObj.id;
      methodName = connObj.methodName;
      success = (function(_this) {
        return function(response, remainingParts) {
          if (remainingParts == null) {
            remainingParts = 0;
          }
          if (response && 'getObject' in response) {
            response = response.getObject();
          }
          return _this._sendResponse(id, methodName, response, true, remainingParts);
        };
      })(this);
      error = (function(_this) {
        return function(response, remainingParts) {
          var message;
          if (remainingParts == null) {
            remainingParts = 0;
          }
          message = response && 'message' in response ? response.message : 'Unknown error.';
          return _this._sendResponse(id, methodName, {
            message: message
          }, false, remainingParts);
        };
      })(this);
      return this._run(connObj, success, error);
    };

    LocalConnection.prototype._sendResponse = function(id, methodName, data, status, remainingParts) {
      var connObj;
      if (remainingParts == null) {
        remainingParts = 0;
      }
      if (this._source !== null) {
        connObj = {
          id: id,
          type: 'response',
          methodName: methodName,
          data: data,
          status: status,
          remainingParts: remainingParts
        };
        return this._source.postMessage(connObj, this._origin);
      } else {
        return error(new darwin.Error('The connection between the client and server is not established.'));
      }
    };

    LocalConnection.prototype.sendRequest = function(methodName, data, success, error) {
      var connObj;
      if (this._source !== null) {
        connObj = {
          id: darwin.Utils.uuid(),
          type: 'request',
          methodName: methodName,
          data: data
        };
        this._callbacks[connObj.id] = {
          success: success,
          error: error
        };
        return this._source.postMessage(connObj, this._origin);
      } else {
        return error(new darwin.Error('The connection between the client and server is not established.'));
      }
    };

    LocalConnection.prototype.triggerEvent = function(name, data) {
      var connObj;
      if (data == null) {
        data = null;
      }
      if (this._source !== null) {
        connObj = {
          id: darwin.Utils.uuid(),
          type: 'event',
          name: name,
          data: data
        };
        return this._source.postMessage(connObj, this._origin);
      } else {
        return error(new darwin.Error('The connection between the client and server is not established.'));
      }
    };

    return LocalConnection;

  })();

  darwin.LocalConnection = new LocalConnection;

}).call(this);

//# sourceMappingURL=local-connection.js.map
