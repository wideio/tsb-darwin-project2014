class LocalManager
  constructor: ->
    @_appDbs = null

  # Logs messages
  _log: () =>
    args = ['local-manager @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  getPublicMethods: =>
    forbiddenMethods = ['getPublicMethods']
    methods = []
    for methodName of @
      if methodName[0] isnt '_' and methodName not in forbiddenMethods
        methods.push(methodName)
    methods

  # Uses locally stored credentials to sign in automatically
  autoSignIn: (sourceData, destinationData, success, error) =>
    # Retrieving credentials from the local storage
    credentials = darwin.LocalStorageManager.getCredentials()

    if credentials isnt null
      destinationData.parameters = [credentials['user-address'], credentials['password']]
      @signIn(sourceData, destinationData, ((response) =>
        success(response)
      ), ((err) =>
        error(err)
      ))
    else
      error(new darwin.Error('No credentials stored.'))

  # Signs in Darwin user
  signIn: (sourceData, destinationData, success, error) =>
    parameters = destinationData.parameters

    userAddress = parameters[0]
    password = parameters[1]

    darwin.Core.authenticate(userAddress, password).then(((result) =>
      @_log('Authenticated successfully.')
      darwin.Core.setUserAddress(userAddress)
      darwin.Core.setPassword(password)
      darwin.Core.setupUserStatus()

      # TODO: Refactor it!
      darwin.DatabaseManager.initialise(userAddress, password).then(((result) =>
        darwin.Core.initialise().then(() =>
          success(result)
          darwin.LocalConnection.triggerEvent('sign-in-success')
        )
      ), ((err) =>
        error(err)
        darwin.LocalConnection.triggerEvent('sign-in-error')
      ))
    ), ((err) =>
      error(err)
      darwin.LocalConnection.triggerEvent('sign-in-error')
    ))

  # Signs out Darwin user
  signOut: (sourceData, destinationData, success, error) =>
    coordinates = darwin.Utils.extractRemoteAddress(darwin.Core.getUserAddress())
    darwin.Core.setUserAddress(null)
    darwin.Core.resetUserStatus()

    # Deleting current data
    darwin.LocalStorageManager.deleteCredentials()
    darwin.RemoteManager.clearConnections()

    success(coordinates)

  hello: (sourceData, destinationData, success, error) =>
    parameters = destinationData.parameters

    appId = parameters[0]

    darwin.Core.setAppId(appId)
    success()

  # TODO: Move drive methods to DriveManager or something
  # FILE STORAGE

  getDrive: (sourceData, destinationData, success, error) =>
    parameters = destinationData.parameters

    driveName = parameters[0]
    create = parameters[1]

    drive = darwin.Core.getDrives().getDrive(driveName, create)
    if drive isnt null
      success()
    else
      error(new darwin.Error('The drive does not exist.'))

  getAllDriveNames: (sourceData, destinationData, success, error) =>
    driveNames = darwin.Core.getDrives().getAllDriveNames()
    if driveNames isnt null
      success(driveNames)
    else
      error(new darwin.Error('Unknown error.'))

  listDirectoryNames: (sourceData, destinationData, success, error) =>
    parameters = destinationData.parameters

    driveName = parameters[0]
    internalPath = parameters[1]

    fs = darwin.Core.getDrives().getDrive(driveName)
    fs.listDirectoryNames(internalPath).then(success, error)

  listFileNames: (sourceData, destinationData, success, error) =>
    parameters = destinationData.parameters

    driveName = parameters[0]
    internalPath = parameters[1]

    fs = darwin.Core.getDrives().getDrive(driveName)
    fs.listFileNames(internalPath).then(success, error)

  resolveDirectoryPath: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      driveName = parameters[0]
      internalPath = parameters[1]
      create = parameters[2]

      fs = darwin.Core.getDrives().getDrive(driveName)
      fs.resolveDirectoryPath(internalPath, create).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  deleteDirectory: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      driveName = parameters[0]
      internalPath = parameters[1]

      fs = darwin.Core.getDrives().getDrive(driveName)
      fs.deleteDirectory(internalPath).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  deleteDirectoryRecursively: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      driveName = parameters[0]
      internalPath = parameters[1]

      fs = darwin.Core.getDrives().getDrive(driveName)
      fs.deleteDirectoryRecursively(internalPath).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  resolveFilePath: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      driveName = parameters[0]
      internalPath = parameters[1]
      create = parameters[2]

      fs = darwin.Core.getDrives().getDrive(driveName)
      fs.resolveFilePath(internalPath, create).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  getFileSize: (sourceData, destinationData, success, error) =>
    parameters = destinationData.parameters

    driveName = parameters[0]
    internalPath = parameters[1]

    fs = darwin.Core.getDrives().getDrive(driveName)
    fs.getFileSize(internalPath).then(success, error)

  getFile: (sourceData, destinationData, success, error) =>
    parameters = destinationData.parameters

    driveName = parameters[0]
    internalPath = parameters[1]
    create = parameters[2]
    position = parameters[3]
    length = parameters[4]

    fs = darwin.Core.getDrives().getDrive(driveName)
    fs.getFile(internalPath, create, position, length).then(success, error)

  updateFile: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      driveName = parameters[0]
      internalPath = parameters[1]
      content = parameters[2]
      position = parameters[3]
      truncate = parameters[4]
      type = parameters[5]

      fs = darwin.Core.getDrives().getDrive(driveName)
      fs.updateFile(internalPath, content, position, truncate, type).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  truncateFile: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      driveName = parameters[0]
      internalPath = parameters[1]
      position = parameters[2]

      fs = darwin.Core.getDrives().getDrive(driveName)
      fs.truncateFile(internalPath, position).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  deleteFile: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      driveName = parameters[0]
      internalPath = parameters[1]

      fs = darwin.Core.getDrives().getDrive(driveName)
      fs.deleteFile(internalPath).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

darwin.LocalManager = new LocalManager
