class LocalStorageManager
  # Gets credentials from the local storage
  getCredentials: =>
    appId = darwin.Core.getAppId()

    darwinCredentials = JSON.parse(localStorage.getItem('darwin-credentials'))
    if typeof(darwinCredentials) isnt 'object' or darwinCredentials is null
      darwinCredentials = {}

    if appId of darwinCredentials then darwinCredentials[appId] else null

  # Stores credentials into the local storage
  setCredentials: (userAddress, password) =>
    appId = darwin.Core.getAppId()

    darwinCredentials = JSON.parse(localStorage.getItem('darwin-credentials'))
    if typeof(darwinCredentials) isnt 'object' or darwinCredentials is null
      darwinCredentials = {}

    darwinCredentials[appId] = {
      'user-address': userAddress,
      'password': password
    }

    localStorage.setItem('darwin-credentials', JSON.stringify(darwinCredentials))

  # Deletes credentials from the local storage
  deleteCredentials: =>
    appId = darwin.Core.getAppId()

    darwinCredentials = JSON.parse(localStorage.getItem('darwin-credentials'))
    if typeof(darwinCredentials) isnt 'object' or darwinCredentials is null
      darwinCredentials = {}

    if appId of darwinCredentials
      delete darwinCredentials[appId]
      localStorage.setItem('darwin-credentials', JSON.stringify(darwinCredentials))

  # Gets credentials from the local storage
  getKeys: =>
    userAddress = darwin.Core.getUserAddress()

    darwinKeys = JSON.parse(localStorage.getItem('darwin-keys'))
    if typeof(darwinKeys) isnt 'object' or darwinKeys is null
      darwinKeys = {}

    if userAddress of darwinKeys then darwinKeys[userAddress] else null

  # Stores credentials into the local storage
  setKeys: (publicKey, privateKey) =>
    userAddress = darwin.Core.getUserAddress()

    darwinKeys = JSON.parse(localStorage.getItem('darwin-keys'))
    if typeof(darwinKeys) isnt 'object' or darwinKeys is null
      darwinKeys = {}

    darwinKeys[userAddress] = {
      'public-key': publicKey,
      'private-key': privateKey
    }

    localStorage.setItem('darwin-keys', JSON.stringify(darwinKeys))

  # Deletes credentials from the local storage
  deleteKeys: =>
    userAddress = darwin.Core.getUserAddress()

    darwinKeys = JSON.parse(localStorage.getItem('darwin-keys'))
    if typeof(darwinKeys) isnt 'object' or darwinKeys is null
      darwinKeys = {}

    if userAddress of darwinKeys
      delete darwinKeys[userAddress]
      localStorage.setItem('darwin-keys', JSON.stringify(darwinKeys))

darwin.LocalStorageManager = new LocalStorageManager
