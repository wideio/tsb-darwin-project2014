class PermissionManager extends darwin.EventProvider
  constructor: ->
    super

  updateRemoteUserPermissions: (remoteUserAddress, permissions) =>
    new Promise((success, error) =>
      permissionsDb = darwin.DatabaseManager.getAppHiddenDbs().getController('__permissions')
      permissionsDb.set(remoteUserAddress, {
        'permissions': permissions,
        'timestamp': new Date().getTime().toString()
      }).then(success, error)
      permissionsDb.sync()
    )

darwin.PermissionManager = new PermissionManager
