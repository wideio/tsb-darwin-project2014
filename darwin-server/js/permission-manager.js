// Generated by CoffeeScript 1.8.0
(function() {
  var PermissionManager,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  PermissionManager = (function(_super) {
    __extends(PermissionManager, _super);

    function PermissionManager() {
      this.updateRemoteUserPermissions = __bind(this.updateRemoteUserPermissions, this);
      PermissionManager.__super__.constructor.apply(this, arguments);
    }

    PermissionManager.prototype.updateRemoteUserPermissions = function(remoteUserAddress, permissions) {
      return new Promise((function(_this) {
        return function(success, error) {
          var permissionsDb;
          permissionsDb = darwin.DatabaseManager.getAppHiddenDbs().getController('__permissions');
          permissionsDb.set(remoteUserAddress, {
            'permissions': permissions,
            'timestamp': new Date().getTime().toString()
          }).then(success, error);
          return permissionsDb.sync();
        };
      })(this));
    };

    return PermissionManager;

  })(darwin.EventProvider);

  darwin.PermissionManager = new PermissionManager;

}).call(this);

//# sourceMappingURL=permission-manager.js.map
