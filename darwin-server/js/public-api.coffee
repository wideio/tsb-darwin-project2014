# Represents database proxy which is just a limited instance of DatabaseController
class Database
  constructor: (controller, permissions) ->
    @_controller = controller

    @_permissions = permissions

  all: (options, success, error) =>
    if darwin.Config.PERMISSION.FETCH in @_permissions
      @_controller.all(options).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  get: (key, success, error) =>
    if darwin.Config.PERMISSION.FETCH in @_permissions
      @_controller.get(key).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  set: (key, value, success, error) =>
    if darwin.Config.PERMISSION.MODIFY in @_permissions
      @_controller.set(key, value).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

  delete: (key, success, error) =>
    if darwin.Config.PERMISSION.MODIFY in @_permissions
      @_controller.delete(key).then(success, error)
    else
      error(new darwin.Error('Running this function is forbidden.'))

# Represents public API for distributed computing users
class darwin.PublicAPI
  constructor: (permissions) ->
    @_appDbs = darwin.DatabaseManager.getAppDbs()

    @_permissions = permissions

  getDatabase: (dbName) =>
    controller = @_appDbs.getController(dbName)
    if controller isnt null then new Database(controller, @_permissions) else null
