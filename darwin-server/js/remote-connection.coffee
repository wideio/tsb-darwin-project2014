class darwin.RemoteConnection
  constructor: (remoteUserAddress) ->
    @_remoteUserAddress = remoteUserAddress

    # ID of the PeerJS connection
    @_peerId = null

    # PeerJS connection
    @_peerConnection = null

    # Buffer for awaited callbacks from remote requests
    @_callbacks = {}

    # Available dispatchers which may handle incoming requests
    @_dispatchers = {
      DatabaseManager: darwin.DatabaseManager,
      LocalManager: darwin.LocalManager,
      RemoteManager: darwin.RemoteManager,
      DistributionManager: darwin.DistributionManager,
      ExplicitGraphManager: darwin.ExplicitGraphManager,
      ImplicitGraphManager: darwin.ImplicitGraphManager
    }

  # Updates event handlers to the peerJS connection
  _updateEventHandlers: =>
    new Promise((success, error) =>
      if @_peerConnection isnt null
        @_detachEventHandlers()

        @_peerConnection.on('open', () =>
          # Set _receive as a handler for incoming objects
          @_peerConnection.on('data', @_receive)

          success(new darwin.Success(true))

          console.log('Connected to ' + @_remoteUserAddress + '.')
        )

        @_peerConnection.on('error', (err) =>
          error(new darwin.Error('Failed to connect.'))
        )
      else
        error(new darwin.Error('Failed to attach event handlers to the null remote connection.'))
    )

  # Detaches event handlers from the peerJS connection
  _detachEventHandlers: =>
    if @_peerConnection isnt null
      for name of @_peerConnection._events
        @_peerConnection._events[name] = []

  # Handles incoming objects
  _receive: (connObj) =>
    connObj = JSON.parse(connObj)

    id = connObj.id
    type = connObj.type
    sourceData = connObj.sourceData
    signature = connObj.signature
    serialisedParameters = connObj.serialisedParameters

    passEvent = () =>
      if type is 'request'
        @_receiveRequest(connObj)
      else if type is 'response'
        @_receiveResponse(connObj)

    if type is 'request' and darwin.Config.CRYPTOGRAPHY.ENABLED
      sourceUserAddress = sourceData.sourceUserAddress

      darwin.CryptographyManager.verify(serialisedParameters, signature, sourceUserAddress, (() =>
        @_log('Verified request/response')
        passEvent()
      ), (() =>
        response = new darwin.Error('Failed to verify request/response.')
        @_sendResponse(id, sourceUserAddress, false, response)
      ))
    else
      passEvent()

  # Receives response from the remote user
  _receiveResponse: (connObj) =>
    id = connObj.id
    status = connObj.status
    response = connObj.response

    if id of @_callbacks
      if status
        callback = @_callbacks[id]['success']
      else
        callback = @_callbacks[id]['error']

      if typeof(callback) is 'function'
        callback(response)

      delete @_callbacks[id]
    else
      console.error('Failed to find callback.')

  # Receives request from the remote user
  _receiveRequest: (connObj) =>
    id = connObj.id
    sourceUserAddress = connObj.sourceUserAddress

    success = (response) =>
      if response and 'getObject' of response
        response = response.getObject()
      @_sendResponse(id, sourceUserAddress, true, response)

    error = (response) =>
      @_sendResponse(id, sourceUserAddress, false, response)

    @_run(connObj).then(success, error)

  # Sends response to the remote user
  _sendResponse: (id, destinationUserAddress, status, response) =>
    connObj = {
      id: id,
      type: 'response',
      sourceUserAddress: darwin.Core.getUserAddress(),
      destinationUserAddress: destinationUserAddress,
      status: status,
      response: response,
      signature: null
    }

    if darwin.Config.CRYPTOGRAPHY.ENABLED
      darwin.CryptographyManager.sign(response, ((signature) =>

        connObj.signature = signature
        @_peerConnection.send(JSON.stringify(connObj))

      ), (() =>
        console.error('Failed to sign outbound data')
      ))
    else
      @_peerConnection.send(JSON.stringify(connObj))

  # Process request and runs proper method
  _run: (connObj) =>
    new Promise((success, error) =>
      try
        sourceData = connObj.sourceData
        destinationData = connObj.destinationData

        if navigator.onLine
          if darwin.Core.getUserStatus()
            if @getStatus()is darwin.Config.CONNECTION.STATUS.CONNECTED

              dispatcherName = destinationData.dispatcherName
              methodName = destinationData.methodName

              # Ensures that the caller calls the proper method
              if dispatcherName of @_dispatchers
                f = @_dispatchers[dispatcherName][methodName]
                f(sourceData, destinationData, success, error)
              else
                error(new darwin.Error('Cannot find the specified method.'))

            else
              error(new darwin.Error('The connection is not properly established.'))
          else
            error(new darwin.Error('The user is not signed in.'))
        else
          error(new darwin.Error('Cannot perform remote call without an access to the Internet.'))
      catch err
        if 'message' of err
          error(new darwin.Error(err.message))
        else
          error(new darwin.Error('Failed to process request. Unknown error.'))
    )

  # Initialises remote connection, fetches remote peer ID from remote database
  initialise: =>
    new Promise((success, error) =>
      peersDb = new darwin.Database(@_remoteUserAddress, null, '__darwin', 'peers')
      peersDb.get(darwin.Core.getAppId()).then(((doc) =>
        if 'recent-peer-id' of doc and doc['recent-peer-id'] isnt null
          @_peerId = doc['recent-peer-id']
          success(new darwin.Success(true, { peerId: @_peerId }))
        else
          error(new darwin.Error('Failed to get recent remote peer ID.'))
      ), error)
    )

  # Updates peer ID and peer connection object within current remote connection
  update: (peerConnection) =>
    new Promise((success, error) =>

      updateCallback = () =>
        @_peerId = peerConnection.peer
        @_peerConnection = peerConnection

        # Timeout function which is called if the event handlers do not respond within certain amount of time
        timeout = () =>
          console.error('Failed to connect to ' + @_remoteUserAddress + ' (connection timeout).')
          error(new darwin.Error('Connection timeout.', null, @))

        updateConnectionTimeout = setTimeout(timeout, darwin.Config.CONNECTION.TIMEOUT)

        @_updateEventHandlers().then((() =>
          clearInterval(updateConnectionTimeout)
          success(new darwin.Success(true, { peerId: @_peerId }))
        ), ((err) =>
          clearInterval(updateConnectionTimeout)
          error(new darwin.Error('Failed to update remote connection.', err.message))
        ))

      @close(true).then(updateCallback, updateCallback)

    )

  # Gets live connection status
  getStatus: =>
    if @_peerConnection is null
      darwin.Config.CONNECTION.STATUS.NEW
    else if @_peerConnection.open
      darwin.Config.CONNECTION.STATUS.CONNECTED
    else
      darwin.Config.CONNECTION.STATUS.DISCONNECTED

  # Simple gets the user address of current remote connection
  getRemoteUserAddress: =>
    @_remoteUserAddress

  # Simple gets the peer ID of current remote connection
  getPeerId: =>
    @_peerId

  # Tries to open the remote connection
  open: (silent=false) =>
    new Promise((success, error) =>
      if @_peerId isnt null

        openConnection = () =>
          peer = darwin.Server.getPeer()
          metadata = {
            userAddress: darwin.Core.getUserAddress()
          }

          @_peerConnection = peer.connect(@_peerId, { metadata: metadata })

          # Timeout function which is called if the event handlers do not respond within certain amount of time
          timeout = () =>
            if not silent
              console.error('Failed to connect to ' + @_remoteUserAddress + ' (connection timeout).')
            error(new darwin.Error('Connection timeout.', null, @))

          openConnectionTimeout = setTimeout(timeout, darwin.Config.CONNECTION.TIMEOUT)

          @_updateEventHandlers().then((() =>
            clearInterval(openConnectionTimeout)
            success(new darwin.Success(@, { peerId: @_peerId }))
          ), ((err) =>
            clearInterval(openConnectionTimeout)
            error(new darwin.Error('Failed to open remote connection.', err.message, @))
          ))

        # If peer connection already exists then closes it
        if @getStatus() is darwin.Config.CONNECTION.STATUS.CONNECTED
          @close().then(openConnection, openConnection)
        else
          openConnection()

      else
        error(new darwin.Error('The remote connection is not properly initialised.', null, @))
    )

  # Tries to close the remote connection
  close: (silent=false) =>
    new Promise((success, error) =>
      if @_peerConnection isnt null

        # Timeout function which is called if the close event handler do not respond within certain amount of time
        timeout = () =>
          if not silent
            console.error('Failed to close the connection with ' + @_remoteUserAddress + ' (connection timeout).')
          error(new darwin.Error('Connection timeout.'))

        closeConnectionTimeout = setTimeout(timeout, darwin.Config.CONNECTION.TIMEOUT)

        # Close event handler
        @_peerConnection.on('close', () =>
          clearInterval(closeConnectionTimeout)
          console.log('The connection has been closed.')
          @_detachEventHandlers()
          success(new darwin.Success(true, { peerId: @_peerId }))
        )

        @_peerConnection.close()

      else
        error(new darwin.Error('The remote connection is closed or null.'))
    )

  # Sends request to the remote user
  sendRequest: (dispatcherName, methodName, parameters) =>
    new Promise((success, error) =>
      if @_peerConnection isnt null and @getStatus() is darwin.Config.CONNECTION.STATUS.CONNECTED

        connObj = {
          id: darwin.Utils.uuid(),
          type: 'request',
          sourceData: {
            userAddress: darwin.Core.getUserAddress(),
            appId: darwin.Core.getAppId()
          },
          destinationData: {
            userAddress: @_remoteUserAddress,
            dispatcherName: dispatcherName,
            methodName: methodName,
            parameters: parameters
          },
          serialisedParameters: JSON.stringify(parameters),
          signature: null
        }

        @_callbacks[connObj.id] = {
          success: success,
          error: error
        }

        if darwin.Config.CRYPTOGRAPHY.ENABLED
          serialisedData = JSON.stringify(parameters)
          darwin.CryptographyManager.sign(serialisedData, ((signature) =>

            connObj.signature = signature
            connObj.serialisedData = serialisedData
            @_peerConnection.send(JSON.stringify(connObj))

          ), (() =>
            console.error('Failed to sign outbound data')
          ))
        else
          @_peerConnection.send(JSON.stringify(connObj))

      else
        error(new darwin.Error('The remote connection is closed or null.'))
    )
