// Generated by CoffeeScript 1.8.0
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  darwin.RemoteConnection = (function() {
    function RemoteConnection(remoteUserAddress) {
      this.sendRequest = __bind(this.sendRequest, this);
      this.close = __bind(this.close, this);
      this.open = __bind(this.open, this);
      this.getPeerId = __bind(this.getPeerId, this);
      this.getRemoteUserAddress = __bind(this.getRemoteUserAddress, this);
      this.getStatus = __bind(this.getStatus, this);
      this.update = __bind(this.update, this);
      this.initialise = __bind(this.initialise, this);
      this._run = __bind(this._run, this);
      this._sendResponse = __bind(this._sendResponse, this);
      this._receiveRequest = __bind(this._receiveRequest, this);
      this._receiveResponse = __bind(this._receiveResponse, this);
      this._receive = __bind(this._receive, this);
      this._detachEventHandlers = __bind(this._detachEventHandlers, this);
      this._updateEventHandlers = __bind(this._updateEventHandlers, this);
      this._remoteUserAddress = remoteUserAddress;
      this._peerId = null;
      this._peerConnection = null;
      this._callbacks = {};
      this._dispatchers = {
        DatabaseManager: darwin.DatabaseManager,
        LocalManager: darwin.LocalManager,
        RemoteManager: darwin.RemoteManager,
        DistributionManager: darwin.DistributionManager,
        ExplicitGraphManager: darwin.ExplicitGraphManager,
        ImplicitGraphManager: darwin.ImplicitGraphManager
      };
    }

    RemoteConnection.prototype._updateEventHandlers = function() {
      return new Promise((function(_this) {
        return function(success, error) {
          if (_this._peerConnection !== null) {
            _this._detachEventHandlers();
            _this._peerConnection.on('open', function() {
              _this._peerConnection.on('data', _this._receive);
              success(new darwin.Success(true));
              return console.log('Connected to ' + _this._remoteUserAddress + '.');
            });
            return _this._peerConnection.on('error', function(err) {
              return error(new darwin.Error('Failed to connect.'));
            });
          } else {
            return error(new darwin.Error('Failed to attach event handlers to the null remote connection.'));
          }
        };
      })(this));
    };

    RemoteConnection.prototype._detachEventHandlers = function() {
      var name, _results;
      if (this._peerConnection !== null) {
        _results = [];
        for (name in this._peerConnection._events) {
          _results.push(this._peerConnection._events[name] = []);
        }
        return _results;
      }
    };

    RemoteConnection.prototype._receive = function(connObj) {
      var id, passEvent, serialisedParameters, signature, sourceData, sourceUserAddress, type;
      connObj = JSON.parse(connObj);
      id = connObj.id;
      type = connObj.type;
      sourceData = connObj.sourceData;
      signature = connObj.signature;
      serialisedParameters = connObj.serialisedParameters;
      passEvent = (function(_this) {
        return function() {
          if (type === 'request') {
            return _this._receiveRequest(connObj);
          } else if (type === 'response') {
            return _this._receiveResponse(connObj);
          }
        };
      })(this);
      if (type === 'request' && darwin.Config.CRYPTOGRAPHY.ENABLED) {
        sourceUserAddress = sourceData.sourceUserAddress;
        return darwin.CryptographyManager.verify(serialisedParameters, signature, sourceUserAddress, ((function(_this) {
          return function() {
            _this._log('Verified request/response');
            return passEvent();
          };
        })(this)), ((function(_this) {
          return function() {
            var response;
            response = new darwin.Error('Failed to verify request/response.');
            return _this._sendResponse(id, sourceUserAddress, false, response);
          };
        })(this)));
      } else {
        return passEvent();
      }
    };

    RemoteConnection.prototype._receiveResponse = function(connObj) {
      var callback, id, response, status;
      id = connObj.id;
      status = connObj.status;
      response = connObj.response;
      if (id in this._callbacks) {
        if (status) {
          callback = this._callbacks[id]['success'];
        } else {
          callback = this._callbacks[id]['error'];
        }
        if (typeof callback === 'function') {
          callback(response);
        }
        return delete this._callbacks[id];
      } else {
        return console.error('Failed to find callback.');
      }
    };

    RemoteConnection.prototype._receiveRequest = function(connObj) {
      var error, id, sourceUserAddress, success;
      id = connObj.id;
      sourceUserAddress = connObj.sourceUserAddress;
      success = (function(_this) {
        return function(response) {
          if (response && 'getObject' in response) {
            response = response.getObject();
          }
          return _this._sendResponse(id, sourceUserAddress, true, response);
        };
      })(this);
      error = (function(_this) {
        return function(response) {
          return _this._sendResponse(id, sourceUserAddress, false, response);
        };
      })(this);
      return this._run(connObj).then(success, error);
    };

    RemoteConnection.prototype._sendResponse = function(id, destinationUserAddress, status, response) {
      var connObj;
      connObj = {
        id: id,
        type: 'response',
        sourceUserAddress: darwin.Core.getUserAddress(),
        destinationUserAddress: destinationUserAddress,
        status: status,
        response: response,
        signature: null
      };
      if (darwin.Config.CRYPTOGRAPHY.ENABLED) {
        return darwin.CryptographyManager.sign(response, ((function(_this) {
          return function(signature) {
            connObj.signature = signature;
            return _this._peerConnection.send(JSON.stringify(connObj));
          };
        })(this)), ((function(_this) {
          return function() {
            return console.error('Failed to sign outbound data');
          };
        })(this)));
      } else {
        return this._peerConnection.send(JSON.stringify(connObj));
      }
    };

    RemoteConnection.prototype._run = function(connObj) {
      return new Promise((function(_this) {
        return function(success, error) {
          var destinationData, dispatcherName, err, f, methodName, sourceData;
          try {
            sourceData = connObj.sourceData;
            destinationData = connObj.destinationData;
            if (navigator.onLine) {
              if (darwin.Core.getUserStatus()) {
                if (_this.getStatus() === darwin.Config.CONNECTION.STATUS.CONNECTED) {
                  dispatcherName = destinationData.dispatcherName;
                  methodName = destinationData.methodName;
                  if (dispatcherName in _this._dispatchers) {
                    f = _this._dispatchers[dispatcherName][methodName];
                    return f(sourceData, destinationData, success, error);
                  } else {
                    return error(new darwin.Error('Cannot find the specified method.'));
                  }
                } else {
                  return error(new darwin.Error('The connection is not properly established.'));
                }
              } else {
                return error(new darwin.Error('The user is not signed in.'));
              }
            } else {
              return error(new darwin.Error('Cannot perform remote call without an access to the Internet.'));
            }
          } catch (_error) {
            err = _error;
            if ('message' in err) {
              return error(new darwin.Error(err.message));
            } else {
              return error(new darwin.Error('Failed to process request. Unknown error.'));
            }
          }
        };
      })(this));
    };

    RemoteConnection.prototype.initialise = function() {
      return new Promise((function(_this) {
        return function(success, error) {
          var peersDb;
          peersDb = new darwin.Database(_this._remoteUserAddress, null, '__darwin', 'peers');
          return peersDb.get(darwin.Core.getAppId()).then((function(doc) {
            if ('recent-peer-id' in doc && doc['recent-peer-id'] !== null) {
              _this._peerId = doc['recent-peer-id'];
              return success(new darwin.Success(true, {
                peerId: _this._peerId
              }));
            } else {
              return error(new darwin.Error('Failed to get recent remote peer ID.'));
            }
          }), error);
        };
      })(this));
    };

    RemoteConnection.prototype.update = function(peerConnection) {
      return new Promise((function(_this) {
        return function(success, error) {
          var updateCallback;
          updateCallback = function() {
            var timeout, updateConnectionTimeout;
            _this._peerId = peerConnection.peer;
            _this._peerConnection = peerConnection;
            timeout = function() {
              console.error('Failed to connect to ' + _this._remoteUserAddress + ' (connection timeout).');
              return error(new darwin.Error('Connection timeout.', null, _this));
            };
            updateConnectionTimeout = setTimeout(timeout, darwin.Config.CONNECTION.TIMEOUT);
            return _this._updateEventHandlers().then((function() {
              clearInterval(updateConnectionTimeout);
              return success(new darwin.Success(true, {
                peerId: _this._peerId
              }));
            }), (function(err) {
              clearInterval(updateConnectionTimeout);
              return error(new darwin.Error('Failed to update remote connection.', err.message));
            }));
          };
          return _this.close(true).then(updateCallback, updateCallback);
        };
      })(this));
    };

    RemoteConnection.prototype.getStatus = function() {
      if (this._peerConnection === null) {
        return darwin.Config.CONNECTION.STATUS.NEW;
      } else if (this._peerConnection.open) {
        return darwin.Config.CONNECTION.STATUS.CONNECTED;
      } else {
        return darwin.Config.CONNECTION.STATUS.DISCONNECTED;
      }
    };

    RemoteConnection.prototype.getRemoteUserAddress = function() {
      return this._remoteUserAddress;
    };

    RemoteConnection.prototype.getPeerId = function() {
      return this._peerId;
    };

    RemoteConnection.prototype.open = function(silent) {
      if (silent == null) {
        silent = false;
      }
      return new Promise((function(_this) {
        return function(success, error) {
          var openConnection;
          if (_this._peerId !== null) {
            openConnection = function() {
              var metadata, openConnectionTimeout, peer, timeout;
              peer = darwin.Server.getPeer();
              metadata = {
                userAddress: darwin.Core.getUserAddress()
              };
              _this._peerConnection = peer.connect(_this._peerId, {
                metadata: metadata
              });
              timeout = function() {
                if (!silent) {
                  console.error('Failed to connect to ' + _this._remoteUserAddress + ' (connection timeout).');
                }
                return error(new darwin.Error('Connection timeout.', null, _this));
              };
              openConnectionTimeout = setTimeout(timeout, darwin.Config.CONNECTION.TIMEOUT);
              return _this._updateEventHandlers().then((function() {
                clearInterval(openConnectionTimeout);
                return success(new darwin.Success(_this, {
                  peerId: _this._peerId
                }));
              }), (function(err) {
                clearInterval(openConnectionTimeout);
                return error(new darwin.Error('Failed to open remote connection.', err.message, _this));
              }));
            };
            if (_this.getStatus() === darwin.Config.CONNECTION.STATUS.CONNECTED) {
              return _this.close().then(openConnection, openConnection);
            } else {
              return openConnection();
            }
          } else {
            return error(new darwin.Error('The remote connection is not properly initialised.', null, _this));
          }
        };
      })(this));
    };

    RemoteConnection.prototype.close = function(silent) {
      if (silent == null) {
        silent = false;
      }
      return new Promise((function(_this) {
        return function(success, error) {
          var closeConnectionTimeout, timeout;
          if (_this._peerConnection !== null) {
            timeout = function() {
              if (!silent) {
                console.error('Failed to close the connection with ' + _this._remoteUserAddress + ' (connection timeout).');
              }
              return error(new darwin.Error('Connection timeout.'));
            };
            closeConnectionTimeout = setTimeout(timeout, darwin.Config.CONNECTION.TIMEOUT);
            _this._peerConnection.on('close', function() {
              clearInterval(closeConnectionTimeout);
              console.log('The connection has been closed.');
              _this._detachEventHandlers();
              return success(new darwin.Success(true, {
                peerId: _this._peerId
              }));
            });
            return _this._peerConnection.close();
          } else {
            return error(new darwin.Error('The remote connection is closed or null.'));
          }
        };
      })(this));
    };

    RemoteConnection.prototype.sendRequest = function(dispatcherName, methodName, parameters) {
      return new Promise((function(_this) {
        return function(success, error) {
          var connObj, serialisedData;
          if (_this._peerConnection !== null && _this.getStatus() === darwin.Config.CONNECTION.STATUS.CONNECTED) {
            connObj = {
              id: darwin.Utils.uuid(),
              type: 'request',
              sourceData: {
                userAddress: darwin.Core.getUserAddress(),
                appId: darwin.Core.getAppId()
              },
              destinationData: {
                userAddress: _this._remoteUserAddress,
                dispatcherName: dispatcherName,
                methodName: methodName,
                parameters: parameters
              },
              serialisedParameters: JSON.stringify(parameters),
              signature: null
            };
            _this._callbacks[connObj.id] = {
              success: success,
              error: error
            };
            if (darwin.Config.CRYPTOGRAPHY.ENABLED) {
              serialisedData = JSON.stringify(parameters);
              return darwin.CryptographyManager.sign(serialisedData, (function(signature) {
                connObj.signature = signature;
                connObj.serialisedData = serialisedData;
                return _this._peerConnection.send(JSON.stringify(connObj));
              }), (function() {
                return console.error('Failed to sign outbound data');
              }));
            } else {
              return _this._peerConnection.send(JSON.stringify(connObj));
            }
          } else {
            return error(new darwin.Error('The remote connection is closed or null.'));
          }
        };
      })(this));
    };

    return RemoteConnection;

  })();

}).call(this);

//# sourceMappingURL=remote-connection.js.map
