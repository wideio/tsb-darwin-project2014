class RemoteManager extends darwin.EventProvider
  constructor: ->
    super

    # Remote connections by remote user address
    @_connectionsByUserAddress = {}

    # Remote connections by peer ID
    @_connectionsByPeerId = {}

  # Initialises remote manager
  initialise: =>
    new Promise((success, error) =>
      info = {
        connected: 0,
        failed: 0
      }

      usersDb = darwin.DatabaseManager.getCoreDbs().getController('applications-users')
      usersDb.all().then(((response) =>
        step = () =>
          if info.connected + info.failed is response.length
            @_triggerEvent('initialise-success')
            success(new darwin.Success(true))

        if navigator.onLine
          if response.length isnt 0
            for item in response
              remoteUserAddress = item._id

              darwin.RemoteManager.initialiseRemoteConnection(remoteUserAddress).then((() =>
                ++info.connected
                step()
              ), (() =>
                ++info.failed
                step()
              ))
          else
            @_triggerEvent('initialise-success')
            success(new darwin.Success(true))
        else
          @_triggerEvent('initialise-success')
          success(new darwin.Success(true))
      ), (() =>
        @_triggerEvent('initialise-error')
        error(new darwin.Error('Failed to fetch established connections.'))
      ))
    )

  # Gets method names which are available outside the class
  getPublicMethods: =>
    forbiddenMethods = ['initialise', 'getPublicMethods', 'getRemoteUserAddresses', 'getConnectionByUserAddress',
                        'getConnectionByPeerId', 'updateConnectionByUserAddress', 'deleteConnectionByUserAddress',
                        'purgeConnections', 'initialiseRemoteConnection', 'setRemoteUserPermissions']
    methods = []
    for methodName of @
      if methodName[0] isnt '_' and methodName not in forbiddenMethods
        methods.push(methodName)
    methods

  # Gets all remote user addresses which belongs to the current user's friends
  getRemoteUserAddresses: =>
    Object.keys(@_connectionsByUserAddress)

  # Gets remote connection by given user address
  getConnectionByUserAddress: (remoteUserAddress) =>
    if remoteUserAddress of @_connectionsByUserAddress then @_connectionsByUserAddress[remoteUserAddress] else null

  # Gets remote connection by given peer ID
  getConnectionByPeerId: (peerId) =>
    if peerId of @_connectionsByPeerId then @_connectionsByPeerId[peerId] else null

  # Updates remote connection of given user address
  updateConnectionByUserAddress: (remoteUserAddress, remoteConnection) =>
    new Promise((success, error) =>
      if remoteConnection.getPeerId() isnt null
        if remoteUserAddress not of @_connectionsByUserAddress
          @_connectionsByUserAddress[remoteUserAddress] = remoteConnection
          @_connectionsByPeerId[remoteConnection.getPeerId()] = remoteConnection
          success(new darwin.Success(true))
        else
          updateCallback = () =>
            @_connectionsByUserAddress[remoteUserAddress] = remoteConnection
            @_connectionsByPeerId[remoteConnection.getPeerId()] = remoteConnection
            success(new darwin.Success(true))

          # Removes previous connection
          darwin.RemoteManager.deleteConnectionByUserAddress(remoteUserAddress, true).then(updateCallback, updateCallback)
      else
        error(new darwin.Error('Failed to update remote connection.'))
    )

  # Deletes remote connection of given user address and closes the corresponding peer connection
  deleteConnectionByUserAddress: (remoteUserAddress, silentClose=false) =>
    new Promise((success, error) =>
      if remoteUserAddress of @_connectionsByUserAddress
        remoteConnection = @_connectionsByUserAddress[remoteUserAddress]
        remoteConnection.close(silentClose).then((() =>

          if remoteConnection.getPeerId() of @_connectionsByPeerId
            delete @_connectionsByPeerId[remoteConnection.getPeerId()]
          delete @_connectionsByUserAddress[remoteUserAddress]
          success(new darwin.Success(true))

        ), error)
      else
        error(new darwin.Error('Specified remote connection does not exist.'))
    )

  # Purges all the remote connections and closes the corresponding peer connections
  purgeConnections: =>
    new Promise((success, error) =>
      remoteUserAddresses = darwin.RemoteManager.getRemoteUserAddresses()

      info = {
        deleted: 0,
        failed: 0
      }

      step = () =>
        if info.deleted + info.failed is remoteUserAddresses.length
          success(new darwin.Success(true, info))

      for remoteUserAddress in remoteUserAddresses
        darwin.RemoteManager.deleteConnectionByUserAddress(remoteUserAddress).then((() =>
          ++info.deleted
          step()
        ), (() =>
          ++info.failed
          step()
        ))
    )

  # Initialises and opens remote connection of given friend user address
  initialiseRemoteConnection: (remoteUserAddress) =>
    new Promise((success, error) =>
      if remoteUserAddress not of @_connectionsByUserAddress

        remoteConnection = new darwin.RemoteConnection(remoteUserAddress)
        remoteConnection.initialise().then((() =>

          darwin.RemoteManager.updateConnectionByUserAddress(remoteUserAddress, remoteConnection).then((() =>
            remoteConnection.open().then((() =>
              success(new darwin.Success(remoteConnection))
            ), error)
          ), ((err) =>
            error(new darwin.Error('Failed to initialise remote connection.', err.message))
          ))

        ), ((err) =>
          darwin.RemoteManager.updateConnectionByUserAddress(remoteUserAddress, remoteConnection).then((() =>
            error(new darwin.Error('Failed to initialise remote connection.', err.message))
          ), ((err) =>
            error(new darwin.Error('Failed to initialise remote connection.', err.message))
          ))
        ))

      else
        success(new darwin.Success(@_connectionsByUserAddress[remoteUserAddress]))
        #error(new darwin.Error('Failed to initialise remote connection.\nThe connection of user address ' + remoteUserAddress + ' already exists.'))
    )

  # Initiates a handshake with remote user
  performHandshake: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress() and destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      remoteUserAddress = parameters[0]
      wantedPermissions = parameters[1]

      if @getConnectionByUserAddress(remoteUserAddress) is null
        darwin.CryptographyManager.getPublicKey().then(((publicKey) =>
          parameters = [publicKey, wantedPermissions]

          # Creates proper remote connection
          darwin.RemoteManager.initialiseRemoteConnection(remoteUserAddress).then(((response) =>
            dispatcherName = darwin.Config.DISPATCHER.REMOTE_MANAGER
            methodName = 'receiveHandshake'

            # Requests remote user to receive request
            remoteConnection = response.result
            remoteConnection.sendRequest(dispatcherName, methodName, parameters).then(((response) =>
              console.log('Performed handshake')

              # Saves received remote user's public key
              remoteUserKey = response.result
              darwin.CryptographyManager.setRemoteUserKey(remoteUserAddress, remoteUserKey).then((() =>
                darwin.PermissionManager.updateRemoteUserPermissions(remoteUserAddress, wantedPermissions)
                success(new darwin.Success(true))
              ), (() =>
                callback = () =>
                  error(new darwin.Error('Failed to save remote user in the database.'))

                @deleteConnectionByUserAddress(remoteUserAddress).then(callback, callback)
              ))
            ), error)
          ), ((err) =>
            error(err)
          ))

        ), ((err) =>
          error(err)
        ))
      else
        success(new darwin.Success(true))
    else
      error(new darwin.Error('This operation is forbidden.'))

  # Receives handshake from remote user
  receiveHandshake: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      sourceUserAddress = sourceData.userAddress
      parameters = destinationData.parameters

      remotePublicKey = parameters[0]
      wantedPermissions = parameters[1]

      if @getConnectionByUserAddress(sourceUserAddress) is null
        methodName = 'receiveHandshake'
        parameters = [sourceUserAddress, wantedPermissions]

        # Requests client to show handshake popup
        darwin.LocalConnection.sendRequest(methodName, parameters, (() =>
          darwin.PermissionManager.updateRemoteUserPermissions(sourceUserAddress, wantedPermissions)
          darwin.CryptographyManager.setRemoteUserKey(sourceUserAddress, remotePublicKey).then((() =>
            darwin.CryptographyManager.getPublicKey().then(((publicKey) =>
              success(new darwin.Success(publicKey))
            ), (() =>
              error(new darwin.Error('Failed to get public key of ' + darwin.Core.getUserAddress() + '.'))
            ))
          ), (() =>
            error(new darwin.Error('Failed to save remote user in the database.'))
          ))
        ), (() =>
          @deleteConnectionByUserAddress(sourceUserAddress)
          error(new darwin.Error('User declined the connection.'))
        ))
      else
        # If the connection exists, simply sends back public key
        darwin.CryptographyManager.getPublicKey().then(((publicKey) =>
          success(new darwin.Success(publicKey))
        ), (() =>
          error(new darwin.Error('Failed to get public key of ' + darwin.Core.getUserAddress() + '.'))
        ))
    else
      error(new darwin.Error('This operation is forbidden.'))

  # Gets some information about currently opened connections
  getAllRemoteConnectionsInfo: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      remoteConnectionsInfo = {}
      for address, remoteConnection of @_connectionsByUserAddress
        remoteConnectionsInfo[address] = {
          status: remoteConnection.getStatus()
        }

      success(new darwin.Success(remoteConnectionsInfo))
    else
      error(new darwin.Error('This operation is forbidden.'))

  # Gets some information about given remote connection
  getRemoteConnectionInfo: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      remoteUserAddress = parameters[0]

      remoteConnection = darwin.RemoteManager.getConnectionByUserAddress(remoteUserAddress)
      if remoteConnection isnt null
        info = {
          status: remoteConnection.getStatus()
        }
        success(new darwin.Success(info))
      else
        error(new darwin.Error('Specified remote connection does not exist.'))
    else
      error(new darwin.Error('This operation is forbidden.'))

  getRemoteUserPermissions: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      remoteUserAddress = parameters[0]

      permissionsDb = darwin.DatabaseManager.getAppHiddenDbs().getController('__permissions')
      permissionsDb.get(remoteUserAddress).then(((doc) =>
        if 'permissions' of doc
          success(new darwin.Success(doc.permissions))
        else
          error(new darwin.Error('There is no array with permissions in the database.'))
      ), error)
    else
      @callRemoteMethod(sourceData, destinationData, success, error)

  requestPermissions: (sourceData, destinationData, success, error) =>
    if destinationData.userAddress is darwin.Core.getUserAddress()
      sourceUserAddress = sourceData.userAddress
      parameters = destinationData.parameters

      wantedPermissions = parameters[0]

      methodName = 'requestPermissions'
      parameters = [sourceUserAddress, wantedPermissions]
      darwin.LocalConnection.sendRequest(methodName, parameters, (() =>
        darwin.PermissionManager.updateRemoteUserPermissions(sourceUserAddress, wantedPermissions).then((() =>
          success(new darwin.Success(true))
        ), error)
      ), (() =>
        error(new darwin.Error('User declined to set requested permissions.'))
      ))
    else
      @callRemoteMethod(sourceData, destinationData, success, error)

  grantPermissions: (sourceData, destinationData, success, error) =>
    if sourceData.userAddress is darwin.Core.getUserAddress()
      parameters = destinationData.parameters

      remoteUserAddress = parameters[0]
      permissions = parameters[1]

      darwin.PermissionManager.updateRemoteUserPermissions(remoteUserAddress, permissions).then((() =>
        success(new darwin.Success(true))
      ), error)
    else
      error(new darwin.Error('This operation is forbidden.'))

  # Calls a method on remote computer
  callRemoteMethod: (sourceData, destinationData, success, error) =>
    remoteUserAddress = destinationData.userAddress
    dispatcherName = destinationData.dispatcherName
    methodName = destinationData.methodName
    parameters = destinationData.parameters

    remoteConnection = darwin.RemoteManager.getConnectionByUserAddress(remoteUserAddress)
    if remoteConnection isnt null
      remoteConnection.sendRequest(dispatcherName, methodName, parameters).then(success, error)
    else
      error(new darwin.Error('Specified remote connection does not exist.'))

darwin.RemoteManager = new RemoteManager