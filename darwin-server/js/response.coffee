class darwin.Success
  constructor: (result, info={}) ->
    @result = result

    @info = info

  getObject: =>
    {
      result: @result,
      info: @info
    }



class darwin.Error
  constructor: (message, history=null, data=null) ->
    @message = message

    @data = data

    if history isnt null and typeof(history) is 'string' and history.length > 0
      @message += '\n' + history

  getObject: =>
    {
      status: false,
      message: @message,
      data: @data
    }
