// Generated by CoffeeScript 1.8.0
(function() {
  var __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  darwin.Success = (function() {
    function Success(result, info) {
      if (info == null) {
        info = {};
      }
      this.getObject = __bind(this.getObject, this);
      this.result = result;
      this.info = info;
    }

    Success.prototype.getObject = function() {
      return {
        result: this.result,
        info: this.info
      };
    };

    return Success;

  })();

  darwin.Error = (function() {
    function Error(message, history, data) {
      if (history == null) {
        history = null;
      }
      if (data == null) {
        data = null;
      }
      this.getObject = __bind(this.getObject, this);
      this.message = message;
      this.data = data;
      if (history !== null && typeof history === 'string' && history.length > 0) {
        this.message += '\n' + history;
      }
    }

    Error.prototype.getObject = function() {
      return {
        status: false,
        message: this.message,
        data: this.data
      };
    };

    return Error;

  })();

}).call(this);

//# sourceMappingURL=response.js.map
