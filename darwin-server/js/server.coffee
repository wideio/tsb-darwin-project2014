class Server
  constructor: ->
    # ID of the PeerJS peer
    @_peerId = null

    # PeerJS peer object
    @_peer = null

  _handleConnection: (peerConnection) =>
    remoteUserAddress = peerConnection.metadata.userAddress

    remoteConnection = new darwin.RemoteConnection(remoteUserAddress)
    remoteConnection.update(peerConnection).then(() =>
      darwin.RemoteManager.updateConnectionByUserAddress(remoteUserAddress, remoteConnection).then(() =>
        console.log('Received connection from remote user ' + remoteUserAddress + '.')
      )
    )

  start: =>
    new Promise((success, error) =>
      if navigator.onLine
        @_peer = new Peer({
          # brunokam's personal key
          key: 'xd6fwj2vsu4n29'
        })
        @_peer.on('open', (peerId) =>
          darwin.Core.setPeerId(peerId)
          @_peer.on('connection', @_handleConnection)
          success()
        )
      else
        success()

      console.log('Server started.')
    )

  # Gets the PeerJS peer object
  getPeer: =>
    @_peer

darwin.Server = new Server
