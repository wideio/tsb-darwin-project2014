// Generated by CoffeeScript 1.8.0
(function() {
  var Server,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  Server = (function() {
    function Server() {
      this.getPeer = __bind(this.getPeer, this);
      this.start = __bind(this.start, this);
      this._handleConnection = __bind(this._handleConnection, this);
      this._peerId = null;
      this._peer = null;
    }

    Server.prototype._handleConnection = function(peerConnection) {
      var remoteConnection, remoteUserAddress;
      remoteUserAddress = peerConnection.metadata.userAddress;
      remoteConnection = new darwin.RemoteConnection(remoteUserAddress);
      return remoteConnection.update(peerConnection).then((function(_this) {
        return function() {
          return darwin.RemoteManager.updateConnectionByUserAddress(remoteUserAddress, remoteConnection).then(function() {
            return console.log('Received connection from remote user ' + remoteUserAddress + '.');
          });
        };
      })(this));
    };

    Server.prototype.start = function() {
      return new Promise((function(_this) {
        return function(success, error) {
          if (navigator.onLine) {
            _this._peer = new Peer({
              key: 'xd6fwj2vsu4n29'
            });
            _this._peer.on('open', function(peerId) {
              darwin.Core.setPeerId(peerId);
              _this._peer.on('connection', _this._handleConnection);
              return success();
            });
          } else {
            success();
          }
          return console.log('Server started.');
        };
      })(this));
    };

    Server.prototype.getPeer = function() {
      return this._peer;
    };

    return Server;

  })();

  darwin.Server = new Server;

}).call(this);

//# sourceMappingURL=server.js.map
