class Utils
  constructor: ->
    null

  _log: (message) =>
    console.log('utils @', message)

  stringToBuffer: (str) =>
    chars = []
    for i in [0..str.length - 1] by 1
      chars.push(str.charCodeAt(i))
    new Uint8Array(chars)

  bufferToString: (buf) =>
    String.fromCharCode.apply(null, new Uint8Array(buf))

  bufferToHexString: (buf) =>
    str = ''
    for item in new Uint8Array(buf)
      str += item.toString(16)
    str

  intsToBuffer: (ints) =>
    abv = new Uint8Array(ints.length)
    for i in [0..ints.length - 1] by 1
      abv[i] = ints[i]
    abv

  bufferToInts: (abv) =>
    buffer = new Uint8Array(abv.buffer, abv.byteOffset, abv.byteLength)
    ints = []
    for i in [0..buffer.length - 1] by 1
      num = buffer[i].toString(10)
      ints.push(parseInt(num))
    ints

  intsToHexString: (ints) =>
    hexString = ''
    for item in ints
      if item >= 16
        hexString += item.toString(16)
      else
        hexString += '0' + item.toString(16)
    hexString

  # Extracts address from username@server-address
  extractRemoteAddress: (address) =>
    arr = address.split('@')
    value = {
      username: arr[0],
      address: arr[1]
    }
    value

  uuid: =>
    d = new Date().getTime()
    uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) =>
      r = (d + Math.random() * 16) % 16 | 0
      d = Math.floor(d/16)
      (if c is 'x' then r else (r & 0x3 | 0x8)).toString(16)
    )
    uuid

darwin.Utils = new Utils
