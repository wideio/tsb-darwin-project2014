class Tests
  constructor: ->
    @_userAddress = 'brunokam@mbk.iriscouch.com'

    @_password = 'cheeseplease'

    @_appId = 'd147c9e5d875'

    @_dbName = 'general'

    @_client = null

  _log: () =>
    args = ['tests @']
    for item in arguments
      args.push(item)
    console.log.apply(console, args)

  _uuid: =>
    d = new Date().getTime()
    uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) =>
      r = (d + Math.random() * 16) % 16 | 0
      d = Math.floor(d/16)
      return (if c is 'x' then r else (r & 0x7 | 0x8)).toString(16)
    )
    uuid

  _cleanDatabase: =>
    return new Promise((success, error) =>
      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      db.all().then(((result) =>
        for item in result
          writeBatch.addDelete(item._id)
        writeBatch.submit().then((() =>
          @_log('Cleaned the database.')
          success()
        ), error)
      ), error)
    )

  initialise: =>
    return new Promise((success, error) =>
      @_client = new darwin.Client(@_appId)
      @_client.start().then((() =>
        if not @_client.getUserStatus()
          @_client.signIn(@_userAddress, @_password).then(success, error)
        else
          success()
      ), error)
    )

  run: =>
    QUnit.module('method all()', {
      setup: () =>
        QUnit.stop()
        @_cleanDatabase().then(() =>
          QUnit.start()
        )
    })
    QUnit.asyncTest('local | no options | order not important', (assert) =>
      result = null
      error = null
      samplesCount = 5
      sampleData = {}

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object' and 'length' of result, 'The result has length.')
        assert.ok(result.length is samplesCount, 'The result has correct number of documents.')

        for i in [0..result.length - 1]
          assert.ok('_id' of result[i], 'Document ' + i + ' has ID.')
          assert.ok('data' of result[i], 'Document ' + i + ' has data.')
          assert.ok(result[i].data is sampleData[result[i]._id].data, 'Document ' + i + ' has correct data.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Cleans database, submits sample data and test all()
      writeBatch.submit().then((() =>
        db.all().then(((res) =>
          result = res
          assertions()
        ), ((err) =>
          error = err
          assertions()
        ))
      ), (() =>
        assertions()
      ))
    )

    QUnit.asyncTest('local | options: limit | order not important', (assert) =>
      result = null
      error = null
      samplesCount = 10
      sampleData = {}
      limit = 7

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object' and 'length' of result, 'The result has length.')
        assert.ok(result.length is limit, 'The result has correct number of documents.')

        for i in [0..result.length - 1]
          assert.ok('_id' of result[i], 'Document ' + i + ' has ID.')
          assert.ok('data' of result[i], 'Document ' + i + ' has data.')
          assert.ok(result[i].data is sampleData[result[i]._id].data, 'Document ' + i + ' has correct data.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Cleans database, submits sample data and test all()
      writeBatch.submit().then((() =>
        options = { limit: limit }
        db.all(options).then(((res) =>
          result = res
          assertions()
        ), ((err) =>
          error = err
          assertions()
        ))
      ), ((err) =>
        error = err
        assertions()
      ))
    )

    QUnit.asyncTest('local | options: skip | order important', (assert) =>
      result = null
      error = null
      samplesCount = 10
      sampleData = []
      skip = 3

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object' and 'length' of result, 'The result has length.')
        assert.ok(result.length is (sampleData.length - skip), 'The result has correct number of documents.')

        for i in [0..result.length - 1]
          assert.ok('_id' of result[i], 'Document ' + i + ' has ID.')
          assert.ok('data' of result[i], 'Document ' + i + ' has data.')
          assert.ok(result[i]._id is sampleData[i + skip]._id, 'Document ' + i + ' has correct ID.')
          assert.ok(result[i].data is sampleData[i + skip].data, 'Document ' + i + ' has correct data.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      keys = []
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { '_id': id, 'data': @_uuid() }

        sampleData.push(doc)
        keys.push(id)
        writeBatch.addSet(id, doc)

      # Cleans database, submits sample data and test all()
      writeBatch.submit().then((() =>
        options = { keys: keys, skip: skip }
        db.all(options).then(((res) =>
          result = res
          assertions()
        ), ((err) =>
          error = err
          assertions()
        ))
      ), ((err) =>
        error = err
        assertions()
      ))
    )

    QUnit.asyncTest('local | options: limit, skip | order important', (assert) =>
      result = null
      error = null
      samplesCount = 10
      sampleData = []
      limit = 5
      skip = 3

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object' and 'length' of result, 'The result has length.')
        assert.ok(result.length is limit, 'The result has correct number of documents.')

        for i in [0..result.length - 1]
          assert.ok('_id' of result[i], 'Document ' + i + ' has ID.')
          assert.ok('data' of result[i], 'Document ' + i + ' has data.')
          assert.ok(result[i]._id is sampleData[i + skip]._id, 'Document ' + i + ' has correct ID.')
          assert.ok(result[i].data is sampleData[i + skip].data, 'Document ' + i + ' has correct data.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      keys = []
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { '_id': id, 'data': @_uuid() }

        sampleData.push(doc)
        keys.push(id)
        writeBatch.addSet(id, doc)

      # Cleans database, submits sample data and test all()
      writeBatch.submit().then((() =>
        options = { keys: keys, limit: limit, skip: skip }
        db.all(options).then(((res) =>
          result = res
          assertions()
        ), ((err) =>
          error = err
          assertions()
        ))
      ), ((err) =>
        error = err
        assertions()
      ))
    )

    QUnit.module('method get()')
    QUnit.asyncTest('local | existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object', 'The result is an object.')

        assert.ok('_id' of result, 'The document has ID.')
        assert.ok('data' of result, 'The document has data.')
        assert.ok(result.data is sampleData[result._id].data, 'The document has correct data.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = Object.keys(sampleData)[1]
        db.get(key).then(((res) =>
          result = res
          assertions()
        ), ((err) =>
          error = err
          assertions()
        ))
      ), (() =>
        assertions()
      ))
    )

    QUnit.asyncTest('local | non-existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result is null, 'The result is null.')
        assert.ok(error isnt null, 'The error is not null.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = @_uuid()
        db.get(key).then(((res) =>
          result = res
          assertions()
        ), ((err) =>
          error = err
          assertions()
        ))
      ), (() =>
        assertions()
      ))
    )

    QUnit.module('method set()')
    QUnit.asyncTest('local | existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}
      key = null
      value = null

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object', 'The result is an object.')

        assert.ok('_id' of result, 'The document has ID.')
        assert.ok('data' of result, 'The document has data.')
        assert.ok(result.data is value.data, 'The document has correct data.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = Object.keys(sampleData)[1]
        value = { 'data': @_uuid() }
        db.set(key, value).then((() =>
          db.get(key).then(((res) =>
            result = res
            assertions()
          ), ((err) =>
            error = err
            assertions()
          ))
        ), ((err) =>
          error = err
          assertions()
        ))
      ), ((err) =>
        assertions()
      ))
    )

    QUnit.asyncTest('local | non-existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}
      key = null
      value = null

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object', 'The result is an object.')

        assert.ok('_id' of result, 'The document has ID.')
        assert.ok('data' of result, 'The document has data.')
        assert.ok(result.data is value.data, 'The document has correct data.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = @_uuid()
        value = { 'data': @_uuid() }
        db.set(key, value).then((() =>
          db.get(key).then(((res) =>
            result = res
            assertions()
          ), ((err) =>
            error = err
            assertions()
          ))
        ), ((err) =>
          error = err
          assertions()
        ))
      ), ((err) =>
        assertions()
      ))
    )

    QUnit.asyncTest('local | null key | non-existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}
      key = null
      value = null

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result is null, 'The result is null.')
        assert.ok(error isnt null, 'The error is not null.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = null
        value = { 'data': @_uuid() }
        db.set(key, value).then(((res) =>
          result = res
          assertions()
        ), ((err) =>
          error = err
          assertions()
        ))
      ), ((err) =>
        assertions()
      ))
    )

    QUnit.asyncTest('local | null data | existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}
      key = null
      value = null

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object', 'The result is an object.')

        assert.ok('_id' of result, 'The document has ID.')
        assert.ok('data' of result, 'The document has data.')
        assert.ok(result.data is value.data, 'The document has correct data.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = Object.keys(sampleData)[2]
        value = { 'data': null }
        db.set(key, value).then((() =>
          db.get(key).then(((res) =>
            result = res
            assertions()
          ), ((err) =>
            error = err
            assertions()
          ))
        ), ((err) =>
          error = err
          assertions()
        ))
      ), ((err) =>
        assertions()
      ))
    )

    QUnit.asyncTest('local | null value | non-existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}
      key = null
      value = null

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object', 'The result is an object.')

        assert.ok('_id' of result, 'The document has ID.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = @_uuid()
        value = null
        db.set(key, value).then((() =>
          db.get(key).then(((res) =>
            result = res
            assertions()
          ), ((err) =>
            error = err
            assertions()
          ))
        ), ((err) =>
          error = err
          assertions()
        ))
      ), ((err) =>
        assertions()
      ))
    )

    QUnit.module('method delete()')
    QUnit.asyncTest('local | existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}
      key = null

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = @_uuid()
        value = { 'data': @_uuid() }
        db.set(key, value).then((() =>
          db.delete(key).then(((res) =>
            db.get(key).then((() =>
              error = true
              assertions()
            ), ((err) =>
              result = res
              assertions()
            ))
          ), ((err) =>
            error = err
            assertions()
          ))
        ), (() =>
          assertions()
        ))
      ), (() =>
        assertions()
      ))
    )

    QUnit.asyncTest('local | non-existent document', (assert) =>
      result = null
      error = null
      samplesCount = 3
      sampleData = {}
      key = null

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result is null, 'The result is null.')
        assert.ok(error isnt null, 'The error is not null.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = @_uuid()
        value = { 'data': @_uuid() }
        db.set(key, value).then((() =>
          key2 = @_uuid
          db.delete(key2).then(((res) =>
            result = res
          ), ((err) =>
            error = err
            assertions()
          ))
        ), (() =>
          assertions()
        ))
      ), (() =>
        assertions()
      ))
    )

    QUnit.module('method sync()', {
      setup: () =>
        QUnit.stop()
        @_cleanDatabase().then(() =>
          QUnit.start()
        )
    })
    QUnit.asyncTest('global', (assert) =>
      result = null
      error = null
      samplesCount = 5
      sampleData = {}
      key = null
      value = null

      # Assertions to be executed at the end
      assertions = () =>
        assert.ok(result isnt null, 'The result is not null.')
        assert.ok(error is null, 'The error is null.')
        assert.ok(typeof(result) is 'object', 'The result is an object.')

        assert.ok('push' of result, 'The result has push object.')
        assert.ok('pull' of result, 'The result has pull object.')
        assert.ok(result.push.docs_written >= samplesCount, 'The number of documents written is greater than zero.')

        QUnit.start()

      app = @_client.getApplication()
      db = app.getDatabase(@_dbName)
      writeBatch = db.getWriteBatch()

      # Adds sample data
      for i in [0..samplesCount - 1]
        id = @_uuid()
        doc = { 'data': @_uuid() }

        sampleData[id] = doc
        writeBatch.addSet(id, doc)

      # Submits sample data and test all()
      writeBatch.submit().then((() =>
        key = @_uuid()
        value = { 'data': @_uuid() }
        db.set(key, value).then((() =>
          db.sync().then(((res) =>
            result = res
            assertions()
          ), ((err) =>
            error = err
            assertions()
          ))
        ), ((err) =>
          error = err
          assertions()
        ))
      ), (() =>
        assertions()
      ))
    )

darwin.Tests = new Tests()
